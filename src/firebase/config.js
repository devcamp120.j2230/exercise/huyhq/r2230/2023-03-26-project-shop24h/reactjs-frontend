// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBBgNHYKr6NG11yHFaUEUu_zuCyA88issk",
  authDomain: "test-308c6.firebaseapp.com",
  databaseURL: "https://test-308c6-default-rtdb.firebaseio.com",
  projectId: "test-308c6",
  storageBucket: "test-308c6.appspot.com",
  messagingSenderId: "692231811872",
  appId: "1:692231811872:web:bb5a10bd1d39aae6bb9d45"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;