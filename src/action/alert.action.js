export const getAlertState = (openAlert, status, message)=>{
    return ({
        type: "ALERT_GET_STATE",
        alertOpen: openAlert,
        message: message,
        status: status,
    })
}

export const getAlertHandle = (openAlert)=>{
    return ({
        type: "ALERT_HANDLE",
        alertOpen: openAlert,
    })
}