import axios from "axios";
import { PRODUCT_TYPE_FETCH_ERROR, PRODUCT_TYPE_FETCH_PENDING, PRODUCT_TYPE_FETCH_SUCCESS } from "../constants/product-type.constant";

const url = process.env.REACT_APP_BASE_URL+"product-type"

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data.productType;
};

export const getAllProductType = ()=>{
    return async (dispatch) => {
        await dispatch({
            type: PRODUCT_TYPE_FETCH_PENDING
        })

        axiosCall(url)
            .then(result => {
                return dispatch({
                    type: PRODUCT_TYPE_FETCH_SUCCESS,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: PRODUCT_TYPE_FETCH_ERROR,
                    error: error
                })
            });
    }
};