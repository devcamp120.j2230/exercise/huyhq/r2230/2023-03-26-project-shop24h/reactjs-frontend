import axios from "axios";
import { ORDERS_FETCH_ERROR, ORDERS_FETCH_PENDING, ORDERS_FETCH_SUCCESS, ORDERS_PAGE_CHANGE, ORDER_BY_ID_FETCH_ERROR, ORDER_BY_ID_FETCH_SUCCESS, ORDER_DELETE_BY_ID_FETCH_ERROR, ORDER_DELETE_BY_ID_FETCH_SUCCESS, ORDER_DETAIL_DELETE_BY_ID_FETCH_ERROR, ORDER_DETAIL_DELETE_BY_ID_FETCH_SUCCESS, ORDER_GET_LIST, ORDER_GET_SHIP, ORDER_POST_FETCH_ERROR, ORDER_POST_FETCH_SUCCESS, ORDER_UPDATE_BY_ID_FETCH_ERROR, ORDER_UPDATE_BY_ID_FETCH_SUCCESS } from "../constants/order.constant";
import AccessTokenHeader from "../helper/AccessTokenHeader";

const url = process.env.REACT_APP_BASE_URL+"order"

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data.order;
};

//get all order
export const getAllOrder = (limit, page, order, orderBy) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        })

        var config = {
            headers: AccessTokenHeader,
        };

        //lấy tổng danh sách customer
        const response = await axios(url, config);
        const length = response.data.order.length;

        var limitObj = { start: (page - 1) * limit, limit };

        if (order) {
            limitObj = { ...limitObj, order, orderBy };
        }
        
        const params = new URLSearchParams(limitObj);
        // console.log(url + "?" + params.toString());
        axiosCall(url + "?" + params.toString(), config)
            .then(result => {
                return dispatch({
                    type: ORDERS_FETCH_SUCCESS,
                    length: length,
                    limitUser: limit,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDERS_FETCH_ERROR,
                    error: error
                })
            });
    }
}
//get order by id
export const getOrderById = (orderId) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        })

        var config = {
            headers: AccessTokenHeader,
        };

        axiosCall(url + "/" + orderId, config)
            .then(result => {
                return dispatch({
                    type: ORDER_BY_ID_FETCH_SUCCESS,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDER_BY_ID_FETCH_ERROR,
                    error: error
                })
            });
    }
}
//create new order
export const createNewOrder = (idCustomer, order) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        })

        let url = process.env.REACT_APP_BASE_URL+`customer/${idCustomer}/order`;

        var config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            },
            data: order
        };
        console.log(order);

        axiosCall(url, config)
            .then(result => {
                console.log("success");
                return dispatch({
                    type: ORDER_POST_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDER_POST_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}
//update order by id
export const updateOrderById = (orderId, order) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        })

        var config = {
            method: 'put',
            headers: AccessTokenHeader,
            data: order
        };

        axiosCall(url + "/" + orderId, config)
            .then(result => {
                return dispatch({
                    type: ORDER_UPDATE_BY_ID_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDER_UPDATE_BY_ID_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}

//delete order detail by id
export const deleteOrderDetailById = (IdOrder, IdOrderDetail) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        })

        var config = {
            method: 'delete',
            headers: AccessTokenHeader,
        };

        var url = process.env.REACT_APP_BASE_URL+`order/${IdOrder}/order-detail/${IdOrderDetail}`
        axiosCall(url, config)
            .then(result => {
                return dispatch({
                    type: ORDER_DETAIL_DELETE_BY_ID_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDER_DETAIL_DELETE_BY_ID_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}

//delete order by id
export const deleteOrderById = (IdOrder, IdCustomer) => {
    return async (dispatch) => {
        await dispatch({
            type: ORDERS_FETCH_PENDING
        })

        var config = {
            method: 'delete',
            headers: AccessTokenHeader,
        };
        var url = process.env.REACT_APP_BASE_URL+`customer/${IdCustomer}/order/${IdOrder}`
        axiosCall(url, config)
            .then(result => {
                return dispatch({
                    type: ORDER_DELETE_BY_ID_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                return dispatch({
                    type: ORDER_DELETE_BY_ID_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}

export const getOrderAction = (data) => {
    return {
        type: ORDER_GET_LIST,
        data: data
    }
};

export const getShipAction = (data) => {
    return {
        type: ORDER_GET_SHIP,
        data: data
    }
};

export const changePaginationOrder = (page) => {
    return {
        type: ORDERS_PAGE_CHANGE,
        page
    }

}
