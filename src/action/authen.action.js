import axios from "axios";
import { LOG_IN_ERROR, LOG_IN_FETCH_PENDING, LOG_IN_SUCCESS, LOG_OUT, REGISTER_FETCH_ERROR, REGISTER_FETCH_PENDING, REGISTER_FETCH_SUCCESS } from "../constants/authen.constant";

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data;
};

export const loginAction = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: LOG_IN_FETCH_PENDING
        })
        const loginUrl = process.env.REACT_APP_BASE_URL+"login"

        var config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        axiosCall(loginUrl, config)
            .then(result => {
                var _id = result.Customer._id
                var email = result.Customer.email
                var name = result.Customer.fullName
                var imgUrl = result.Customer.imageAvatar;
                var _token = result.token

                localStorage.setItem("user", JSON.stringify({
                    _id,
                    _token,
                    email, 
                    name,
                    imgUrl
                }))

                return dispatch({
                    type: LOG_IN_SUCCESS
                })
            })
            .catch(error => {
                return dispatch({
                    type: LOG_IN_ERROR,
                    error: error
                })
            });
    }
}

export const updateAction = (email) => {
    return async (dispatch) => {
        await dispatch({
            type: LOG_IN_FETCH_PENDING
        })
        const url = process.env.REACT_APP_BASE_URL+"login-update"

        var config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: email
        };

        axiosCall(url, config)
            .then(result => {
                var _id = result.Customer._id
                var email = result.Customer.email
                var name = result.Customer.fullName
                var imgUrl = result.Customer.imageAvatar;
                var _token = result.token

                localStorage.setItem("user", JSON.stringify({
                    _id,
                    _token,
                    email, 
                    name,
                    imgUrl
                }))

                return dispatch({
                    type: LOG_IN_SUCCESS
                })
            })
            .catch(error => {
                return dispatch({
                    type: LOG_IN_ERROR,
                    error: error
                })
            });
    }
}

export const registerAction = (data)=>{
    return async (dispatch) => {
        await dispatch({
            type: REGISTER_FETCH_PENDING
        })
        const url = process.env.REACT_APP_BASE_URL+"register"

        var config = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axiosCall(url, config)
            .then(result => {
                console.log(result);

                return dispatch({
                    type: REGISTER_FETCH_SUCCESS
                })
            })
            .catch(error => {
                return dispatch({
                    type: REGISTER_FETCH_ERROR,
                    error: error.response.status
                })
            });
    }
}
export const logoutAction = ()=>{
    localStorage.removeItem("user");
    return {
        type: LOG_OUT
    }
}