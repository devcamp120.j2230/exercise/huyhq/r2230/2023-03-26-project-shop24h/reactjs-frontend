import axios from "axios";
import { PRODUCTS_FETCH_ERROR, PRODUCTS_FETCH_PENDING, PRODUCTS_FETCH_SUCCESS, PRODUCTS_PAGE_CHANGE, PRODUCT_DELETE_BY_ID_SUCCESS, PRODUCT_FETCH_BY_ID_SUCCESS, PRODUCT_POST_FETCH_ERROR, PRODUCT_POST_FETCH_SUCCESS, PRODUCT_PUT_FETCH_ERROR, PRODUCT_PUT_FETCH_SUCCESS, PRODUCT_SOFT_DELETE_FETCH_ERROR, PRODUCT_SOFT_DELETE_FETCH_SUCCESS } from "../constants/product.constant";
import  AccessTokenHeader  from "../helper/AccessTokenHeader";

const url = process.env.REACT_APP_BASE_URL+"product"

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data.product;
};

//lấy dữ liệu toàn bộ sản phẩm
export const getAllProduct = (limit, page, order, orderBy, filter) => {
    return async (dispatch) => {
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        var config = {
            headers: AccessTokenHeader,
        };

        var limitObj = ""

        if (filter) {
            limitObj = filter;
        }

        // if (filter && filter.min) {
        //     limitObj = { ...limitObj, min: filter.min };
        // }

        // if (filter && filter.max) {
        //     limitObj = { ...limitObj, max: filter.max };
        // }

        // if (filter && filter.category) {
        //     limitObj = { ...limitObj, category: filter.category };
        // }

        var params = new URLSearchParams(limitObj);

        //lấy tổng danh sách sản phẩm
        const response = await axios(url + "?" + params.toString(), config);
        const lengthProduct = response.data.product.length;

        limitObj = { ...limitObj, start: (page - 1) * limit, limit };

        if(order){
            limitObj = { ...limitObj, order, orderBy };
        }
        params = new URLSearchParams(limitObj);
        // console.log(url);
        // console.log(url + "?" + params.toString());
        axiosCall(url + "?" + params.toString(), config)
            .then(result => {
                return dispatch({
                    type: PRODUCTS_FETCH_SUCCESS,
                    length: lengthProduct,
                    limitProduct: limit,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: PRODUCTS_FETCH_ERROR,
                    error: error
                })
            });
    }
}

//lấy dữ liệu 1 sản phẩm bằng id
export const getProductById = (id)=>{
    return async (dispatch)=>{
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        var config = {
            headers: AccessTokenHeader,
        };

        axiosCall(url + "/" + id, config)
        .then(result => {
            return dispatch({
                type: PRODUCT_FETCH_BY_ID_SUCCESS,
                data: result
            })
        })
        .catch(error => {
            return dispatch({
                type: PRODUCTS_FETCH_ERROR,
                error: error
            })
        });
        
    }
}

//thêm mới 1 sản phẩm
export const postProductAction = (product)=>{
    return async (dispatch)=>{
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        var config = {
			method: 'post',
			headers: AccessTokenHeader,
            data: product
		};

        axiosCall(url, config)
        .then(result => {
            return dispatch({
                type: PRODUCT_POST_FETCH_SUCCESS,
                state: "success"
            })
        })
        .catch(error => {
            return dispatch({
                type: PRODUCT_POST_FETCH_ERROR,
                error: error,
                data: "error"
            })
        });
        
    }
}

//update 1 sản phẩm bằng id
export const updateProductById = (id, product)=>{
    return async (dispatch)=>{
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        var config = {
			method: 'put',
			headers: AccessTokenHeader,
            data: product
		};

        axiosCall(url + "/" + id, config)
        .then(result => {
            return dispatch({
                type: PRODUCT_PUT_FETCH_SUCCESS,
                state: "success"
            })
        })
        .catch(error => {
            return dispatch({
                type: PRODUCT_PUT_FETCH_ERROR,
                error: error,
                data: "error"
            })
        });
        
    }
}

//xóa tạm 1 sản phẩm bằng id
export const softDeleteProductById = (id)=>{
    return async (dispatch)=>{
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        var config = {
			method: 'put',
			headers: AccessTokenHeader,
		};

        axiosCall(url + "/soft-delete/" + id, config)
        .then(result => {
            return dispatch({
                type: PRODUCT_SOFT_DELETE_FETCH_SUCCESS,
                state: "success"
            })
        })
        .catch(error => {
            return dispatch({
                type: PRODUCT_SOFT_DELETE_FETCH_ERROR,
                error: error,
                data: "error"
            })
        });
        
    }
}
//xóa 1 sản phẩm bằng id
export const deleteProductById = (id)=>{
    return async (dispatch)=>{
        await dispatch({
            type: PRODUCTS_FETCH_PENDING
        })

        var config = {
			method: 'delete',
			headers: AccessTokenHeader,
		};

        axiosCall(url + "/" + id, config)
        .then(result => {
            return dispatch({
                type: PRODUCT_DELETE_BY_ID_SUCCESS,
                state: "success"
            })
        })
        .catch(error => {
            return dispatch({
                type: PRODUCTS_FETCH_ERROR,
                error: error,
                data: "error"
            })
        });
        
    }
}

//xử lý phân trang
export const changePaginationAction = (page) => {
    return {
        type: PRODUCTS_PAGE_CHANGE,
        page
    }
    
}