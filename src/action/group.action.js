import axios from "axios";
import { GROUPS_FETCH_ERROR, GROUPS_FETCH_PENDING, GROUPS_FETCH_SUCCESS } from "../constants/group.constant";

const url = process.env.REACT_APP_BASE_URL+"group"

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data.group;
};

export const getListGroupAction = ()=>{
    return async (dispatch) => {
        await dispatch({
            type: GROUPS_FETCH_PENDING
        })

        axiosCall(url)
            .then(result => {
                return dispatch({
                    type: GROUPS_FETCH_SUCCESS,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: GROUPS_FETCH_ERROR,
                    error: error
                })
            });
    }
}