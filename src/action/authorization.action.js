import axios from "axios";
import { AUTHORIZATION_ERROR, AUTHORIZATION_PENDING, AUTHORIZATION_SUCCESS } from "../constants/authorization.constant";
const url = process.env.REACT_APP_BASE_URL+"authorization"

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data;
};

export const checkAuthorization = (token) => {
    return async (dispatch) => {
        await dispatch({
            type: AUTHORIZATION_PENDING
        })
        
        var config = {
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
                'auth-token': token
            },
        };

        axiosCall(url, config)
            .then(result => {
                // console.log(result);
                return dispatch({
                    type: AUTHORIZATION_SUCCESS,
                    data: result.data
                })
            })
            .catch(error => {
                // console.log(error);
                return dispatch({
                    type: AUTHORIZATION_ERROR,
                    error: error
                })
            });
    }
}
