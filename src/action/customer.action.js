import axios from "axios";
import { CUSTOMER_DELETE_FETCH_ERROR, CUSTOMERS_FETCH_ERROR, CUSTOMERS_FETCH_PENDING, CUSTOMERS_FETCH_SUCCESS, CUSTOMERS_PAGE_CHANGE, CUSTOMER_POST_FETCH_ERROR, CUSTOMER_PUT_FETCH_ERROR, CUSTOMER_DELETE_FETCH_SUCCESS, CUSTOMER_POST_FETCH_SUCCESS, CUSTOMER_PUT_FETCH_SUCCESS, CUSTOMER_SOFT_DELETE_FETCH_SUCCESS, CUSTOMER_SOFT_DELETE_FETCH_ERROR, CUSTOMER_FETCH_BY_ID_SUCCESS, CUSTOMER_FETCH_BY_ID_ERROR } from "../constants/customer.constant";
import  AccessTokenHeader  from "../helper/AccessTokenHeader";

const url = process.env.REACT_APP_BASE_URL+"customer"

const axiosCall = async (url, config) => {
    const response = await axios(url, config);
    return response.data.customer;
};

//get all customer
export const getAllCustomer = (limit, page, order, orderBy, filter) => {
    return async (dispatch) => {
        await dispatch({
            type: CUSTOMERS_FETCH_PENDING
        })

        var config = {
            headers: AccessTokenHeader,
        };

        var limitObj = "";

        if(filter && filter.all){
            limitObj = {...limitObj, all: filter.all}
        }
        var params = new URLSearchParams(limitObj);
        //lấy tổng danh sách customer
        const response = await axios(url + "?" + params.toString(), config);

        const length = response.data.customer.length;

        limitObj = {...limitObj,  start: (page - 1) * limit, limit };

        if(order){
            limitObj = { ...limitObj, order, orderBy };
        }
        
        params = new URLSearchParams(limitObj);

        // console.log(url + "?" + params.toString());
        axiosCall(url + "?" + params.toString(), config)
            .then(result => {
                return dispatch({
                    type: CUSTOMERS_FETCH_SUCCESS,
                    length: length,
                    limitUser: limit,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: CUSTOMERS_FETCH_ERROR,
                    data: "error",
                    error: error
                })
            });
    }
}

//get customer by id
export const getCustomerById = (id) => {
    return async (dispatch) => {
        await dispatch({
            type: CUSTOMERS_FETCH_PENDING
        })

        axiosCall(url + "/" + id)
            .then(result => {
                return dispatch({
                    type: CUSTOMER_FETCH_BY_ID_SUCCESS,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: CUSTOMER_FETCH_BY_ID_ERROR,
                    error: error
                })
            });
    }
}

//create new customer
export const createCustomerAction = (customer) => {
    return async (dispatch) => {
        await dispatch({
            type: CUSTOMERS_FETCH_PENDING
        })

        var config = {
            method: 'post',
            headers: AccessTokenHeader,
            data: customer
        };

        axiosCall(url, config)
            .then(result => {
                return dispatch({
                    type: CUSTOMER_POST_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                console.log(error);
                return dispatch({
                    type: CUSTOMER_POST_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}

//update customer
export const updateCustomerByIdAction = (idCustomer, customer) => {
    return async (dispatch) => {
        await dispatch({
            type: CUSTOMERS_FETCH_PENDING
        })

        var config = {
            method: 'put',
            headers: AccessTokenHeader,
            data: customer
        };

        axiosCall(url + "/"+ idCustomer, config)
        .then(result => {
                return dispatch({
                    type: CUSTOMER_PUT_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                return dispatch({
                    type: CUSTOMER_PUT_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}

//xóa mềm customer
export const softDeleteCustomerByIdAction = (idCustomer) => {
    return async (dispatch) => {
        await dispatch({
            type: CUSTOMERS_FETCH_PENDING
        })

        var config = {
            method: 'put',
            headers: AccessTokenHeader,
        };

        axiosCall(url + "/soft-delete/"+ idCustomer, config)
            .then(result => {
                return dispatch({
                    type: CUSTOMER_SOFT_DELETE_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                return dispatch({
                    type: CUSTOMER_SOFT_DELETE_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}
//delete customer by id
export const deleteCustomerById = (idCustomer) => {
    return async (dispatch) => {
        await dispatch({
            type: CUSTOMERS_FETCH_PENDING
        })

        var config = {
            method: 'delete',
            headers: AccessTokenHeader,
        };

        axiosCall(url + "/"+ idCustomer, config)
            .then(result => {
                return dispatch({
                    type: CUSTOMER_DELETE_FETCH_SUCCESS,
                    data: "success"
                })
            })
            .catch(error => {
                return dispatch({
                    type: CUSTOMER_DELETE_FETCH_ERROR,
                    error: error,
                    data: "error"
                })
            });
    }
}
//phân trang
export const changePaginationCustomer = (page) => {
    return {
        type: CUSTOMERS_PAGE_CHANGE,
        page
    }
    
}