import { CART_GET_DISCOUNT_CODE, CART_GET_LIST, CART_GET_NUMBER_ITEM, CART_GET_SUBTOTAL } from "../constants/cart.constant"

export const getListCart = (data)=>{
    return {
        type: CART_GET_LIST,
        data: data
    }
};

export const getNumberItem = (num)=>{
    return {
        type: CART_GET_NUMBER_ITEM,
        data: num
    }
};

export const getSubtotalCart = (value)=>{
    return {
        type: CART_GET_SUBTOTAL,
        data: value
    }
};

export const getDiscountAction = (value)=>{
    return{
        type: CART_GET_DISCOUNT_CODE,
        data: value
    }
}
