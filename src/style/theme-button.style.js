import { createTheme } from "@mui/material";

export const themeList = createTheme({
    components: {
        // For label
        MuiListItemButton: {
            styleOverrides: {
                root: {
                    "&.Mui-selected": {
                        backgroundColor: "grey"
                    },
                    "&:hover": {
                        color: "white"
                    }
                }
            }
        }
    }
});