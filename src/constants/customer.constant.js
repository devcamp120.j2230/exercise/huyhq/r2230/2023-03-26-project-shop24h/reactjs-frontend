export const CUSTOMERS_FETCH_PENDING = "CUSTOMERS_FETCH_PENDING";
export const CUSTOMERS_FETCH_SUCCESS = "CUSTOMERS_FETCH_SUCCESS";
export const CUSTOMERS_FETCH_ERROR = "CUSTOMERS_FETCH_ERROR";
export const CUSTOMERS_PAGE_CHANGE = "CUSTOMERS_PAGE_CHANGE";

export const CUSTOMER_FETCH_BY_ID_SUCCESS = "CUSTOMER_FETCH_BY_ID_SUCCESS";
export const CUSTOMER_FETCH_BY_ID_ERROR = "CUSTOMER_FETCH_BY_ID_ERROR";

export const CUSTOMER_POST_FETCH_SUCCESS = "CUSTOMER_POST_FETCH_SUCCESS";
export const CUSTOMER_POST_FETCH_ERROR = "CUSTOMERS_POST_FETCH_ERROR";

export const CUSTOMER_PUT_FETCH_SUCCESS = "CUSTOMER_PUT_FETCH_SUCCESS";
export const CUSTOMER_PUT_FETCH_ERROR = "CUSTOMERS_PUT_FETCH_ERROR";

export const CUSTOMER_SOFT_DELETE_FETCH_SUCCESS = "CUSTOMER_SOFT_DELETE_FETCH_SUCCESS";
export const CUSTOMER_SOFT_DELETE_FETCH_ERROR = "CUSTOMER_SOFT_DELETE_FETCH_ERROR";

export const CUSTOMER_DELETE_FETCH_SUCCESS = "CUSTOMER_DELETE_FETCH_SUCCESS";
export const CUSTOMER_DELETE_FETCH_ERROR = "CUSTOMER_DELETE_FETCH_ERROR";