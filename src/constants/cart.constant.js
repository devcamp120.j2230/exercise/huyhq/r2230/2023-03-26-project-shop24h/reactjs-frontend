export const CART_GET_LIST = "CART_GET_LIST";
export const CART_GET_SUBTOTAL = "CART_GET_SUBTOTAL";
export const CART_GET_NUMBER_ITEM = "CART_GET_NUMBER_ITEM";
export const CART_GET_DISCOUNT_CODE = "CART_GET_DISCOUNT_CODE";
