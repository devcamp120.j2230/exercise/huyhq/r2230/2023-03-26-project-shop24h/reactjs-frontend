const token = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user"))._token : "";

var AccessTokenHeader =  {
    'Content-Type': 'application/json',
    'auth-token': token
}

export default AccessTokenHeader