export const formatCurrency = (price) => {
    return new Intl.NumberFormat().format(price)
}