import AdminCustomerList from "./component/admin/content/main/AdminCustomerList";
import AdminHome from "./component/admin/content/main/AdminHome";
import AdminOrderList from "./component/admin/content/main/AdminOrderList";
import AdminProductList from "./component/admin/content/main/AdminProductList";
import CartComponent from "./component/cart/CartComponent";
import HomeComponent from "./component/content/home/HomeComponent";
import UserComponent from "./component/content/user/UserComponent";
import LoginComponent from "./component/login/LoginComponent";
import RegisterComponent from "./component/login/RegisterComponent";
import OrderComponent from "./component/order/OrderComponent";
import ProductDetail from "./component/product/product-detail/ProductDetail";
import ProductList from "./component/product/ProductList";

export const routeAdmin = [
    { label: "Admin Home", path: "/home", element: <AdminHome />},
    { label: "Admin Product", path: "/products", element: <AdminProductList /> },
    { label: "Admin Customer", path: "/customers", element: <AdminCustomerList /> },
    { label: "Admin Order", path: "/orders", element: <AdminOrderList /> },
]

export const routeList = [
    { label: "", path: "/login", element: <LoginComponent /> },
    { label: "", path: "/register", element: <RegisterComponent /> },
    { label: "", path: "/", element: <HomeComponent /> },
    { label: "Product", path: "/products", element: <ProductList /> },
    { label: "", path: "/products/:productId", element: <ProductDetail /> },
    { label: "", path: "/cart", element: <CartComponent /> },
    { label: "", path: "/order", element: <OrderComponent /> },
    { label: "", path: "/user", element: <UserComponent /> },
]


