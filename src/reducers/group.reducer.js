import { GROUPS_FETCH_ERROR, GROUPS_FETCH_PENDING, GROUPS_FETCH_SUCCESS } from "../constants/group.constant";

const groupState = {
    groups: "",
    pending: true
}

const groupReducer = (state = groupState, action) => {
    switch (action.type) {
        case GROUPS_FETCH_PENDING:
            state.pending = true;
            break;
        case GROUPS_FETCH_SUCCESS:
            state.pending = false;
            state.groups = action.data;
            break;
        case GROUPS_FETCH_ERROR:
            state.pending = false;
            break;
        default:
            break;
    }
    return { ...state };
};

export default groupReducer;