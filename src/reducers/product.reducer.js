import { PRODUCTS_FETCH_ERROR, PRODUCTS_FETCH_PENDING, PRODUCTS_FETCH_SUCCESS, PRODUCTS_PAGE_CHANGE, PRODUCT_DELETE_BY_ID_SUCCESS, PRODUCT_FETCH_BY_ID_SUCCESS, PRODUCT_POST_FETCH_SUCCESS, PRODUCT_PUT_FETCH_SUCCESS, PRODUCT_SOFT_DELETE_FETCH_SUCCESS } from "../constants/product.constant";

const productState = {
    products: "",
    pending: false,
    currentPage: 1,
    countPage: 0,
    productById: "",
    statePost: "",
    statePut: "",
    stateDelete: "",
}

const productReducer = (state = productState, action) => {
    switch (action.type) {
        case PRODUCTS_FETCH_PENDING:
            state.pending = true;
            state.statePost = "";
            state.statePut = "";
            state.stateDelete = "";
            break;
        case PRODUCTS_FETCH_SUCCESS:
            state.countPage = Math.ceil(action.length / action.limitProduct);
            state.pending = false;
            state.products = action.data;
            state.currentPage = state.countPage >= state.currentPage ? state.currentPage : 1;
            break;
        case PRODUCT_FETCH_BY_ID_SUCCESS:
            state.productById = action.data;
            break;
        case PRODUCT_POST_FETCH_SUCCESS:
            state.statePost = action.data;
            break;
        case PRODUCT_PUT_FETCH_SUCCESS:
            state.statePut = action.data;
            break;
        case PRODUCT_SOFT_DELETE_FETCH_SUCCESS:
            state.stateDelete = action.state;
            break;
        case PRODUCTS_FETCH_ERROR:
            state.pending = true;
            break;
        case PRODUCTS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        default:
            break;
    }
    return { ...state };
};

export default productReducer;