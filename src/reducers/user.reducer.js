import { LOG_IN_ERROR, LOG_IN_FETCH_PENDING, LOG_IN_SUCCESS, LOG_OUT, REGISTER_FETCH_ERROR, REGISTER_FETCH_PENDING, REGISTER_FETCH_SUCCESS } from "../constants/authen.constant";
import { AUTHORIZATION_ERROR, AUTHORIZATION_PENDING, AUTHORIZATION_SUCCESS } from "../constants/authorization.constant";

const getUser = JSON.parse(localStorage.getItem('user'));

const userState = {
    user: getUser ? getUser : null,
    token: getUser ? getUser._token : null,
    statusLogin: "",
    statusRegister: "",
    pending: true,
    pendingAuth: true,
    authorization: null,
    checkTimeAuthExp: false,
}

const userReducer = (state = userState, action) => {
    switch (action.type) {
        case LOG_IN_FETCH_PENDING:
            state.pending = true;
            state.statusLogin = "";
            break;
        case LOG_IN_SUCCESS:
            state.statusLogin = "success";
            state.pending = false;
            break;
        case LOG_IN_ERROR:
            state.statusLogin = "error";
            state.pending = false;
            break;
        case LOG_OUT:
            state.pending = false;
            state.user = null;
            state.token = null;
            break;
        case REGISTER_FETCH_PENDING:
            state.pending = true;
            state.statusRegister = "";
            break;
        case REGISTER_FETCH_SUCCESS:
            state.pending = false;
            state.statusRegister = "success";
            break;
        case REGISTER_FETCH_ERROR:
            state.pending = false;
            state.statusRegister = "error";
            break;
        case AUTHORIZATION_PENDING:
            state.pendingAuth = true;
            break;
        case AUTHORIZATION_SUCCESS:
            state.pendingAuth = false;
            state.authorization = true;
            // console.log(Date.now());
            // console.log(action.data.exp * 1000);
            // console.log("CheckTime: " + (Date.now() <= action.data.exp * 1000));
            state.checkTimeAuthExp = Date.now() <= action.data.exp * 1000;
            break;
        case AUTHORIZATION_ERROR:
            state.pendingAuth = false;
            state.authorization = false;
            break;
        default:
            break;
    }
    return { ...state };
};

export default userReducer;