import { CUSTOMERS_FETCH_ERROR, CUSTOMERS_FETCH_PENDING, CUSTOMERS_FETCH_SUCCESS, CUSTOMERS_PAGE_CHANGE, CUSTOMER_DELETE_FETCH_SUCCESS, CUSTOMER_FETCH_BY_ID_SUCCESS, CUSTOMER_POST_FETCH_SUCCESS, CUSTOMER_PUT_FETCH_SUCCESS, CUSTOMER_SOFT_DELETE_FETCH_SUCCESS } from "../constants/customer.constant";

const customerState = {
    customers: "",
    customer: "",
    currentPage: 1,
    countPage: 0,
    pending: true,
    message: "",
    stateView: "",
    statePost: "",
    statePut: "",
    stateDelete: "",
}

const customerReducer = (state = customerState, action) => {
    switch (action.type) {
        case CUSTOMERS_FETCH_PENDING:
            state.pending = true;
            state.message = "";
            state.stateView = "";
            state.statePost = "";
            state.statePut = "";
            state.stateDelete = "";
            break;
        case CUSTOMERS_FETCH_SUCCESS:
            state.countPage = Math.ceil(action.length / action.limitUser);
            state.pending = false;
            state.customers = action.data;
            state.currentPage = state.countPage >= state.currentPage ? state.currentPage : 1;
            break;
        case CUSTOMER_FETCH_BY_ID_SUCCESS:
            state.customer = action.data;
            state.pending = false;
            break;
        case CUSTOMER_POST_FETCH_SUCCESS:
            state.statePost = action.data;
            break;
        case CUSTOMER_PUT_FETCH_SUCCESS:
            state.statePut = action.data;
            break;
        case CUSTOMER_SOFT_DELETE_FETCH_SUCCESS:
            state.stateDelete = action.data;
            break;
        case CUSTOMERS_FETCH_ERROR:
            state.pending = true;
            break;
        case CUSTOMERS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        default:
            break;
    }
    return { ...state };
};

export default customerReducer;