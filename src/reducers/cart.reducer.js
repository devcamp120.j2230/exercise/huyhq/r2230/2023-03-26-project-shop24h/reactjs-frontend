import { CART_GET_DISCOUNT_CODE, CART_GET_LIST, CART_GET_NUMBER_ITEM, CART_GET_SUBTOTAL } from "../constants/cart.constant";
import { cartItem } from "../data/cart.data";

const cartState = {
    cart: cartItem(),
    itemNum: cartItem().length,
    subtotal: 0,
    discount: 0,
}

const cartReducer = (state = cartState, action) => {
    switch (action.type) {
        case CART_GET_LIST:
            console.log(action.data);
            state.cart = action.data;
            break;
        case CART_GET_SUBTOTAL:
            state.subtotal = action.data;
            break;
        case CART_GET_NUMBER_ITEM:
            state.itemNum = action.data;
            break;
        case CART_GET_DISCOUNT_CODE:
            state.discount = (action.data * state.subtotal) / 100;
            break;
        default:
            break;
    }
    return { ...state }
}

export default cartReducer;
