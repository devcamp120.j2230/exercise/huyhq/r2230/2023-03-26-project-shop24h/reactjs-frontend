import { PRODUCT_TYPE_FETCH_ERROR, PRODUCT_TYPE_FETCH_PENDING, PRODUCT_TYPE_FETCH_SUCCESS } from "../constants/product-type.constant";

const productState = {
    productType: "",
    pending: false,
}

const productTypeReducer = (state = productState, action) => {
    switch (action.type) {
        case PRODUCT_TYPE_FETCH_PENDING:
            state.pending = true;
            break;
        case PRODUCT_TYPE_FETCH_SUCCESS:
            state.pending = false;
            state.productType = action.data;
            break;
        case PRODUCT_TYPE_FETCH_ERROR:
            state.pending = true;
            break;
        default:
            break;
    }
    return { ...state };
};

export default productTypeReducer;