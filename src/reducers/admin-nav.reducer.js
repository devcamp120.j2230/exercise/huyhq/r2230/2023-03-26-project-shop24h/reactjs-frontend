
const checkState = {
    checkNavOpen: true,
}

const checkNavAdminReducer = (state = checkState, action) => {
    switch (action.type) {
        case "ADMIN_NAV_TOGGLE":
            state.checkNavOpen = !state.checkNavOpen;
            break;
        case "ADMIN_NAV_CLOSE":
            state.checkNavOpen = false;
            break;
        default:
            break;
    }
    return { ...state }
}

export default checkNavAdminReducer;
