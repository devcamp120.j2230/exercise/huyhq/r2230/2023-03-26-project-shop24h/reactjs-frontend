
const alertState = {
    alertOpen: false,
    alertMessage: "",
    alertStatus: "",
}

const alertReducer = (state = alertState, action) => {
    switch (action.type) {
        case "ALERT_HANDLE":
            state.alertOpen = action.alertOpen;
            break;
        case "ALERT_GET_STATE":
            state.alertOpen = action.alertOpen;
            state.alertMessage = action.message;
            state.alertStatus = action.status;
            break;
        case "ALERT_OPEN":
            state.alertOpen = true;
            state.alertMessage = action.message;
            state.alertStatus = action.status;
            break;
        case "ALERT_CLOSE":
            state.alertOpen = false;
            state.alertMessage = "";
            state.alertStatus = "";
            break;
        default:
            break;
    }
    return { ...state }
}

export default alertReducer;
