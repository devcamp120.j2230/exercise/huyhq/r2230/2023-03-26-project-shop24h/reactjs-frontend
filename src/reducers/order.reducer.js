import { ORDERS_FETCH_ERROR, ORDERS_FETCH_PENDING, ORDERS_FETCH_SUCCESS, ORDERS_PAGE_CHANGE, ORDER_BY_ID_FETCH_SUCCESS, ORDER_DELETE_BY_ID_FETCH_SUCCESS, ORDER_DETAIL_DELETE_BY_ID_FETCH_SUCCESS, ORDER_GET_LIST, ORDER_GET_SHIP, ORDER_UPDATE_BY_ID_FETCH_SUCCESS } from "../constants/order.constant";

const orderState = {
    orders: "",
    // order: "",
    pending: true,
    currentPage: 1,
    countPage: 0,
    orderProduct: "",
    costShip: 0,
    stateUpdateOrder: "",
    stateDeleteOrder: "",
    // stateDeleteOrderDetail: "",
}

const orderReducer = (state = orderState, action) => {
    switch (action.type) {
        case ORDERS_FETCH_PENDING:
            state.pending = true;
            state.stateUpdateOrder = "";
            state.stateDeleteOrder = "";
            // state.stateDeleteOrderDetail = "";
            break;
        case ORDERS_FETCH_SUCCESS:
            state.countPage = Math.ceil(action.length / action.limitUser);
            state.pending = false;
            state.orders = action.data;
            state.currentPage = state.countPage >= state.currentPage ? state.currentPage : 1;
            break;
        // case ORDER_BY_ID_FETCH_SUCCESS:
        //     state.order = action.data;
        //     break;
        case ORDER_DELETE_BY_ID_FETCH_SUCCESS:
            state.stateDeleteOrder = action.data;
            break;
        case ORDER_UPDATE_BY_ID_FETCH_SUCCESS:
            state.stateUpdateOrder = action.data;
            break;
        // case ORDER_DETAIL_DELETE_BY_ID_FETCH_SUCCESS:
        //     state.stateDeleteOrderDetail = action.data;
        //     break;
        case ORDERS_FETCH_ERROR:
            state.pending = true;
            break;
        case ORDERS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        case ORDER_GET_LIST:
            state.orderProduct = action.data;
            break;
        case ORDER_GET_SHIP:
            state.costShip = action.data;
            break;
        default:
            break;
    }
    return { ...state }
}

export default orderReducer;
