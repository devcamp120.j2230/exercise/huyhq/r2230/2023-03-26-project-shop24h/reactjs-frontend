
const checkState = {
    search: "",
}

const adminSearchReducer = (state = checkState, action) => {
    switch (action.type) {
        case "ADMIN_SEARCH":
            state.search = action.data;
            break;
        default:
            break;
    }
    return { ...state }
}

export default adminSearchReducer;
