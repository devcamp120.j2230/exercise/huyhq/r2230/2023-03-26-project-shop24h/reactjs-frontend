import { Container, Grid, Box } from "@mui/material";

import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import OrderDetail from "./OrderDetail";
import { useState } from "react";
import AlertNotification from "../alert/AlertNotification";
import ModalAlertNotification from "../alert/ModalAlertNotification";

const setTimeOut = 3000;
const OrderComponent = () => {
    const location = useLocation();
    const navigate = useNavigate();

    const [openAlert, setOpenAlert] = useState(false);

    const [message, setMessage] = useState("");
    const [status, setStatus] = useState("success");

    const { user, pending } = useSelector((reduxData) => {
        return reduxData.userReducer;
    })

    const { orderProduct } = useSelector((reduxData) => {
        return reduxData.orderReducer;
    })

    useEffect(() => {
        //kiểm tra các điều kiện
        if (orderProduct === "") {
            getNotification("warning", "Giỏ hàng không được để trống!")
            setTimeout(() => {
                navigate("/cart")
            }, setTimeOut)
        } else if (orderProduct.subtotal === 0) {
            getNotification("warning", "Giỏ hàng không được để trống!")
            setTimeout(() => {
                navigate("/cart")
            }, setTimeOut)
        } else if (user === null) {
            getNotification("warning", "Bạn chưa đăng nhập!")
            setTimeout(() => {
                navigate("/login")
            }, setTimeOut)
        }
    }, [])

    //lấy dữ liệu thông báo rồi show
    const getNotification = (status, message) => {
        setStatus(status)
        setMessage(message)
        setOpenAlert(true)
    }

    return (
        <Container maxWidth={false}>
            <Grid container justifyContent="center" textAlign="center">
                <Box sx={{ width: '100%' }}>
                    {
                        openAlert
                            ? <ModalAlertNotification
                                state={status}
                                status={openAlert}
                                message={message}
                                getStatus={(val) => { setOpenAlert(val) }}
                            />
                            : false
                    }
                    {
                        openAlert
                            ? <AlertNotification
                                state={status}
                                status={openAlert}
                                message={message}
                                getStatus={(val) => { setOpenAlert(val) }}
                            />
                            : false
                    }

                </Box>
            </Grid>
            {
                orderProduct !== ""
                    ? orderProduct.subtotal !== 0
                        ? user
                            ? <OrderDetail
                                orderProduct={orderProduct}
                                getNotification={getNotification}
                                setTimeOut = {setTimeOut}
                            />
                            : false
                        : false
                    : false
            }


        </Container>
    )
}

export default OrderComponent;