import { Grid, FormGroup, FormControlLabel, Checkbox, Divider, Button, Typography } from "@mui/material";
import OrderFormInfo from "./content/OrderFormInfo";
import OrderShipInfo from "./content/OrderShipInfo";
import OrderProductInfo from "./content/OrderProductInfo";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getCustomerById, updateCustomerByIdAction } from "../../action/customer.action";
import { createNewOrder } from "../../action/order.action";
import { useNavigate } from "react-router-dom";
import { Breadcrumb } from "reactstrap";
import BreadCrumb from "../BreadCrumb";

const breadcrumb = [
    {
        name: "Trang chủ",
        url: "/"
    },
    {
        name: "Giỏ hàng",
        url: "/cart"
    },
    {
        name: "Thanh toán",
        url: "#"
    },
]

const OrderDetail = (props) => {
    const { orderProduct, setTimeOut } = props;

    const [checkTerm, setCheckTerm] = useState(false);
    const [dataForm, setDataForm] = useState("");

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const { costShip } = useSelector(data => data.orderReducer);
    const { user } = useSelector(data => data.userReducer);
    const { customer } = useSelector(data => data.customerReducer);

    //get form data
    const getDataForm = (data) => {
        setDataForm(data)
    }

    //validate data form
    const validateData = (data) => {
        var message = "";
        if (data.fullName === "") {
            message = "Họ tên không được để trống"
            props.getNotification("error", message);
            return false;
        }

        if (data.phone === "") {
            message = "Số điện thoại không được để trống"
            props.getNotification("error", message);
            return false;
        }

        if (data.email === "") {
            message = "Email không được để trống"
            props.getNotification("error", message);
            return false;
        }

        if (data.country === "") {
            message = "Hãy chọn quốc gia."
            props.getNotification("error", message);
            return false;
        }

        if (data.city === "") {
            message = "Hãy chọn tỉnh/thành phố."
            props.getNotification("error", message);
            return false;
        }

        if (data.district === "") {
            message = "Hãy chọn quận/huyện."
            props.getNotification("error", message);
            return false;
        }

        if (data.address === "") {
            message = "Địa chỉ không được để trống"
            props.getNotification("error", message);
            return false;
        }

        return true;
    }

    //click button send order
    const onClickSendOrder = () => {
        if (validateData(dataForm.customer)) {
            var message = "";
            if (checkTerm) {
                message = "Bạn đã đặt hàng thành công!"
                props.getNotification("success", message);

                var getOrder = {
                    customer: dataForm.customer,
                    order: {
                        order: {
                            shippedDate: "",
                            note: dataForm.note,
                            costShip: costShip,
                            subtotal: orderProduct.subtotal,
                            discount: orderProduct.discount,
                            total: orderProduct.total,
                        },
                        detail: orderProduct.products.map((orderItem) => {
                            return ({
                                product: orderItem._id,
                                quantity: orderItem.quantity,
                            })
                        })
                    }
                }
                // console.log(JSON.stringify(getOrder.order));
                dispatch(updateCustomerByIdAction(customer._id, getOrder.customer));
                dispatch(createNewOrder(customer._id, getOrder.order));
                //xóa sản phẩm đã đặt trong giỏ hàng
                var cart = [];
                cart = JSON.parse(localStorage.getItem("cart"))
                if (cart.length > 0) {
                    cart = cart.filter(item => !item.check);
                    // localStorage.setItem("cart", JSON.stringify(cart));
                }
                //load lại trang
                setTimeout(() => {
                    navigate(0);
                }, setTimeOut)
            } else {
                message = "Bạn chưa đồng ý với các điều khoản."
                props.getNotification("warning", message);
            }
        }
    }

    useEffect(() => {
        dispatch(getCustomerById(user._id));
    }, [])

    return (
        <Grid container>
            <Grid item xs={12} my={3}>
                <BreadCrumb link={JSON.stringify(breadcrumb)} />
            </Grid>
            <Grid item xs={12}>
                <Typography component="div" variant="h5" textTransform="uppercase" fontWeight={700} mt={1}>Thanh toán</Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={8}>
                {
                    customer !== ""
                        ? <OrderFormInfo
                            userById={customer}
                            getDataForm={getDataForm}
                        />
                        : false
                }
                <OrderShipInfo />
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={4}>
                <Grid container justifyContent="center" mt={3} pl={{ xs: 0, md: 3 }}>
                    <Grid container justifyContent="center" flexDirection="column" border="1px solid #E2E6E9" borderRadius="5px" p={5}>
                        <OrderProductInfo orderList={orderProduct} />
                        <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
                        <Grid item my={1}>
                            <FormGroup sx={{ paddingLeft: "16px" }}>
                                <FormControlLabel control={
                                    <Checkbox
                                        defaultChecked={checkTerm}
                                        onChange={(e) => { setCheckTerm(e.target.checked) }}
                                    />
                                }
                                    labelPlacement="end"
                                    label="Tôi đồng ý với các điều khoản bán hàng và giao hàng."
                                />
                            </FormGroup>
                        </Grid>
                        <Grid item my={1}>
                            <Button
                                variant="contained"
                                sx={{ width: "100%", background: "#515151", color: "#ffff", borderRadius: "20px" }}
                                onClick={onClickSendOrder}
                            >
                                ĐẶT HÀNG
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default OrderDetail