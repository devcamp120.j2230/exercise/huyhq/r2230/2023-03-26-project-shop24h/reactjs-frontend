import { Box, Divider, Grid, Typography } from "@mui/material"
import cityList from "../../../data/data-country-vn.json"
import countryList from "../../../data/data-country-list.json"
import { useEffect } from "react"
import { useState } from "react"

const OrderFormInfo = (props) => {
    const { userById, getDataForm } = props;

    const [fullName, setFullName] = useState(userById !== "" ? userById.fullName : "")
    const [email, setEmail] = useState(userById !== "" ? userById.email : "")
    const [phone, setPhone] = useState(userById !== "" ? userById.phone : "")
    const [address, setAddress] = useState(userById !== "" ? userById.address : "")
    const [city, setCity] = useState(userById !== "" ? userById.city : "")
    const [country, setCountry] = useState(userById !== "" ? userById.country : "")
    const [note, setNote] = useState(userById !== "" ? userById.note : "")
    const [district, setDistrict] = useState(userById !== "" ? userById.district : "")

    const [countriesData, setCountriesData] = useState(countryList)
    const [citiesData, setCitiesData] = useState("")
    const [districtsData, setDistrictsData] = useState("")

    useEffect(() => {
        getDataForm({
            customer: {
                fullName,
                phone,
                email,
                address,
                city,
                country,
                district,
            },
            note: note
        })
    }, [fullName, email, phone, address, note, district, city, country])

    //lấy dữ liệu select country
    const getSelectCountry = (e) => {
        var countryName = e.target.value;
        setCountry(countryName)

    }

    //lấy dữ liệu select thành phố
    const getSelectCity = (e) => {
        var cityName = e.target.value;
        setCity(cityName)

    }

    //lấy dữ liệu select quận/ huyện
    const getSelectDistrict = (e) => {
        var districtName = e.target.value;
        setDistrict(districtName);
    }

    useEffect(() => {
        if (country === "Việt Nam") {
            setCitiesData(cityList)
        } else {
            setCity("");
            setCitiesData("")
        }

        //lấy dữ liệu các quận/ huyện thành phố tương ứng
        if (citiesData !== "") {
            var cityData = "";
            cityData = citiesData.find(cityData => cityData.name === city);

            if (cityData !== undefined) {
                setDistrictsData(cityData.districts);
            } else {
                setDistrict("");
                setDistrictsData("");
            }
        }

    }, [citiesData, districtsData, country, city, district])

    return (
        <Grid container justifyContent="center" flexDirection="column" border="1px solid #E2E6E9" borderRadius="5px" mt={3} p={3}>
            <Grid item mb={1}>
                <Typography component="div" variant="h6">Thông tin đặt hàng</Typography>
            </Grid>
            <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
            <Box component="form"
                noValidate
                autoComplete="off"
                my={2}
            >
                <Grid item my={2}>
                    <Typography component="div" fontWeight={700} mb={1}>Họ và tên <Typography component="span" color="red">*</Typography></Typography>
                    <Typography
                        defaultValue={fullName}
                        component="input"
                        type="text"
                        autoComplete="off"
                        border="1px solid #515151"
                        borderRadius="20px"
                        padding="5px 10px"
                        width="100%"
                        onChange={(e) => { setFullName(e.target.value) }}
                    />

                </Grid>
                <Grid item my={2}>
                    <Typography component="div" fontWeight={700} mb={1}>Số điện thoại <Typography component="span" color="red">*</Typography></Typography>
                    <Typography
                        defaultValue={phone}
                        component="input"
                        type="text"
                        autoComplete="off"
                        border="1px solid #515151"
                        borderRadius="20px"
                        padding="5px 10px"
                        width="100%"
                        onChange={(e) => { setPhone(e.target.value) }}
                    />

                </Grid>
                <Grid item my={2}>
                    <Typography component="div" fontWeight={700} mb={1}>Email <Typography component="span" color="red">*</Typography></Typography>
                    <Typography
                        defaultValue={email}
                        component="input"
                        type="text"
                        autoComplete="off"
                        border="1px solid #515151"
                        borderRadius="20px"
                        padding="5px 10px"
                        width="100%"
                        onChange={(e) => { setEmail(e.target.value) }}
                    />

                </Grid>

                <Grid item my={2}>
                    <Grid container>
                        <Grid item mr={2}>
                            <Typography component="div" fontWeight={700} mb={1}>Quốc gia <Typography component="span" color="red">*</Typography></Typography>
                            <Typography
                                defaultValue={country}
                                component="select"
                                border="1px solid #515151"
                                borderRadius="20px"
                                padding="5px 10px"
                                width="200px"
                                onChange={getSelectCountry}
                            >
                                <option value="">--Lựa chọn--</option>
                                {
                                    countriesData !== ""
                                        ? countriesData.map((country, i) => {
                                            return <option value={country.name} key={i}>
                                                {country.name}
                                            </option>
                                        })
                                        : false
                                }

                            </Typography>
                        </Grid>
                        <Grid item mr={2}>
                            <Typography component="div" fontWeight={700} mb={1}>Tỉnh/Thành phố <Typography component="span" color="red">*</Typography></Typography>
                            <Typography
                                component="select"
                                border="1px solid #515151"
                                borderRadius="20px"
                                padding="5px 10px"
                                width="200px"
                                defaultValue={city}
                                onChange={getSelectCity}
                            >
                                <option value="">--Lựa chọn--</option>
                                {
                                    citiesData !== ""
                                        ? citiesData.map((city, i) => {
                                            return <option value={city.name} key={i}>{city.name}</option>
                                        })
                                        : false
                                }
                            </Typography>
                        </Grid>
                        <Grid item mr={2}>
                            <Typography component="div" fontWeight={700} mb={1}>Quận/Huyện <Typography component="span" color="red">*</Typography></Typography>
                            <Typography
                                component="select"
                                border="1px solid #515151"
                                borderRadius="20px"
                                padding="5px 10px"
                                width="200px"
                                defaultValue={district}
                                onChange={getSelectDistrict}
                            >
                                <option value="">--Lựa chọn--</option>
                                {
                                    districtsData !== ""
                                        ? districtsData.map((district, i) => {
                                            return <option value={district.name} key={i}>{district.name}</option>
                                        })
                                        : false

                                }
                            </Typography>
                        </Grid>
                    </Grid>

                </Grid>

                <Grid item my={2}>
                    <Typography component="div" fontWeight={700} mb={1}>Địa chỉ <Typography component="span" color="red">*</Typography></Typography>
                    <Typography
                        component="input"
                        type="text"
                        autoComplete="off"
                        border="1px solid #515151"
                        borderRadius="20px"
                        padding="5px 10px"
                        width="100%"
                        defaultValue={address}
                        onChange={(e) => { setAddress(e.target.value) }}
                    />

                </Grid>
                <Grid item my={2}>
                    <Typography component="div" fontWeight={700} mb={1}>Ghi chú</Typography>
                    <Typography
                        component="textarea"
                        type="text"
                        autoComplete="off"
                        border="1px solid #515151"
                        borderRadius="20px"
                        padding="5px 10px"
                        width="100%"
                        rows={4}
                        sx={{ resize: "none" }}
                        onChange={(e) => { setNote(e.target.value) }}
                    />

                </Grid>
            </Box>
        </Grid>
    )
}

export default OrderFormInfo;