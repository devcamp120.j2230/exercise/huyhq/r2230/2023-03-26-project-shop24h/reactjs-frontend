import { Grid, Typography } from "@mui/material"
import { formatCurrency } from "../../../helper/FormatCurrency";

const OrderItem = (props) => {
    const product = props.product;
    const img = product ? JSON.parse(product.imageUrl) : [];
    
    return (
        <Grid container flexDirection="row" my={1}>
            <Grid item xs={4} sm={2} md={2}>
                <Typography
                    maxWidth={{xs:"80px", md:"50px"}}
                    component="img"
                    src={img[0]}
                />
            </Grid>
            <Grid item xs={8} sm={7} md={7}>
                <Grid container flexDirection="column">
                    <Grid item>
                        <Typography component="p"
                            sx={{
                                width: {xs:"180px", md:"200px"},
                                whiteSpace: "nowrap",
                                overflow: "hidden",
                                textOverflow: "ellipsis"
                            }}
                        >
                            {product.name}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography component="div">Phân loại: {product.type.name}</Typography>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} sm={3} md={3}>
                <Grid container flexDirection="column" textAlign="right">
                    <Grid item>
                        <Typography component="div">SL: {product.quantity}</Typography>
                    </Grid>
                    <Grid item>
                        <Typography component="div">{formatCurrency(product.buyPrice * product.quantity)}</Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default OrderItem;