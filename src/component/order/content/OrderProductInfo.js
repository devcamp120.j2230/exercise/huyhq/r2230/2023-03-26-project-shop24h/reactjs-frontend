import { Divider, Grid, Typography, Button } from "@mui/material"
import OrderItem from "./OrderItem";
import { formatCurrency } from "../../../helper/FormatCurrency";
import { useSelector } from "react-redux";
import { useEffect } from "react";

const OrderProductInfo = (props) => {
    const orderList = props.orderList;

    const { costShip } = useSelector((reduxData) => {
        return reduxData.orderReducer
    });

    useEffect(() => {

    }, [costShip]);

    return (
        <>
            <Grid item mb={1}>
                <Typography component="span" variant="h6">Đơn hàng</Typography>
                <Button variant="contained" sx={{ float: "right", background: "#515151", color: "#ffff", borderRadius: "20px" }} href="/cart">Sửa</Button>
            </Grid>
            <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
            <Grid item my={1}>
                {
                    orderList.products.map((product, i) => {
                        return <OrderItem product={product} key={i} />
                    })
                }
            </Grid>
            <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
            <Grid item my={1}>
                <Grid container flexDirection="row">
                    <Grid item xs={6}>
                        <Typography component="div">Tạm tính: </Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="right">
                        <Typography component="div">{formatCurrency(orderList.subtotal)}</Typography>
                    </Grid>
                </Grid>
                <Grid container flexDirection="row">
                    <Grid item xs={6}>
                        <Typography component="div">Giảm giá: </Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="right">
                        <Typography component="div">- {formatCurrency(orderList.discount)}</Typography>
                    </Grid>
                </Grid>
                <Grid container flexDirection="row">
                    <Grid item xs={6}>
                        <Typography component="div" >Vận chuyển: </Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="right">
                        <Typography component="div">+ {formatCurrency(costShip)}</Typography>
                    </Grid>
                </Grid>
                <Grid container flexDirection="row">
                    <Grid item xs={6}>
                        <Typography component="div">Tổng tiền: </Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="right">
                        <Typography component="div" fontWeight={700}>{formatCurrency(orderList.total + costShip)}</Typography>
                    </Grid>
                </Grid>
            </Grid>
        </>
    )
}

export default OrderProductInfo;