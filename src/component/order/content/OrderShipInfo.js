import { Divider, Grid, Typography, FormControl, RadioGroup, Radio, FormControlLabel } from "@mui/material"
import { useState } from "react";
import { useDispatch } from "react-redux";
import { getShipAction } from "../../../action/order.action";

const OrderShipInfo = () => {
    const [ship, setShip] = useState('atm');

    const dispatch = useDispatch();

    const handleChangeShip = (e) => {
        setShip(e.target.value);

        if (e.target.value == 'cod') {
            dispatch(getShipAction(30000))
        } else if (e.target.value == 'atm') {
            dispatch(getShipAction(0))
        }
    };

    return (
        <Grid container justifyContent="center" flexDirection="column" border="1px solid #E2E6E9" borderRadius="5px" p={3} mt={3}>
            <Grid item mb={1}>
                <Typography component="div" variant="h6">Phương thức thanh toán</Typography>
            </Grid>
            <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
            <FormControl>
                <RadioGroup
                    name="radio-buttons-group"
                    onChange={handleChangeShip}
                    defaultValue={ship}
                >
                    <Grid item mt={1}>
                        <FormControlLabel value='atm' control={<Radio />} label="Thanh toán bằng thẻ ngân hàng." />
                    </Grid>
                    <Grid item mt={1}>
                        <FormControlLabel value='cod' control={<Radio />} label="Thanh toán khi nhận hàng (COD)." />
                    </Grid>

                </RadioGroup>
            </FormControl >
        </Grid>
    )
}

export default OrderShipInfo;