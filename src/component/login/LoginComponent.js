import { Container, Grid, Typography } from "@mui/material";

import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import LoginForm from "./content/LoginForm";
import LoginGoogle from "./content/LoginGoogle";

const LoginComponent = () => {
    const navigate = useNavigate();					//chuyển hướng sang route khác

    const { user } = useSelector((reduxData) => {
        return reduxData.userReducer;
    });

    //lưu trữ thông tin đăng nhập
    useEffect(() => {
        if (user) {
            navigate("/");
        }
    }, [user])

    return (
        <Container maxWidth={false} disableGutters>
            {
                user !== null
                    ? <div>
                        <h1>Hello {user.displayName}</h1>
                    </div>
                    : <Grid container justifyContent="center" py="100px">
                        <Grid
                            item xs={10} sm={10} md={6} lg={4}
                            sx={{ background: "#DFE7E9" }}
                            textAlign="center"
                            p="20px"
                        >
                            <Grid item>
                                <Typography component="div" variant="h5" textTransform="uppercase" my={2}>Đăng nhập</Typography>
                            </Grid>
                            <Grid container
                                sx={{ background: "#ffff" }}
                            >
                                {/* <Grid item xs={12} my={3} px={3}>
                                    <LoginGoogle/>
                                </Grid> */}
                                {/* <Grid item xs={12}>
                                    <hr />
                                </Grid> */}
                                <LoginForm />
                            </Grid>

                        </Grid>
                    </Grid>
            }

        </Container>
    )
}

export default LoginComponent;