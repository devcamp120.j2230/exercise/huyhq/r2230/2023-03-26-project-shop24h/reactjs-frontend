import { Container, Grid, Button, Typography } from "@mui/material";

import { GoogleAuthProvider, signOut } from 'firebase/auth';
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import auth from "../../firebase/config";
import RegisterForm from "./content/RegisterForm";

const RegisterComponent = () => {
    const navigate = useNavigate();					//chuyển hướng sang route khác
    // const dispatch = useDispatch();

    const { user } = useSelector((reduxData) => {
        return reduxData.userReducer;
    });

    // //chức năng log out
    // const logOutGoogle = () => {
    //     signOut(auth)
    //         .then(() => {
    //             dispatch({
    //                 type: "LOGOUT",
    //             });
    //         })
    //         .catch((error) => {
    //             console.error(error);
    //         })
    // }

    //lưu trữ thông tin đăng nhập
    useEffect(() => {
        if (user) {
            navigate("/");
        }
    }, [user])

    return (
        <Container maxWidth={false} disableGutters>
            {
                user !== null
                    ? <div>
                        <h1>Hello {user.displayName}</h1>
                        {/* <Button variant="contained" onClick={logOutGoogle}>Log Out</Button> */}
                    </div>
                    : <Grid container justifyContent="center" py="100px">
                        <Grid
                            item xs={10} sm={10} md={6} lg={4}
                            sx={{ background: "#DFE7E9" }}
                            textAlign="center"
                            p="20px"
                        >
                            <Grid item>
                                <Typography component="div" variant="h5" textTransform="uppercase" my={2}>Đăng ký</Typography>
                            </Grid>
                            <Grid container
                                sx={{ background: "#ffff" }}
                            >
                                <RegisterForm />
                            </Grid>

                        </Grid>
                    </Grid>
            }

        </Container>
    )
}

export default RegisterComponent;