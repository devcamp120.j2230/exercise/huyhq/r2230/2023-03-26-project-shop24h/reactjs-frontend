import { Button, Typography } from "@mui/material";

import { faGoogle } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { signInWithPopup, GoogleAuthProvider, signOut, onAuthStateChanged } from 'firebase/auth';
import auth from "../../../firebase/config";
import { useDispatch, useSelector } from "react-redux";

const provider = new GoogleAuthProvider();

const LoginGoogle = () => {
    const dispatch = useDispatch();

    //chức năng đăng nhập
    const logInGoogle = () => {
        dispatch({
            type: "LOGIN_PENDING",
        });
        signInWithPopup(auth, provider)
            .then((result) => {
                console.log(result.user);
                dispatch({
                    type: "LOGIN",
                    data: result.user
                });
            })
            .catch((error) => {
                console.error(error);
            })
    }

    //chức năng log out
    const logOutGoogle = () => {
        signOut(auth)
            .then(() => {
                dispatch({
                    type: "LOGOUT",
                });
            })
            .catch((error) => {
                console.error(error);
            })
    }

    return (
        <>
            <Button
                variant="contained"
                gap={2}
                sx={{
                    minWidth: "300px",
                    color: "#ffff",
                    background: "#DE4830",
                    borderRadius: "20px"
                }}
                onClick={logInGoogle}
            >
                <Typography variant="p" component="div" mx={1}>
                    <FontAwesomeIcon icon={faGoogle} />
                </Typography>
                <Typography variant="p" component="div" mx={1}>Sigin with Google</Typography>
            </Button>
        </>
    )
}

export default LoginGoogle;