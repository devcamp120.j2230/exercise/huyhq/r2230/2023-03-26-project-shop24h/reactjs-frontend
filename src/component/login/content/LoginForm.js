import { Grid, Button, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../../action/authen.action";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import ModalAlertNotification from "../../alert/ModalAlertNotification";
import { validateEmail } from "../../../helper/ValidateEmail";

const LoginForm = () => {
    const [openAlert, setOpenAlert] = useState(false);
    const [message, setMessage] = useState("");
    const [status, setStatus] = useState("warning");

    const [email, setEmail] = useState("huyqh4@gmail");
    const [password, setPassword] = useState("huy12345");
    const navigate = useNavigate();

    const dispatch = useDispatch();

    const { pending, statusLogin } = useSelector((reduxData) => {
        return reduxData.userReducer;
    });

    const onInputEmailChange = (e) => {
        setEmail(e.target.value)
    }

    const onInputPasswordChange = (e) => {
        setPassword(e.target.value)
    }

    useEffect(() => {
        setStatus(statusLogin);

        if(statusLogin === "success"){
            setMessage("Đăng nhập thành công.");
            setOpenAlert(true);
            setTimeout(()=>{navigate(0)}, 1000);
        }

        if(statusLogin === "error"){
            setMessage("Sai email hoặc mật khẩu. Hãy thử lại!");
            setOpenAlert(true);
        }

    }, [pending, statusLogin]);

    //click Button Login
    const onClickBtnLogin = () => {
        const check = validateForm(email, password);
        if (check) {
            const data = {
                email,
                password
            };

            dispatch(loginAction(data))
        }else{
            setStatus("warning");
            setOpenAlert(true);
        }

    }

    //validate Form
    const validateForm = (email, password) => {
        if (email === "") {
            setMessage("Email không được để trống!");
            return false;
        }

        if (!validateEmail(email)) {
            setMessage("Email không đúng định dạng.");
            return false;
        }

        if (password === "") {
            setMessage("Password không được để trống!");
            return false;
        }

        return true;
    }

    return (
        <>
            <ModalAlertNotification
                state={status}
                status={openAlert}
                message={message}
                getStatus={(val) => { setOpenAlert(val) }}
            />
            <Grid item xs={12} my={3} px={3}>
                <TextField placeholder="Email" fullWidth sx={{ borderRadius: "20px" }} defaultValue={email} onChange={onInputEmailChange} />
            </Grid>
            <Grid item xs={12} my={3} px={3}>
                <TextField placeholder="Password" type="password" fullWidth sx={{ borderRadius: "20px" }} defaultValue={password} onChange={onInputPasswordChange} />
            </Grid>
            <Grid item xs={12} my={1} px={3}>
                <Button
                    variant="contained"
                    fullWidth
                    sx={{
                        color: "#ffff",
                        background: "#000",
                        borderRadius: "20px"
                    }}
                    onClick={onClickBtnLogin}
                >
                    Đăng nhập
                </Button>
            </Grid>
            <Grid item xs={12} mb={3} px={2}>
                <Typography component="div">Nếu bạn chưa có tài khoản. &nbsp;
                    <Typography component="a" href="/register" color="blue">Hãy đăng ký tài khoản mới</Typography>
                </Typography>
            </Grid>
        </>
    )
}

export default LoginForm;