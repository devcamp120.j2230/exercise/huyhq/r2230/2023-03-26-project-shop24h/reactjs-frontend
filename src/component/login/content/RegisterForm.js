import { Grid, Button, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { validateEmail } from "../../../helper/ValidateEmail";
import ModalAlertNotification from "../../alert/ModalAlertNotification";
import { useDispatch, useSelector } from "react-redux";
import { registerAction } from "../../../action/authen.action";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const RegisterForm = () => {
    const [openAlert, setOpenAlert] = useState(false);
    const [message, setMessage] = useState("");
    const [status, setStatus] = useState("warning");

    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    const [repassword, setRepassword] = useState("");

    const navigate = useNavigate();

    const dispatch = useDispatch();

    const { statusRegister } = useSelector((reduxData) => {
        return reduxData.userReducer;
    });

    //click button register
    const clickRegisterForm = () => {
        var formData = {
            fullName, email, password, repassword, phone
        }

        if (validateForm(formData)) {
            var data = {
                fullName, email, password, phone
            }
            //send Api
            dispatch(registerAction(data));
        } else {
            setStatus("warning");
            setOpenAlert(true);
        }
    }
    //validate form
    const validateForm = (data) => {
        if (data.fullName === "") {
            setMessage("Họ tên không được để trống.");
            return false;
        }

        if (data.email === "") {
            setMessage("Email không được để trống.");
            return false;
        }

        if (!validateEmail(data.email)) {
            setMessage("Email không đúng định dạng.");
            return false;
        }

        if (data.password === "") {
            setMessage("Mật khẩu không được để trống.");
            return false;
        }

        if (data.password !== data.repassword) {
            setMessage("Mật khẩu xác nhận không khớp.");
            return false;
        }


        if (data.phone === "") {
            setMessage("Số điện thoại không được để trống.");
            return false;
        }

        return true;
    }

    useEffect(()=>{
        setStatus(statusRegister);

        if(statusRegister === "success"){
            setMessage("Tạo mới người dùng thành công. Hãy đăng nhập!");
            setOpenAlert(true);

            setTimeout(()=>{navigate("/login")}, 1000);
        }

        if(statusRegister === "error"){
            setMessage("Có lỗi khi tạo mới người dùng. Email hoặc SĐT đã tồn tại. Hãy thử lại!");
            setOpenAlert(true)
        }
    },[statusRegister])
    return (
        <>
            <>
                <ModalAlertNotification
                    state={status}
                    status={openAlert}
                    message={message}
                    getStatus={(val) => { setOpenAlert(val) }}
                />
            </>
            <Grid item xs={12} my={1} px={3}>
                <TextField placeholder="Họ và tên" defaultValue={fullName} fullWidth sx={{ borderRadius: "20px" }} onChange={(e) => { setFullName(e.target.value) }} />
            </Grid>
            <Grid item xs={12} my={1} px={3}>
                <TextField placeholder="Email" defaultValue={email} fullWidth sx={{ borderRadius: "20px" }} onChange={(e) => { setEmail(e.target.value) }} />
            </Grid>
            <Grid item xs={12} my={1} px={3}>
                <TextField placeholder="Mật khẩu" type="password" fullWidth sx={{ borderRadius: "20px" }} onChange={(e) => { setPassword(e.target.value) }} />
            </Grid>
            <Grid item xs={12} my={1} px={3}>
                <TextField placeholder="Mật khẩu xác nhận" type="password" fullWidth sx={{ borderRadius: "20px" }} onChange={(e) => { setRepassword(e.target.value) }} />
            </Grid>
            <Grid item xs={12} my={1} px={3}>
                <TextField placeholder="Số điện thoại" defaultValue={phone} fullWidth sx={{ borderRadius: "20px" }} onChange={(e) => { setPhone(e.target.value) }} />
            </Grid>
            <Grid item xs={12} my={1} px={2}>
                <Button
                    variant="contained"
                    fullWidth
                    sx={{
                        color: "#ffff",
                        background: "#000",
                        borderRadius: "20px"
                    }}
                    onClick={clickRegisterForm}
                >
                    Đăng ký
                </Button>
            </Grid>
            <Grid item xs={12} mb={3} px={3}>
                <Typography component="div">Nếu bạn đã có tài khoản. &nbsp;
                    <Typography component="a" href="/login" color="blue">Hãy đăng nhập để sử dụng.</Typography>
                </Typography>
            </Grid>
        </>
    )
}

export default RegisterForm;