import { Grid, Typography, Button, TextField } from "@mui/material";
import { useState } from "react";
import { discountList } from "../../../data/discount.data";
import { useDispatch } from "react-redux";
import { getDiscountAction } from "../../../action/cart.action";

const CartDiscountField = () => {
    const [discountCode, setDiscountCode] = useState("")
    const [checkCode, setCheckCode] = useState("")
    const [message, setMessage] = useState("")

    const dispatch = useDispatch();

    //on input change
    const onInputDiscountCode = (e) => {
        setDiscountCode(e.target.value);
    }

    //click button add discount code
    const onClickBtnAddDiscount = () => {
        var discountPercent = 0;
        if (validateDiscount(discountCode)) {
            //check mã giảm giá, lấy phần trăm giảm giá
            discountList.find((elm) => {
                if (elm.code == discountCode) {
                    discountPercent = elm.discount
                }
            })
        }

        //xử lý thông tin giảm giá, show thông báo
        if(discountPercent <= 0){
            setMessage("Mã giảm giá không đúng.")
            setCheckCode(false);
            dispatch(getDiscountAction(0))
        }else{
            setMessage(`Bạn được giảm giá ${discountPercent}%`)
            setCheckCode(true);
            dispatch(getDiscountAction(discountPercent))
        }
    }

    //validate form
    const validateDiscount = (val) => {
        if (val === "") {
            setMessage("Mã giảm giá đang để trống.")
            return false;
        }
        return true
    }
    return (
        <Grid container>
            <Grid item xs={12} my={3}>
                <Typography component="div" variant="p" fontWeight={700}>Mã giảm giá</Typography>
            </Grid>
            <Grid item xs={12} sm={6} pr={3}>
                <TextField placeholder="Nhập mã" onChange={onInputDiscountCode} fullWidth/>
            </Grid>
            <Grid item xs={12} sm={6} pt={1}>
                <Button variant="contained" sx={{ background: "black" }} onClick={onClickBtnAddDiscount}>Thêm</Button>
            </Grid>
            <Grid item xs={12} my={1}>
                {
                    checkCode !== ""
                        ? checkCode
                            ? <Typography component="div" variant="p" color="blue">({message})</Typography>
                            : <Typography component="div" variant="p" color="red">({message})</Typography>
                        : false
                }

            </Grid>
        </Grid>
    )
}

export default CartDiscountField;