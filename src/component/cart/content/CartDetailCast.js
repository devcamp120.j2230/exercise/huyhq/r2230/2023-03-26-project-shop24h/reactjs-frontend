import { Grid, Typography, Button } from "@mui/material";
import React, { useState } from "react";
import { formatCurrency } from "../../../helper/FormatCurrency";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getOrderAction } from "../../../action/order.action";
import { useEffect } from "react";

const CartDetailCast = (props) => {
    const [cartList, setCartList] = useState(JSON.parse(localStorage.getItem("cart")));
    const [cartCheckCount, setCartCheckCount] = useState(0);

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const { discount, subtotal } = useSelector((reduxData) => {
        return reduxData.cartReducer;
    })

    //click button để sang trang Order
    const clickBtnToCheckout = () => {
        //lấy dữ liệu product check
        var cartCheck = [];
        cartCheck = cartList.filter(cart => cart.check);
        var message = "";
        if (cartCheck.length > 0) {
            if (subtotal > 0) {
                const order = {
                    products: cartCheck,
                    subtotal: subtotal,
                    discount: discount,
                    total: subtotal - discount,
                };
                //gửi dữ liệu order
                dispatch(getOrderAction(order))
                navigate("/order")
            } else {
                message = "Chưa có sản phẩm được chọn hoặc chưa cập nhật lại đơn hàng. Hãy thử lại!";
                props.getMessage("error", message);
            }
        } else {
            message = "Chưa có sản phẩm được chọn hoặc chưa cập nhật lại đơn hàng. Hãy thử lại!";
            props.getMessage("error", message);
        }

    }

    //lấy tổn sản phẩm được chọn
    useEffect(() => {
        var cartCheck = [];
        cartList.length > 0
            ? cartCheck = cartList.filter(cart => cart.check)
            : cartCheck = []
        setCartCheckCount(cartCheck.length);
    }, [subtotal, discount])

    return (
        <Grid container alignContent="center" flexDirection="column" bgcolor="#F0F0F0" px={10} py={3}>
            <Typography component="div" variant="p" fontWeight={700}>Giỏ hàng: ({cartCheckCount} sản phẩm)</Typography>
            <Grid container my={2}>
                <Grid item xs={8} >
                    <Typography component="div" variant="p" fontWeight={700}>Tạm tính</Typography>
                </Grid>
                <Grid item xs={4} textAlign="right">
                    <Typography component="div" variant="p" fontWeight={700} color="error.main">{formatCurrency(subtotal)}</Typography>
                </Grid>
            </Grid>
            <Grid container my={2}>
                <Grid item xs={8} >
                    <Typography component="div" variant="p" fontWeight={700}>Giảm giá: </Typography>
                </Grid>
                <Grid item xs={4} textAlign="right">
                    <Typography component="div" variant="p" fontWeight={700} color="error.main">- {formatCurrency(discount)}</Typography>
                </Grid>
            </Grid>
            <Grid container my={2}>
                <Grid item xs={8} >
                    <Typography component="div" variant="p" fontWeight={700}>Tổng: </Typography>
                </Grid>
                <Grid item xs={4} textAlign="right">
                    <Typography component="div" variant="p" fontWeight={700} color="error.main">{formatCurrency(subtotal - discount)}</Typography>
                </Grid>
            </Grid>
            <Button variant="contained" sx={{ background: "#000" }} onClick={clickBtnToCheckout}>Trang thanh toán</Button>
        </Grid>
    )
}

export default CartDetailCast;