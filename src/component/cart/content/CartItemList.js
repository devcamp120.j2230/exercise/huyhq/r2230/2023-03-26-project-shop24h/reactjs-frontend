import { TableContainer, TableHead, TableRow, TableCell, TableBody, Paper, Table, Typography, makeStyles } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSubtotalCart } from "../../../action/cart.action";
import CartItem from "./CartItem";

const useStyles = {
    // tableLayout: "fixed",
    whiteSpace: "nowrap",
    overflow: "auto",
    // minWidth: 650,
    "& tbody": {
        // whiteSpace: "nowrap",
        // overflow: "auto",
    },
    "& .MuiTableRow-root": {
        // whiteSpace: "nowrap",
        // minWidth: "600px",
        overflowX: "auto",
        border: "1px solid #ddd"
    },
    "& .MuiTableCell-root": {
        borderLeft: "1px solid #ddd"
    }
};

const CartItemList = (props) => {
    const dispatch = useDispatch();

    const { cart } = useSelector((reduxData) => {
        return reduxData.cartReducer;
    })

    useEffect(() => {
        var total = 0;

        cart.map((item, i) => {
            if (item.check) {
                total = total + item.buyPrice * item.quantity;
                return true;
            } else {
                return false;
            }
        });
        dispatch(getSubtotalCart(total));
    }, [cart])

    return (
        <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
            <Table sx={useStyles} aria-label="simple table" >
                <TableHead >
                    <TableRow sx={{display:{xs: "none", md: "table-row"}}}>
                        <TableCell align="center" sx={{ fontWeight: 700 }}></TableCell>
                        <TableCell align="left" sx={{ fontWeight: 700 }}>Sản phẩm</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 700 }}>Giá</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 700 }}>Số lượng</TableCell>
                        <TableCell align="center" sx={{ fontWeight: 700 }}>Thành tiền</TableCell>
                    </TableRow>
                    <TableRow sx={{display:{xs: "table-row", md: "none"}}}>
                        <TableCell align="center" sx={{ fontWeight: 700 }}></TableCell>
                        <TableCell align="left" sx={{ fontWeight: 700}}  colSpan={4}>Sản phẩm</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        cart.length <= 0
                            ? <Typography component="div" ml={10}>Chưa có sản phẩm trong giỏ hàng.</Typography>
                            : cart.map((item, i) => {
                                return <React.Fragment key={i}>
                                    <CartItem
                                        position={i}
                                        item={item}
                                        getQuantityProduct={props.getQuantityProduct}
                                        getCheckProduct={props.getCheckProduct}
                                        deleteProduct={props.deleteProduct}
                                        check={item.check}
                                    />
                                </React.Fragment>
                            })
                    }
                </TableBody>
            </Table>
        </TableContainer >
    )
}
export default CartItemList