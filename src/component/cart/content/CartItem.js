import { faTrashCan } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { TableRow, TableCell, Typography, FormGroup, FormControlLabel, Checkbox, Button, Grid, IconButton, Collapse, Box, Paper, Table, TableHead, TableContainer, TableBody } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { formatCurrency } from "../../../helper/FormatCurrency";
import { KeyboardArrowDown, KeyboardArrowUp } from "@mui/icons-material";

const CartItem = (props) => {
    // const dispatch = useDispatch();

    // const { subtotal } = useSelector((reduxData) => {
    //     return reduxData.cartReducer;
    // })

    const [openCollapse, setOpenCollapse] = useState(true);

    const item = props.item;
    const img = item ? JSON.parse(item.imageUrl) : []
    const [quantity, setQuantity] = useState(item.quantity);

    const [price, setPrice] = useState(item.buyPrice);

    const [total, setTotal] = useState(price * quantity);

    //click tăng số lượng
    const onClickButtonIncrease = () => {
        var val = quantity;
        val = val + 1;
        setQuantity(val);
    }
    //click giảm số lượng
    const onClickButtonDicrease = () => {
        var val = quantity;

        if (val <= 1) {
            val = 1
        } else {
            val = val - 1;
        }
        setQuantity(val);
    }

    //check box
    const onCheckbox = (e) => {
        props.getCheckProduct(props.position, e.target.checked);
    }

    //remove product 
    const onClickRemoveProduct = () => {
        props.deleteProduct(props.position)
    }

    //thay đổi giá khi tăng giảm số lượng
    useEffect(() => {
        setTotal(price * quantity);
        props.getQuantityProduct(props.position, quantity);
    }, [quantity])


    return (
        <>
            <TableRow
                sx={{
                    display: { xs: "none", md: "table-row" },
                    '&:last-child td, &:last-child th': { border: "1px solid #ddd" },
                    '&:hover': { background: "#F4F5F4" }
                }}
            >
                <TableCell scope="row" align="center">
                    <FormGroup sx={{ paddingLeft: "16px", '& .MuiFormControlLabel-root': { margin: 0, width: "fit-content" } }}>
                        {
                            props.check
                                ? <FormControlLabel control={<Checkbox onChange={onCheckbox} defaultChecked />} />
                                : <FormControlLabel control={<Checkbox onChange={onCheckbox} />} />
                        }
                    </FormGroup>
                </TableCell>
                <TableCell align="left">
                    <Grid container alignItems="center" minWidth={300}>
                        <Grid item xs={2}>
                            <Typography
                                component="img"
                                width="100%"
                                maxWidth="80px"
                                sx={{ objectFit: "cover" }}
                                mr={1}
                                src={img[0]}
                            />
                        </Grid>
                        <Grid item xs={10}>
                            <Grid container flexDirection="column" justifyContent='flex-start' ml={1}>
                                <Typography
                                    component="div"
                                    display="inline-block"
                                    sx={{
                                        width: "100%",
                                        whiteSpace: "wrap",
                                        overflow: "auto",
                                        textOverflow: "ellipsis"
                                    }}
                                >
                                    {item.name}
                                </Typography>
                                <Button onClick={onClickRemoveProduct} sx={{ width: "fit-content" }}>
                                    <FontAwesomeIcon icon={faTrashCan} size="lg" color="#cacaca" />
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </TableCell>
                <TableCell align="center">{formatCurrency(price)}</TableCell>
                <TableCell align="center">
                    <Typography
                        component="button"
                        p={1}
                        sx={{ border: "none", minWidth: "40px" }}
                        onClick={onClickButtonDicrease}

                    >
                        -
                    </Typography>
                    <Typography component="button" p={1} sx={{ border: "none", minWidth: "40px" }}>{quantity}</Typography>
                    <Typography
                        component="button"
                        p={1}
                        sx={{ border: "none", minWidth: "40px" }}
                        onClick={onClickButtonIncrease}
                    >
                        +
                    </Typography>
                </TableCell>
                <TableCell align="center">{formatCurrency(total)}</TableCell>
            </TableRow>
            <TableRow sx={{ display: { xs: "table-row", md: "none" } }}>
                <TableCell scope="row" align="center">
                    <FormGroup sx={{ paddingLeft: "5px", '& .MuiFormControlLabel-root': { margin: 0, width: "fit-content" } }}>
                        {
                            props.check
                                ? <FormControlLabel control={<Checkbox onChange={onCheckbox} defaultChecked />} />
                                : <FormControlLabel control={<Checkbox onChange={onCheckbox} />} />
                        }
                    </FormGroup>
                </TableCell>
                <TableCell colSpan={4}>
                    <Collapse in={openCollapse} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }} maxWidth={240}>
                            <Grid container flexDirection="row" justifyContent="flex-start" alignItems="center">
                                <Grid item xs={2}>
                                    <Typography
                                        component="img"
                                        width="100%"
                                        maxWidth="80px"
                                        sx={{ objectFit: "cover" }}
                                        mr={1}
                                        src={img[0]}
                                    />
                                </Grid>
                                <Grid item xs={10}>
                                    <Grid container flexDirection="column" flexWrap="wrap" justifyContent='flex-start' ml={1}>
                                        <Typography
                                            component="div"
                                            display="inline-block"
                                            sx={{
                                                width: {xs:"100%", md: "100%"},
                                                whiteSpace: "wrap",
                                                overflow: "hidden",
                                                textOverflow: "ellipsis"
                                            }}
                                        >
                                            {item.name}
                                        </Typography>
                                        <Grid container flexDirection="column" my={1}>
                                            <Grid item>Đơn giá: {formatCurrency(price)}</Grid>
                                            <Grid item>
                                                <Typography
                                                    my={1}
                                                    component="button"
                                                    p={1}
                                                    sx={{ border: "none", minWidth: "40px" }}
                                                    onClick={onClickButtonDicrease}

                                                >
                                                    -
                                                </Typography>
                                                <Typography my={1} component="button" p={1} sx={{ border: "none", minWidth: "40px" }}>{quantity}</Typography>
                                                <Typography
                                                    my={1}
                                                    component="button"
                                                    p={1}
                                                    sx={{ border: "none", minWidth: "40px" }}
                                                    onClick={onClickButtonIncrease}
                                                >
                                                    +
                                                </Typography>
                                            </Grid>
                                            <Grid item>Thành tiền: {formatCurrency(total)}</Grid>
                                        </Grid>
                                        <Button onClick={onClickRemoveProduct} sx={{ width: "fit-content" }}>
                                            <FontAwesomeIcon icon={faTrashCan} size="lg" color="#cacaca" />
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    )
}

export default CartItem