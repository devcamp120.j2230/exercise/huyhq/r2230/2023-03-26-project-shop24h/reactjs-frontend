import { Grid, Typography } from "@mui/material";
import { Container } from "@mui/system"
import React, { useState } from "react";
import "../../App.css"
import { useLocation, useNavigate } from "react-router-dom";
import CartItemList from "./content/CartItemList";
import DeleteConfirmModal from "../modal/DeleteConfirmModal";
import CartDiscountField from "./content/CartDiscountField";
import CartDetailCast from "./content/CartDetailCast";
import { useDispatch, useSelector } from "react-redux";
import ModalAlertNotification from "../alert/ModalAlertNotification";
import { useEffect } from "react";
import { getAlertState } from "../../action/alert.action";
import BreadCrumb from "../BreadCrumb";

const breadcrumb = [
    {
        name: "Trang chủ",
        url: "/"
    },
    {
        name: "Giỏ hàng",
        url: "#"
    },
]

const CartComponent = () => {
    const dispatch = useDispatch();
    const location = useLocation();

    const [openAlert, setOpenAlert] = useState(false);
    const [message, setMessage] = useState("");
    const [status, setStatus] = useState("");

    const { cart } = useSelector((reduxData) => {
        return reduxData.cartReducer;
    })

    const [cartList, setCartList] = useState(cart);
    const [modalDelete, setModalDelete] = useState(false)

    const navigate = useNavigate();

    //lấy dữ liệu quantity khi tăng giảm quantity
    const getQuantityProduct = (key, val) => {
        var cart = cartList;
        cart[key].quantity = val
        setCartList(cart)
    }

    //lấy thông tin sản phẩm có được chọn
    const getCheckProduct = (key, val) => {
        // console.log(key+": "+val);
        var cart = cartList;
        cart[key].check = val
        setCartList(cart)
    }

    //click button xóa sản phẩm
    const deleteProduct = (key) => {
        var cart = JSON.parse(localStorage.getItem("cart"))
        cart.splice(key, 1);
        localStorage.setItem("cartRemove", JSON.stringify(cart))
        setModalDelete(true);
    }

    //handle confirm delete
    const handleConfirmDelete = (status) => {
        if (status) {
            var newCart = JSON.parse(localStorage.getItem("cartRemove"))
            // console.log(newCart);
            localStorage.setItem("cart", JSON.stringify(newCart))
            var message = "Xóa sản phẩm thành công!";
            setMessage(message);
            setStatus("success");
            setOpenAlert(true);
            navigate(0);
        }
        //tắt modal và xóa item trong local
        setModalDelete(status);
        localStorage.removeItem("cartRemove");
    }

    //click button update cart trong localstorage
    const clickBtnUpdateCart = () => {
        //update dữ liệu cart list trong local storage
        localStorage.setItem("cart", JSON.stringify(cartList));
        var message = "Cập nhật thông tin giỏ hàng thành công!";
        setMessage(message);
        setStatus("success");
        setOpenAlert(true);
        navigate(0);
    }

    return (
        <Container maxWidth={false} disableGutters>
            <Grid container justifyContent="center" flexDirection="column" p={{xs: 1, md:5}}>
                <Grid item my={1}>
                    {
                        openAlert
                            ?
                            <ModalAlertNotification
                                state={status}
                                status={openAlert}
                                message={message}
                                getStatus={(val) => { setOpenAlert(val) }}
                            />
                            : false
                    }
                </Grid>
                <Grid item xs={12} my={3}>
                    <BreadCrumb link={JSON.stringify(breadcrumb)} />
                </Grid>
                <Grid item xs={12}>
                    <Typography component="div" variant="h5" textTransform="uppercase" fontWeight={700} mt={1}>Giỏ hàng</Typography>
                </Grid>
                <Grid item my={3}>
                    <CartItemList
                        getQuantityProduct={getQuantityProduct}
                        getCheckProduct={getCheckProduct}
                        deleteProduct={deleteProduct}
                    />
                </Grid>
                <Grid item my={3}>
                    <Grid container px={1}>
                        <Grid item xs={6} textAlign="left">
                            <Typography component="button" bgcolor="#000" color="#fff" sx={{ border: "none", borderRadius: "20px" }} p={1} onClick={() => { navigate(-1) }}>Tiếp tục mua sắm </Typography>
                        </Grid>
                        <Grid item xs={6} textAlign="right">
                            <Typography component="button" bgcolor="#000" color="#fff" sx={{ border: "none", borderRadius: "20px" }} p={1} onClick={clickBtnUpdateCart}>Cập nhật Giỏ hàng</Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item my={3}>
                    <Grid container justifyContent="center">
                        <Grid item xs={12} sm={12} md={6}>
                            <CartDiscountField />
                        </Grid>
                        <Grid item xs={12} sm={12} md={6}>
                            <CartDetailCast getMessage={(status, message) => {
                                setStatus(status)
                                setMessage(message)
                            }} />
                        </Grid>
                    </Grid>
                </Grid>

                <DeleteConfirmModal
                    setModal={modalDelete}
                    getModalStatus={handleConfirmDelete}
                />
            </Grid>
        </Container>
    )
}
export default CartComponent;