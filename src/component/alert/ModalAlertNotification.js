import { Alert, Collapse, IconButton, Modal } from "@mui/material";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClose } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { useEffect } from "react";

const style = {
    zIndex: 100,
    position: 'fixed',
    top: '10px',
    left: '50%',
    transform: 'translate(-50%, 0)',
};

//{status, state, message, getStatus()}
const ModalAlertNotification = (props) => {
    const [openAlert, setOpenAlert] = useState(props.status);

    const handleCloseAlert = ()=>{
        props.getStatus(false);
        setOpenAlert(false);
    };

    useEffect(() => {
        setOpenAlert(props.status);

        if(openAlert){
            setTimeout(handleCloseAlert, 3000)
        }
        
    }, [props.status]);

    return (
        <Collapse in={openAlert} sx={style}>
            <Alert
                severity={props.state}
                action={
                    <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={handleCloseAlert}
                    >
                        <FontAwesomeIcon icon={faClose} style={{ color: "grey" }} />
                    </IconButton>
                }
                sx={{
                    minWidth: {xs:"90vw", md:"30vw"},
                    mb: 2,
                    borderRadius: "20px"
                }}
            >
                {props.message}
            </Alert>
        </Collapse>
    )
}

export default ModalAlertNotification