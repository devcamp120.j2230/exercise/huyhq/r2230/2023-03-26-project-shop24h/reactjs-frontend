import { Alert, Collapse, IconButton } from "@mui/material";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClose } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { useEffect } from "react";

//{status, state, message, getStatus()}
const AlertNotification = (props) => {
    const [openAlert, setOpenAlert] = useState(props.status);

    useEffect(()=>{
        setOpenAlert(props.status);
    },[props.status]);

    return (
        <Collapse in={openAlert}>
            <Alert
                severity={props.state}
                action={
                    <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                            props.getStatus(false);
                            setOpenAlert(false);
                        }}
                    >
                        <FontAwesomeIcon icon={faClose} style={{ color: "grey" }} />
                    </IconButton>
                }
                sx={{ mb: 2 }}
            >
                {props.message}
            </Alert>
        </Collapse>
    )
}

export default AlertNotification