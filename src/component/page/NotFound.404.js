import { Link } from "react-router-dom";

const NotFound = () => {
    return (
        <div
            style={{
                height: "70vh",
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                textAlign: "center"
            }}>
            <h1>404 Error - Page Not Found!</h1>
            <p>Trang bạn tìm không có, bạn muốn thử những link sau:</p>
            <Link to='/'>Trang chủ</Link>
            <br />
            <Link to='/products'>Trang sản phẩm</Link>
        </div>
    )
};

export default NotFound;

