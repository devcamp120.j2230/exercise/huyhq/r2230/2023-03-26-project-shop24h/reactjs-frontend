import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { checkAuthorization } from "../../action/authorization.action";

const CheckAuth = () => {
    const dispatch = useDispatch();
    const { token, authorization, checkTimeAuthExp, pendingAuth } = useSelector((data) => data.userReducer);

    useEffect(() => {
            dispatch(checkAuthorization(token));
        
    }, [token]);

    return (
        <>
            {
                authorization
                    ? !checkTimeAuthExp ? localStorage.removeItem("user") : false
                    : false
            }
        </>
    )
}

export default CheckAuth;