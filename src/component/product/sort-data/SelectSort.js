import { Grid, Typography, FormControl, InputLabel, Select, MenuItem } from "@mui/material";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp } from "@fortawesome/free-solid-svg-icons";
import { useEffect } from "react";
import { ArrowDownward, ArrowUpward } from "@mui/icons-material";

const SelectSort = (props) => {
    const { getSortData } = props;

    const [sort, setSort] = useState('asc');
    const [sortBy, setSortBy] = useState('dateCreated');

    const handleChangeSort = (e) => {
        setSortBy(e.target.value);
    }

    useEffect(() => {
        getSortData(sort === 'asc' ? 'desc' : 'asc', sortBy);
    }, [sort, sortBy])

    return (
        <Grid item
            p={1}
            mx={2}
            sx={{
                cursor: "pointer",
                borderBottom: "1px solid #acacac",
                // borderRadius: "20px",
            }}

        >
            <Typography component="div" variant="span" mr={2} height="100%" display="inline-block">
                Sắp xếp theo:
                {
                    sort === 'desc'
                        ? <ArrowDownward />
                        : <ArrowUpward />
                }
            </Typography>
            {/* <select onChange={handleChangeSort} defaultValue={sortBy} style={{border: "none"}}>
                <option value="dateCreated">Mới nhất</option>
                <option value="buyPrice">Giá</option>
                <option value="discount">Khuyến mại</option>
                <option value="bestBuy">Bán chạy</option>
            </select> */}

            <FormControl variant="standard" sx={{ minWidth: 120 }}>
                <Select
                    value={sortBy}
                    label="Sắp xếp"
                    onClick={() => { setSort(sort === 'asc' ? 'desc' : 'asc') }}
                    onChange={handleChangeSort}
                    IconComponent={() => <FontAwesomeIcon icon={faCaretDown} />}
                    disableUnderline={true}
                    sx={{ '.css-1rxz5jq-MuiSelect-select-MuiInputBase-input-MuiInput-input': { padding: "0 0" } }}
                // sx={{ boxShadow: 'none', '.MuiOutlinedInput-notchedOutline': { border: 0 } }}
                >
                    <MenuItem value="dateCreated">Mới nhất</MenuItem>
                    <MenuItem value="buyPrice">Giá</MenuItem>
                    <MenuItem value="promotionPrice">Khuyến mại</MenuItem>
                    <MenuItem value="bestBuy">Bán chạy</MenuItem>
                </Select>
            </FormControl>


        </Grid>
    )
}

export default SelectSort;