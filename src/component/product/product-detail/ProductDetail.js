import { Container, Grid } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getProductById } from "../../../action/product.action";
import BreadCrumb from "../../BreadCrumb";
import LastestProductsComponent from "../../content/collection-product/LastestProductsComponent";
import ProductDetailDescription from "./content/ProductDetailDescription";
import ProductDetailInfo from "./content/ProductDetailInfo";

const breadcrumb = [
    {
        name: "Trang chủ",
        url: "/"
    },
    {
        name: "Danh mục sản phẩm",
        url: "/products"
    },
    {
        name: "Chi tiết",
        url: "#"
    },
]

const ProductDetail = () => {
    const { productId } = useParams();

    const dispatch = useDispatch()

    const { productById } = useSelector((reduxData) => {
        return reduxData.productReducer;
    })

    useEffect(() => {
        dispatch(getProductById(productId));
    }, []);

    return (
        <Container maxWidth={false} disableGutters>
            <Grid container justifyContent="center" px={3}>
                <Grid item xs={12} my={3}>
                    <BreadCrumb link={JSON.stringify(breadcrumb)} />
                </Grid>
                <Grid item xs={12} my={3}>
                    <ProductDetailInfo productById={productById} />
                </Grid>
                <Grid item xs={12} flexDirection="column" my={3}>
                    <ProductDetailDescription productById={productById} />
                </Grid>
                <Grid item xs={12} flexDirection="column" my={3}>
                    <LastestProductsComponent />
                </Grid>
            </Grid>
        </Container >
    )
}

export default ProductDetail;