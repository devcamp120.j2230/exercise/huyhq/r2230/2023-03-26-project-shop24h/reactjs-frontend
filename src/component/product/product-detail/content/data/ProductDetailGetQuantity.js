import { Grid, Typography, Button } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { faMinusCircle, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ProductDetailGetQuantity = (props) => {
    const [quantity, setQuantity] = useState(1);

    //click giảm số lượng
    const clickBtnDesQuantity = () => {
        var value = quantity;
        value = value <= 1 ? 1 : value - 1;
        setQuantity(value);
    }

    //click tăng số lượng
    const clickBtnIncQuantity = () => {
        var value = quantity;
        value += 1;
        setQuantity(value);
    }

    //gửi dữ liệu về component cha
    useEffect(()=>{
        props.getQuantity(quantity);
    },[quantity])

    return (
        <Grid container flexDirection="row" my={3} alignItems="center">
            <Typography variant="span" component="span" color="text.primary" fontWeight={700} mr={3}>
                SỐ LƯỢNG:
            </Typography>
            <Button
                sx={{
                    background: "#ffff",
                    minWidth: "40px"
                }}
                onClick={clickBtnDesQuantity}
            ><FontAwesomeIcon icon={faMinusCircle} color="black" size="lg" /></Button>
            <Typography component="div">
                <Typography variant="span" component="div" color="text.primary" fontSize={15}>{quantity}</Typography>
            </Typography>
            <Button
                sx={{
                    background: "#ffff",
                    minWidth: "40px"
                }}
                onClick={clickBtnIncQuantity}
            ><FontAwesomeIcon icon={faPlusCircle} color="black" size="lg" /></Button>
        </Grid>
    )
}
export default ProductDetailGetQuantity;