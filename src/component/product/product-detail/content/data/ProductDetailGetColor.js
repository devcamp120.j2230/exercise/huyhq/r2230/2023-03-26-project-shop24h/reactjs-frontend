import { Grid, Typography, Button } from "@mui/material";

const ProductDetailGetColor = (props) => {
    const colors = props.color ? JSON.parse(props.color) : [];
    return (
        <Grid container flexDirection="row" my={3} alignItems="center">
            <Typography variant="span" component="span" color="text.primary" fontWeight={700} mr={3} >
                MÀU SẮC:
            </Typography>
            {
                colors.length > 0
                ? colors.map((color, i) => {
                    return <Typography
                        key={i}
                        mx={2}
                        component="div"
                        variant="span"
                        bgcolor={color}
                        sx={{
                            borderRadius: "50%",
                            minWidth: "20px",
                            height: "20px",
                            outline: "#CCCCCC solid 2px",
                            outlineOffset: "3px"
                        }}
                    />
                })
                : false
            }
        </Grid>
    )
}

export default ProductDetailGetColor