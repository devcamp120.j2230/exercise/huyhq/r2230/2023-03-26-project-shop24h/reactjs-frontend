import { Grid, Typography, Button } from "@mui/material";
import { useState } from "react";

const styleHide = {
    maxHeight: "150px",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis"
}

const styleMore = {
    maxHeight: "auto",
    whiteSpace: "wrap",
}

const ProductDetailDescription = (props) => {
    const product = props.productById;
    var imgUrl = product.imageUrl ? JSON.parse(product.imageUrl) : []

    const [toggleShow, setToggleShow] = useState(false)

    const onClickBtnGetDetail = (e) => {
        toggleShow ? setToggleShow(false) : setToggleShow(true)
    };

    return (
        <Grid container justifyContent="center">
            <Grid item xs={12} flexDirection="column">
                <Typography component="div" variant="h5" fontWeight={700} my={3}>Description</Typography>
                <Typography component="div" variant="p" my={3}>{product.description}</Typography>
                <Grid
                    container
                    justifyContent="center"
                    sx={toggleShow ? styleMore : styleHide}
                >
                    <Grid item xs={12} textAlign="center">
                        <Grid container flexDirection="column" justifyContent="center" alignItems="center">
                            {
                                imgUrl.length > 0
                                    ? imgUrl.map((img, i) => {
                                        return <Grid item p={2} key={i} width="500px" alignItem="center">
                                            <Typography component="img" alt="Sample" src={img} maxWidth="100%" sx={{objectFit: "contain"}}/>
                                        </Grid>
                                    })
                                    : false
                            }
                        </Grid>

                    </Grid>
                </Grid>
                <Grid container justifyContent="center">
                    <Grid item xs={12} justifyContent="center" textAlign="center">
                        <Button
                            variant="contained"
                            sx={{
                                margin: "20px auto",
                                minWidth: "40px",
                                background: "black",
                                color: "white",
                            }}
                            onClick={onClickBtnGetDetail}
                        >{toggleShow ? "Ẩn bớt" : "Xem thêm"}</Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid >

    )
}

export default ProductDetailDescription;