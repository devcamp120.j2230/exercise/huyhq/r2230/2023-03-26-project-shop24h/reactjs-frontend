import { Grid, Typography, Button, Divider, List, ListItem, ListItemText } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { getNumberItem } from "../../../../action/cart.action";
import { formatCurrency } from "../../../../helper/FormatCurrency";
import ProductImages from "./ProductImages";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar as StarReg } from "@fortawesome/free-regular-svg-icons";
import { faStar as StarSolid } from "@fortawesome/free-solid-svg-icons";
import ProductDetailGetQuantity from "./data/ProductDetailGetQuantity";
import ProductDetailGetColor from "./data/ProductDetailGetColor";
import AddToCartHandle from "../../../handle/AddToCartHandle";

const ProductDetailInfo = (props) => {
    const product = props.productById;
    const [quantity, setQuantity] = useState(1);

    return (
        <Grid container justifyContent="center">
            <ProductImages imgUrl={product.imageUrl} />
            <Grid xs={12} sm={12} md={6} lg={5} flexDirection="column" px={3} pt={3}>
                <Typography variant="h5" component="div" fontWeight={500} textTransform="uppercase">{product.name}</Typography>
                <Typography variant="p" component="div" color="#333" fontSize="16px">Brand: {product.type ? product.type.name : ""}</Typography>
                {/* <Typography variant="p" component="div" fontSize="16px" mb={1}>
                    <Grid container color="text.secondary" alignItems="center">
                        Rated:
                        <Grid item ml={1} fontSize="12px">
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarReg} color="orange" />
                        </Grid>
                    </Grid>
                </Typography> */}
                <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
                <Grid container my={3} alignItems="end">
                    <Typography variant="span" component="span" color="text.secondary" mr={2} sx={{ textDecoration: "line-through" }}>
                        {formatCurrency(product.buyPrice)} VND
                    </Typography>
                    <Typography variant="h5" component="span" color="text.primary" fontWeight={700} >
                        {formatCurrency(product.buyPrice)} VND
                    </Typography>
                </Grid>
                <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
                <>
                    <ProductDetailGetColor color={product.color}/>
                    <ProductDetailGetQuantity getQuantity={(data) => { setQuantity(data) }} />
                    <AddToCartHandle product={product} quantity={quantity}/>
                </>
                <Divider orientation="horizontal" sx={{ borderColor: "#515151" }} />
                <Grid container flexDirection="column" my={3} alignItems="left">
                    <Typography variant="div" component="div" color="text.primary" fontWeight={700}>
                        CHÍNH SÁCH GIAO HÀNG:
                    </Typography>
                    <List sx={{ padding: "0" }}>
                        <ListItem disablePadding>
                            <ListItemText>
                                <Typography variant="div" component="div">
                                    <Typography component="div" fontSize={12}>
                                        Thời gian giao hàng dự kiến khoảng 03-05 ngày làm việc trong giờ hành chính (từ thứ 2 đến thứ 6). Nếu quý khách cần giao gấp trong ngày xin hãy liên hệ với chúng tôi để được hỗ trợ
                                        <Typography component="a" href="#chat" color="primary" fontSize={12}> chat với sale online</Typography>.
                                    </Typography>
                                    <Typography component="div" fontSize={12}>
                                        Tìm hiểu thêm về
                                        <Typography component="a" href="#back" color="primary" fontSize={12}> Đổi hàng</Typography> &
                                        <Typography component="a" href="#ship" color="primary" fontSize={12}> Cách thức giao nhận hàng</Typography> của chúng tôi.
                                    </Typography>
                                </Typography>
                            </ListItemText>
                        </ListItem>
                    </List>
                </Grid>

            </Grid>
        </Grid>
    )
}

export default ProductDetailInfo;