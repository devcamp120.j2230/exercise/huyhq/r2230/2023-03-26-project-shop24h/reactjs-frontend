import { Grid, Typography, Button } from "@mui/material";
import { useState } from "react";
import ShowImageModal from "../../../modal/ShowImageModal";

const ProductImages = (props) => {
    var imgUrl = props.imgUrl ? JSON.parse(props.imgUrl) : []
    const [modalShow, setModalShow] = useState(false)
    const [modalImg, setModalImg] = useState("")

    const modalShowImg = (e) => {
        setModalShow(true);
        setModalImg(e.target.getAttribute('src'))
    }

    return (
        <Grid item xs={12} sm={12} md={6} lg={7} flexDirection="column" textAlign="center">
            {
                imgUrl.length > 0
                    ? <>
                        <Grid container flexDirection="row" justifyContent="center">
                            <Grid item xs={6} p={1}>
                                <Typography component="img" alt="Sample" src={imgUrl[0]} maxWidth="100%" sx={{ cursor: "pointer" }} onClick={modalShowImg} />
                            </Grid>
                            <Grid item xs={6} p={1}>
                                <Typography component="img" alt="Sample" src={imgUrl[1]} maxWidth="100%" sx={{ cursor: "pointer" }} onClick={modalShowImg} />
                            </Grid>
                        </Grid>
                        <Grid container flexDirection="row" justifyContent="center">
                            <Grid item xs={3} p={1}>
                                <Typography component="img" alt="Sample" src={imgUrl[2]} maxWidth="100%" sx={{ cursor: "pointer" }} onClick={modalShowImg} />
                            </Grid>
                            <Grid item xs={3} p={1}>
                                <Typography component="img" alt="Sample" src={imgUrl[3]} maxWidth="100%" sx={{ cursor: "pointer" }} onClick={modalShowImg} />
                            </Grid>
                            <Grid item xs={3} p={1}>
                                <Typography component="img" alt="Sample" src={imgUrl[4]} maxWidth="100%" sx={{ cursor: "pointer" }} onClick={modalShowImg} />
                            </Grid>
                            <Grid item xs={3} p={1}>
                                <Typography component="img" alt="Sample" src={imgUrl[5]} maxWidth="100%" sx={{ cursor: "pointer" }} onClick={modalShowImg} />
                            </Grid>
                        </Grid>
                    </>
                    : false
            }

            <ShowImageModal
                setModal={modalShow}
                imgUrl={modalImg}
                getModalStatus={(value) => { setModalShow(value) }}
            />
        </Grid>
    )
}

export default ProductImages