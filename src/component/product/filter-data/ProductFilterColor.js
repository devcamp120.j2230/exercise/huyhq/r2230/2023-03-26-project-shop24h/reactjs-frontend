import { Grid, Typography, FormGroup, FormControl, List, ListItem, ListItemButton, ListItemText, ListItemIcon } from "@mui/material";
import { faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect } from "react";
import { useState } from "react";
import colorBag from "../../../data/color-bags.data";
import { useSearchParams } from "react-router-dom";

const ProductFilterColor = (props) => {
    const [param, setParam] = useSearchParams();
    
    const [colorSelect, setColorSelect] = useState(param.get("color") ? param.get("color") : "");
    const { setColorFilter } = props;
    //filter theo category
    const handleChangeColor = (data) => {
        // console.log(data)
        setColorSelect(data);
        setColorFilter(data);
    }

    return (
        <Grid item flexDirection="column" xs={12}>
            <Grid container>
                <Grid item>
                    <FontAwesomeIcon icon={faCaretRight} />
                    <Typography component="span" variant="span" pl={2} fontWeight={700}>Màu sắc</Typography>
                </Grid>
            </Grid>
            <FormGroup sx={{ paddingLeft: "16px" }}>
                <FormControl fullWidth>
                    <List
                        disablePadding
                        sx={{
                            display: "flex",
                            flexDirection: "row",
                            flexWrap: "wrap"
                        }}
                    >
                        {colorBag.map((color) => (
                            <ListItem disablePadding sx={{ width: { xs: "50%", sm: "33%" } }}>
                                <ListItemButton sx={{ padding: "0 0" }} onClick={() => { handleChangeColor(color.value) }} selected={color.value === colorSelect}>
                                    <ListItemIcon>
                                        <Typography
                                            component="div"
                                            sx={{
                                                width: "20px",
                                                height: "20px",
                                                borderRadius: "50%",
                                                margin: "20px 20px",
                                                background: color.value,
                                            }}
                                        />
                                    </ListItemIcon>
                                    <ListItemText primary={color.name} />
                                </ListItemButton>
                            </ListItem>
                        ))}
                    </List>
                </FormControl>
            </FormGroup>
        </Grid>
    )
}

export default ProductFilterColor