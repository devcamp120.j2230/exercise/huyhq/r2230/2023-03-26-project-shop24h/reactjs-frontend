import { Grid, Button } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSearchParams } from "react-router-dom";

import ProductFilterName from "./ProductFilterName";
import ProductFilterPrice from "./ProductFilterPrice";
import ProductFilterCategory from "./ProductFilterCategory";
import ProductFilterRate from "./ProductFilterRate";
import { getAllProduct } from "../../../action/product.action";
import ProductFilterColor from "./ProductFilterColor";

const ProductFilters = (props) => {
    const { getOpenFilterPage, getFilterData } = props;
    const [nameFilter, setNameFilter] = useState("");
    const [minPriceFilter, setMinPriceFilter] = useState(0);
    const [maxPriceFilter, setMaxPriceFilter] = useState(Number.MAX_VALUE);
    const [categoryFilter, setCategoryFilter] = useState("");
    const [colorFilter, setColorFilter] = useState("");

    const [searchParams, setSearchParams] = useSearchParams();

    // const dispatch = useDispatch();

    // const { limitProduct, currentPage } = useSelector((reduxData) => {
    //     return reduxData.productReducer;
    // })

    const handleCloseFilter = () => {
        setSearchParams("");
        getOpenFilterPage(false);
    }

    //khi click vào button filter
    const onClickButtonFilter = () => {
        var filter = ""
        //filter theo color
        if (colorFilter !== "") {
            filter = { ...filter, color: colorFilter }
        }
        //filter theo tên
        if (nameFilter !== "") {
            filter = { ...filter, name: nameFilter }
        }
        //filter theo price
        if (minPriceFilter > 0) {
            filter = { ...filter, min: minPriceFilter }
        }

        if (maxPriceFilter < Number.MAX_VALUE) {
            filter = { ...filter, max: maxPriceFilter }
        }

        //filter theo category
        var category = [];
        if (categoryFilter !== "") {
            // console.log(categoryFilter);
            categoryFilter.filter((elm) => {
                if (elm.check) {
                    category.push(elm._id)
                }
            })

            filter = category.length > 0 ? { ...filter, category: JSON.stringify(category) } : false;
        }
        //set param
        setSearchParams(filter);
        //send filter
        getFilterData(filter)
        //close filter
        getOpenFilterPage(false);
    }

    return (
        <Grid p={5}>
            <Grid p={3} container border="1px solid #acacac" borderRadius="10px">
                <Grid container my={2}>
                    <Grid item xs={12} sm={12} md={4}>
                        <Grid container my={2} flexDirection="column">
                            <ProductFilterName setNameFilter={(data) => { setNameFilter(data) }} />
                            <ProductFilterPrice
                                setMaxPriceFilter={(data) => { setMaxPriceFilter(data) }}
                                setMinPriceFilter={(data) => { setMinPriceFilter(data) }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12} md={8}>
                        <Grid container my={2}>
                            <ProductFilterColor setColorFilter={(data) => setColorFilter(data)} />
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container my={2}>
                            <ProductFilterCategory setCategoryFilter={(data) => { setCategoryFilter(data) }} />
                        </Grid>
                    </Grid>
                </Grid>
                {/* <Grid container my={2}>
                <ProductFilterRate />
            </Grid> */}
                <Grid container my={2} justifyContent="flex-end" textAlign="right">
                    <Button variant="contained" color="inherit" sx={{ marginLeft: "16px" }} onClick={handleCloseFilter}>Hủy</Button>
                    <Button variant="contained" sx={{ marginLeft: "16px", background: "black" }} onClick={onClickButtonFilter}>Áp dụng</Button>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ProductFilters;