import { faStar as StarReg } from "@fortawesome/free-regular-svg-icons";
import { faStar as StarSolid, faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid, Typography, FormGroup, FormControlLabel, Checkbox } from "@mui/material";

const ProductFilterRate = (props) => {
    return (
        <Grid item flexDirection="column">
            <Grid container>
                <Grid item>
                    <FontAwesomeIcon icon={faCaretRight} />
                    <Typography component="span" variant="span" pl={2} fontWeight={700}>Rating</Typography>
                </Grid>
            </Grid>
            <FormGroup sx={{ paddingLeft: "16px" }}>
                <FormControlLabel control={<Checkbox defaultChecked />} labelPlacement="end"
                    label={
                        <>
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                        </>
                    } />
                <FormControlLabel control={<Checkbox />} labelPlacement="end"
                    label={
                        <>
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                        </>
                    } />
                <FormControlLabel control={<Checkbox />} labelPlacement="end"
                    label={
                        <>
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                        </>
                    } />
                <FormControlLabel control={<Checkbox />} labelPlacement="end"
                    label={
                        <>
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                        </>
                    } />
                <FormControlLabel control={<Checkbox />} labelPlacement="end"
                    label={
                        <>
                            <FontAwesomeIcon icon={StarSolid} color="orange" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                            <FontAwesomeIcon icon={StarReg} color="gray" />
                        </>
                    } />
            </FormGroup>
        </Grid>
    )
}

export default ProductFilterRate