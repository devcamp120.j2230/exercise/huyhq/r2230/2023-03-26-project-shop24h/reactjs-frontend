import { Grid, Typography, FormGroup, FormControlLabel, Checkbox } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { getAllProductType } from "../../../action/product-type.action";
import { useSearchParams } from "react-router-dom";

const ProductFilterCategory = (props) => {
    const [param, setParam] = useSearchParams();
    const listCategories = param.get("category") ? JSON.parse(param.get("category")) : [];
    const dispatch = useDispatch();

    const [widthScreen, setWidthScreen] = useState(window.innerWidth);
    const [heightScreen, setHeightScreen] = useState(window.innerHeight);

    const { productType } = useSelector((reduxData) => {
        return reduxData.productTypeReducer;
    });

    const updateDimensions = () => {
        setWidthScreen(window.innerWidth);
        setHeightScreen(window.innerHeight);
    }

    //load category list
    useEffect(() => {
        window.addEventListener("resize", updateDimensions);
        dispatch(getAllProductType());
    }, [])

    //filter theo category
    const onCategoryCheckbox = (e) => {
        var key = e.target.value;
        var newCat = productType;
        newCat[key].check = e.target.checked
        props.setCategoryFilter(newCat);
    }

    return (
        <Grid item flexDirection="column" xs={12}>
            <Grid container>
                <Grid item>
                    <FontAwesomeIcon icon={faCaretRight} />
                    <Typography component="span" variant="span" pl={2} fontWeight={700}>Phân loại</Typography>
                </Grid>
            </Grid>
            <FormGroup
                sx={{
                    paddingLeft: "16px",
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap"
                }}
            >
                {
                    productType !== ""
                        ? productType.map((category, i) => {
                            return <FormControlLabel
                                sx={{
                                    width: widthScreen > 700 ? "25%" : "50%",
                                    margin: "10px 0px"
                                }}
                                key={i}
                                control={
                                    <Checkbox
                                        defaultChecked={listCategories.includes(category._id)}
                                        value={i}
                                        name={category.name}
                                        onChange={onCategoryCheckbox}
                                        sx={{ padding: "5px" }}
                                    />
                                }
                                labelPlacement="end"
                                label={category.description} />
                        })
                        : <></>
                }
            </FormGroup>
        </Grid>
    )
}

export default ProductFilterCategory