import { Grid, Typography, TextField, Box, Slider, createTheme, ThemeProvider } from "@mui/material";
import { faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { formatCurrency } from "../../../helper/FormatCurrency";
import { useSearchParams } from "react-router-dom";

const theme = createTheme({
    status: {
        danger: '#e53e3e',
    },
    palette: {
        primary: {
            main: '#333',
            darker: '#053e85',
        },
        neutral: {
            main: '#64748B',
            contrastText: '#fff',
        },
    },
});

const priceSize = {
    minPrice: 100000,
    maxPrice: 5000000,
    step: 100000,
}

const ProductFilterPrice = (props) => {
    const [param, setParam] = useSearchParams();
    
    const [value, setValue] = useState(param.get("min")? [param.get("min"),param.get("max")] : [200000, 1000000]);

    const handleChange = (e, newValue) => {
        setValue(newValue);
        props.setMinPriceFilter(newValue[0])
        props.setMaxPriceFilter(newValue[1])
    };

    //filter theo giá lớn nhất
    const onInputMaxPriceFilter = (e) => {
        e.target.value != ""
            ? props.setMaxPriceFilter(e.target.value)
            : props.setMaxPriceFilter(Number.MAX_VALUE)
    }

    //filter theo giá nhỏ nhất
    const onInputMinPriceFilter = (e) => {
        e.target.value != ""
            ? props.setMinPriceFilter(e.target.value)
            : props.setMinPriceFilter(0)
    }

    return (
        <Grid item flexDirection="column">
            <Grid container>
                <Grid item>
                    <FontAwesomeIcon icon={faCaretRight} />
                    <Typography component="span" variant="span" pl={2} fontWeight={700}>Giá</Typography>
                </Grid>
            </Grid>
            {/* <Grid container my={1}>
                <Grid item px={1}>
                    <Typography
                        maxWidth={100}
                        component="input"
                        type="text"
                        placeholder="min"
                        borderRadius="20px"
                        border="1px solid #959595"
                        padding="6px"
                        onChange={onInputMinPriceFilter}
                    />
                </Grid>
                <Grid item textAlign="center" lineHeight="3">
                    <Typography component="span">-</Typography>
                </Grid>
                <Grid item px={1}>
                    <Typography
                        maxWidth={100}
                        component="input"
                        type="text"
                        placeholder="max"
                        borderRadius="20px"
                        border="1px solid #959595"
                        padding="6px"
                        onChange={onInputMaxPriceFilter}
                    />
                </Grid>
            </Grid> */}
            <Grid container my={1} pl={1}>
                <ThemeProvider theme={theme}>
                    <Grid item xs={12}>
                        <Box sx={{ width: 300 }}>
                            <Slider
                                min={priceSize.minPrice}
                                step={priceSize.step}
                                max={priceSize.maxPrice}
                                size={"medium"}
                                value={value}
                                onChange={handleChange}
                                valueLabelDisplay="auto"
                                color="primary"
                                getAriaLabel={() => 'Price range'}
                                sx={{width: {xs: "190px", sm: "300px"}}}
                            />
                        </Box>
                    </Grid>
                    <Grid item xs={12} textAlign="left">
                        <Typography component="span" fontSize={14}>{formatCurrency(value[0])} VND</Typography>
                        <Typography component="span" fontSize={14}> - </Typography>
                        <Typography component="span" fontSize={14}>{formatCurrency(value[1])} VND</Typography>
                    </Grid>
                </ThemeProvider>
            </Grid>
        </Grid>
    )
}

export default ProductFilterPrice