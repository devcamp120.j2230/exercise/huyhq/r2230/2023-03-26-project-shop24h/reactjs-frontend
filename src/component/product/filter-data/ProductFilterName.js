import { faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid, Paper, Typography } from "@mui/material";
import { useState } from "react";
import { useSearchParams } from "react-router-dom";

const ProductFilterName = (props) => {
    const [param, setParam] = useSearchParams();
    const [name, setName] = useState(param.get("name") ? param.get("name") : "")
    //filter theo tên sản phẩm
    const onInputNameFilter = (e) => {
        props.setNameFilter(e.target.value);
    }
    return (
        <Grid item flexDirection="column">
            <Grid container>
                <Grid item>
                    <FontAwesomeIcon icon={faCaretRight} />
                    <Typography component="span" variant="span" pl={2} fontWeight={700}>Tên sản phẩm</Typography>
                </Grid>
            </Grid>
            <Paper sx={{ border: "none", boxShadow: "none", margin: "10px 0", width: { xs: "200px", sm: "300px" } }}>
                <Typography
                    defaultValue={name}
                    width="100%"
                    component="input"
                    type="text"
                    placeholder="Name"
                    borderRadius="20px"
                    border="1px solid #959595"
                    padding="6px"
                    mr={2}
                    onChange={onInputNameFilter}
                />
            </Paper>
        </Grid>
    )
}

export default ProductFilterName