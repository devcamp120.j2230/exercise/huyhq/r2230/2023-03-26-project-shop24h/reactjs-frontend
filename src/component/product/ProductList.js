import { Grid, Pagination, Stack, Typography, FormControl, InputLabel, Select, MenuItem } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changePaginationAction, getAllProduct } from "../../action/product.action";
import BreadCrumb from "../BreadCrumb";
import ProductCard from "./ProductCard";
import ProductFilters from "./filter-data/ProductFilters";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import SelectSort from "./sort-data/SelectSort";
const breadcrumb = [
    {
        name: "Trang chủ",
        url: "/"
    },
    {
        name: "Danh mục sản phẩm",
        url: "#"
    }
]
const limitProduct = 12;

const ProductList = () => {
    const [filterPage, setFilterPage] = useState(false);
    const [filterData, setFilterData] = useState("");
    const [sortData, setSortData] = useState("");
    const [sortDataBy, setSortDataBy] = useState("");

    const dispatch = useDispatch();

    const { products, pending, currentPage, countPage } = useSelector((reduxData) => {
        return reduxData.productReducer;
    })

    useEffect(() => {
        // console.log(filterData);
        // console.log(sortData);
        // console.log(sortDataBy);
        dispatch(getAllProduct(limitProduct, currentPage, sortData,  sortDataBy, filterData));
    }, [currentPage, sortData, sortDataBy, filterData])

    const onChangePagination = (event, value) => {
        dispatch(changePaginationAction(value));
    }
    return (
        <Grid container p={3}>
            <Grid item xs={12}>
                <BreadCrumb link={JSON.stringify(breadcrumb)} />
            </Grid>
            <Grid item xs={12} textAlign="center" my={2}>
                <Typography component="div" variant="h4" textTransform="uppercase" fontWeight={700} my={2}>Sản phẩm</Typography>
                <Typography component="div" variant="span">({products.length} sản phẩm)</Typography>
            </Grid>
            <Grid item xs={12} my={3}>
                <Grid container justifyContent="flex-end" alignItems="center">
                    <Grid item
                        p={1}
                        mx={2}
                        sx={{
                            cursor: "pointer",
                            borderBottom: "1px solid #acacac",
                            // borderRadius: "20px",
                        }}
                        onClick={() => { setFilterPage(!filterPage) }}
                    >
                        <Typography component="span" variant="span" mr={2}>Lọc sản phẩm</Typography>
                        <FontAwesomeIcon icon={faCaretDown} />
                    </Grid>
                    <>
                        <SelectSort getSortData={(sort, sortBy)=>{
                            setSortData(sort);
                            setSortDataBy(sortBy);
                        }}/>
                    </>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                {
                    filterPage
                        ? <ProductFilters
                            getOpenFilterPage={(data) => { setFilterPage(data) }}
                            getFilterData={(filter) => { setFilterData(filter) }}
                        />
                        : false
                }
            </Grid>
            <Grid item xs={12}>
                <Grid container justifyContent="center">
                    <Grid item xs={12}>
                        <Grid container>
                            {
                                products !== ""
                                    ? products.map((product, i) => {
                                        return <React.Fragment key={i}>
                                            <ProductCard product={JSON.stringify(product)} />
                                        </React.Fragment>
                                    })
                                    : <></>
                            }
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Stack spacing={2} sx={{ float: "right" }}>
                            <Pagination count={countPage} page={currentPage} onChange={onChangePagination} />
                        </Stack>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default ProductList;