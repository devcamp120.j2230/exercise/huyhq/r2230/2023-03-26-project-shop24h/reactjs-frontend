import { Grid, Card, CardContent, Typography, Button } from "@mui/material";
import { useLocation } from "react-router-dom";
import AddToCartHandle from "../handle/AddToCartHandle";
import { useEffect, useState } from "react";
import Carousel from "react-material-ui-carousel";

const ProductCard = (props) => {
    const product = JSON.parse(props.product);
    const imageProduct = JSON.parse(product.imageUrl);

    const [widthScreen, setWidthScreen] = useState(window.innerWidth);

    const [showContent, setShowContent] = useState(false);

    const formatNumber = (price) => {
        return new Intl.NumberFormat().format(price)
    }

    useEffect(()=>{
        window.addEventListener("resize", ()=>{setWidthScreen(window.innerWidth)});

        widthScreen < 800 ? setShowContent(true) : setShowContent(false)
    },[widthScreen])

    return (
        <Grid item xs={12} sm={6} md={4} lg={3} justifyContent="center" p={3}>
            <Card
                sx={{ minWidth: 275, boxShadow: "none" }}
                onMouseEnter={() => { setShowContent(true) }}
                onMouseLeave={() => { setShowContent(false) }}
            >
                <Typography
                    component="a"
                    href={"/products/" + product._id}
                >
                    <Typography gutterBottom textAlign="center" position="relative" width="300px" height="370px" mx="auto">
                        {
                            showContent
                                ? imageProduct.length > 0
                                    ? <Carousel
                                        indicators={false}
                                        autoPlay={showContent}
                                        stopAutoPlayOnHover={false}
                                        navButtonsAlwaysInvisible={true}
                                        interval={1000}>
                                        {

                                            imageProduct.map((img) => {
                                                return <Typography component="img" alt="Sample" src={img} sx={{objectFit: "cover"}} width="300px" height="370px"/>
                                            })
                                        }
                                    </Carousel>
                                    : false
                                : <Typography component="img" alt="Sample" src={imageProduct[0]} sx={{objectFit: "cover"}} width="300px" height="370px"/>
                        }
                        <Typography
                            sx={{ zIndex: 10 }}
                            component="div"
                            position="absolute"
                            border="1px solid #ccc"
                            fontSize="0.8rem"
                            bgcolor="#ffff"
                            p={1}
                            top={0}
                            left={0}
                        >
                            New Collection
                        </Typography>
                    </Typography>
                    <CardContent sx={{ padding: "5px 5px", width: "300px", mx: "auto" }}>
                        <Typography
                            variant="span"
                            component="div"
                            textAlign="center"
                            minHeight={30}
                            sx={{
                                whiteSpace: "nowrap",
                                overflow: "hidden",
                                textOverflow: "ellipsis"
                            }}>
                            {product.name}
                        </Typography>
                        <Typography component="div" textAlign="center">
                            <Typography variant="span" mr={1} sx={{ fontSize: "0.8rem", textDecoration: "line-through" }} color="text.secondary">
                                {formatNumber(product.buyPrice)}
                            </Typography>
                            <Typography variant="span" color="#000" fontWeight={700} fontSize="1rem">
                                {formatNumber(product.promotionPrice)} VND
                            </Typography>
                        </Typography>
                    </CardContent>
                </Typography>
                {
                    showContent
                        ? <Typography component="div">
                            <Typography component="div" textAlign="center">
                                {
                                    JSON.parse(product.color).map((color, i) => {
                                        return <Typography
                                            sx={{ outline: "#ccc solid 1px", outlineOffset: "3px" }}
                                            key={i}
                                            component="div"
                                            m="0 auto"
                                            width={30}
                                            height={30}
                                            borderRadius="50%"
                                            bgcolor={color}
                                        />
                                    })
                                }
                            </Typography>
                            <Typography component="div" textAlign="center">
                                <AddToCartHandle product={product} quantity={1} />
                            </Typography>
                        </Typography>
                        : false
                }

            </Card>
        </Grid>
    )
}

export default ProductCard;