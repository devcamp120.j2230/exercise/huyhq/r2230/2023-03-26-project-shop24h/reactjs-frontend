import { Grid, Typography } from "@mui/material";
import { useSelector } from "react-redux";

const AdminHome = () => {
    const { search } = useSelector(data => data.adminSearchReducer);
    // console.log(search);

    return (
        <Grid container>
            <Grid item xs={12}>
                <Typography component="div" variant="h5">Home</Typography>
            </Grid>
        </Grid>
    )
}

export default AdminHome;