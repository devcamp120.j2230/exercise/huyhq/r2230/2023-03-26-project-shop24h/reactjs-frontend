import { Grid, Button, Typography, TableContainer, TableHead, TableRow, TableBody, Paper, Table, Pagination, Stack, IconButton, TablePagination } from "@mui/material"
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changePaginationAction, getAllProduct } from "../../../../action/product.action";
import { AddCircleOutlineRounded, Delete, Edit } from "@mui/icons-material";
import { StyledTableCell, StyledTableRow } from "../../../../style/table-mui.style";
import AddProductModal from "../../../modal/admin/product/AddProductModal";
import DeleteConfirmProductModal from "../../../modal/admin/product/DeleteConfirmProductModal";
import AlertNotification from "../../../alert/AlertNotification";
import UpdateProductModal from "../../../modal/admin/product/UpdateProductModal";
import TableHeadProduct from "./data/TableHeadProduct";


const theme = createTheme({
    palette: {
        danger: {
            main: '#e53e3e',
            darker: '#053e85',
        },
    },
});

const AdminProductList = () => {
    const [alert, setAlert] = useState({ status: false, state: "info", message: "" });
    const [rowsPerPage, setRowsPerPage] = useState(3);
    const [openModalAdd, setOpenModalAdd] = useState(false)
    const [openModalUpdate, setOpenModalUpdate] = useState(false)
    const [openModalDeleteConfirm, setOpenModalDeleteConfirm] = useState(false)
    const [product, setProduct] = useState("");
    const [idProduct, setIdProduct] = useState("");

    const dispatch = useDispatch();

    const { products, currentPage, countPage, statePost, statePut, stateDelete } = useSelector((data) => data.productReducer);

    useEffect(() => {
        //thay đổi trạng thái khi thêm sản phẩm sẽ hiện thông báo
        if (statePost !== "") {
            setAlert({ status: true, state: statePost, message: "Thêm sản phẩm mới thành công!" })
        }

        //thay đổi trạng thái khi sửa sản phẩm sẽ hiện thông báo
        if (statePut !== "") {
            setAlert({ status: true, state: statePut, message: "Sửa sản phẩm thành công!" })
        }

        //thay đổi trạng thái khi xóa sản phẩm sẽ hiện thông báo
        if (stateDelete !== "") {
            setAlert({ status: true, state: stateDelete, message: "Xóa sản phẩm thành công!" })
        }


    }, [statePost, statePut, stateDelete])

    //click phân trang
    const onChangePagination = (event, value) => {
        dispatch(changePaginationAction(value));
    };

    //xử lý số dòng trên trang
    const handleChangeRowsPerPage = (e) => {
        setRowsPerPage(e.target.value);
        // setPage(currentPage);
    };

    //click Button Add
    const onClickBtnAddProduct = () => {
        setOpenModalAdd(!openModalAdd)
    }

    //click Button Edit
    const onClickBtnEdit = (e) => {
        setOpenModalUpdate(!openModalUpdate)
        setProduct(e.target.value);
    }

    //click Button Delete
    const onClickBtnDelete = (e) => {
        setOpenModalDeleteConfirm(true);
        setIdProduct(e.target.value);
        // console.log("Delete " + e.target.value);
    }

    return (
        <Grid container>
            {
                alert.status
                    ? <Grid item xs={12}>
                        <AlertNotification
                            status={alert.status}
                            state={alert.state}
                            message={alert.message}
                            getStatus={(data) => { setAlert({ status: data, state: "info", message: "" }) }}
                        />
                    </Grid>
                    : false
            }
            <Grid item xs={12}>
                <Typography component="div" variant="h5">Quản lý Sản phẩm</Typography>
            </Grid>
            <Grid item xs={12} mt={3} mb={1}>
                <Button variant="contained" onClick={onClickBtnAddProduct}>
                    <AddCircleOutlineRounded sx={{ marginRight: "5px" }} /> Thêm sản phẩm
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Grid container alignItems='center'>
                    <Grid item xs={6}>
                        <Typography component="span">Num rows: </Typography>
                        <Typography
                            component="select"
                            defaultValue={rowsPerPage}
                            width={50}
                            border="none"
                            onChange={handleChangeRowsPerPage}
                        >
                            <option value={3}>3</option>
                            <option value={5}>5</option>
                            <option value={10}>10</option>
                        </Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="right">
                        <Stack spacing={2} mb={3} sx={{ float: "right" }}>
                            <Pagination count={countPage} page={currentPage} onChange={onChangePagination} />
                        </Stack>
                    </Grid>
                </Grid>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHeadProduct
                            rowsPerPage={rowsPerPage}
                            currentPage={currentPage}
                        />
                        <TableBody>
                            {
                                products
                                    ? products.map((product, i) => {
                                        return <StyledTableRow
                                            key={i}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <StyledTableCell component="th" scope="row">
                                                {(i + 1) + (rowsPerPage * (currentPage - 1))}
                                            </StyledTableCell>
                                            <StyledTableCell align="left" sx={{ width: "100px" }}>

                                                {
                                                    product.imageUrl
                                                        ? <Typography
                                                            mr={1}
                                                            component="img"
                                                            src={JSON.parse(product.imageUrl)[0]}
                                                            width="100px"
                                                            height="auto"
                                                        />
                                                        : false
                                                }

                                                <Typography
                                                    component="div"
                                                    sx={{
                                                        display: "inline-block",
                                                        width: "100%",
                                                        whiteSpace: "nowrap",
                                                        overflow: "hidden",
                                                        textOverflow: "ellipsis"
                                                    }}
                                                >
                                                    {product.name}
                                                </Typography>
                                            </StyledTableCell>
                                            <StyledTableCell align="left">
                                                {
                                                    product.color
                                                        ? JSON.parse(product.color).map((color, i) => {
                                                            return <Typography
                                                                key={i}
                                                                component="div"
                                                                width="20px"
                                                                height="20px"
                                                                bgcolor={color}
                                                                borderRadius="50%"
                                                            />
                                                        })
                                                        : false
                                                }
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{product.type.name}</StyledTableCell>
                                            <StyledTableCell align="center">{product.buyPrice}</StyledTableCell>
                                            <StyledTableCell align="center">{product.promotionPrice}</StyledTableCell>
                                            <StyledTableCell align="center">{product.amount}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <ThemeProvider theme={theme}>
                                                    <IconButton onClick={onClickBtnEdit} value={JSON.stringify(product)}>
                                                        <Edit color="primary" sx={{ pointerEvents: 'none' }} />
                                                    </IconButton>
                                                    <IconButton onClick={onClickBtnDelete} value={product._id}>
                                                        <Delete color="danger" sx={{ pointerEvents: 'none' }} />
                                                    </IconButton>
                                                </ThemeProvider>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    })
                                    : false
                            }

                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>

            {/* modal add */}
            {
                openModalAdd
                    ? <Grid item xs={12}>
                        <AddProductModal getModalStatus={(data) => { setOpenModalAdd(data) }} />
                    </Grid>
                    : false
            }

            {/* modal update */}
            {
                openModalUpdate
                    ? <Grid item xs={12}>
                        <UpdateProductModal
                            getModalStatus={(data) => { setOpenModalUpdate(data) }}
                            product={product}
                        />
                    </Grid>
                    : false
            }

            {/* modal delete */}
            {
                openModalDeleteConfirm
                    ? <Grid item xs={12}>
                        <DeleteConfirmProductModal idProduct={idProduct} getModalStatus={(data) => setOpenModalDeleteConfirm(data)} />
                    </Grid>
                    : false
            }
        </Grid>
    )
}

export default AdminProductList