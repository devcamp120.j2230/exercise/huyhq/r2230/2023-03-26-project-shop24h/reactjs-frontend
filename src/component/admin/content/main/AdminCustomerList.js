import { Grid, Button, Typography, TableContainer, TableHead, TableRow, TableBody, Paper, Table, Pagination, Stack, IconButton } from "@mui/material"
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AddCircleOutlineRounded, Delete, Edit } from "@mui/icons-material";
import { changePaginationCustomer, getAllCustomer } from "../../../../action/customer.action";
import { StyledTableCell, StyledTableRow } from "../../../../style/table-mui.style";
import { useState } from "react";
import AddCustomerModal from "../../../modal/admin/customer/AddCustomerModal";
import DeleteConfirmCustomerModal from "../../../modal/admin/customer/DeleteConfirmCustomerModal";
import AlertNotification from "../../../alert/AlertNotification";
import UpdateCustomerModal from "../../../modal/admin/customer/UpdateCustomerModal";
import TableHeadCustomer from "./data/TableHeadCustomer";

const theme = createTheme({
    palette: {
        danger: {
            main: '#e53e3e',
            darker: '#053e85',
        },
    },
});

const AdminCustomerList = () => {
    const [alert, setAlert] = useState({ status: false, state: "info", message: "" });
    const [rowsPerPage, setRowsPerPage] = useState(3);
    const [openModalAdd, setOpenModalAdd] = useState(false)
    const [openModalUpdate, setOpenModalUpdate] = useState(false)
    const [openModalDelete, setOpenModalDelete] = useState(false)

    const [idCustomer, setIdCustomere] = useState("")
    const [customer, setCustomer] = useState("")

    const dispatch = useDispatch();

    const { customers, currentPage, countPage, statePost, statePut, stateDelete } = useSelector((data) => data.customerReducer);

    useEffect(() => {
        //thay đổi trạng thái khi tạo mới khách hàng sẽ hiện thông báo
        if (statePost !== "") {
            setAlert({ status: true, state: statePost, message: "Tạo mới khách hàng thành công!" })
        };

        //thay đổi trạng thái khi sửa khách hàng sẽ hiện thông báo
        if (statePut !== "") {
            setAlert({ status: true, state: statePut, message: "Sửa khách hàng thành công!" })
        };

        //thay đổi trạng thái khi xóa khách hàng sẽ hiện thông báo
        if (stateDelete !== "") {
            setAlert({ status: true, state: stateDelete, message: "Xóa khách hàng thành công!" })
        };

    }, [statePost, statePut, stateDelete])

    //click phân trang
    const onChangePagination = (event, value) => {
        dispatch(changePaginationCustomer(value));
    };

    //xử lý số dòng trên trang
    const handleChangeRowsPerPage = (e) => {
        setRowsPerPage(e.target.value);
        // setPage(currentPage);
    };

    //click Button add
    const onClickBtnAddCustomer = () => {
        setOpenModalAdd(!openModalAdd)
    };

    //click Button Edit
    const onClickBtnEdit = (e) => {
        setOpenModalUpdate(!openModalUpdate)
        setCustomer(e.target.value);
    };

    //click Button Delete
    const onClickBtnDelete = (e) => {
        setOpenModalDelete(true);
        setIdCustomere(e.target.value);
    }

    return (
        <Grid container>
            {
                alert.status
                    ? <Grid item xs={12}>
                        <AlertNotification
                            status={alert.status}
                            state={alert.state}
                            message={alert.message}
                            getStatus={(data) => { setAlert({ status: data, state: "info", message: "" }) }}
                        />
                    </Grid>
                    : false
            }
            <Grid item xs={12}>
                <Typography component="div" variant="h5">Quản Lý Khách Hàng</Typography>
            </Grid>
            <Grid item xs={12} mt={3} mb={1}>
                <Button variant="contained" onClick={onClickBtnAddCustomer}>
                    <AddCircleOutlineRounded sx={{ marginRight: "5px" }} /> Thêm Khách hàng
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Grid container alignItems='center'>
                    <Grid item xs={6}>
                        <Typography component="span">Num rows: </Typography>
                        <Typography
                            component="select"
                            defaultValue={rowsPerPage}
                            width={50}
                            border="none"
                            onChange={handleChangeRowsPerPage}
                        >
                            <option value={3}>3</option>
                            <option value={5}>5</option>
                            <option value={10}>10</option>
                        </Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="right">
                        <Stack spacing={2} mb={3} sx={{ float: "right" }}>
                            <Pagination count={countPage} page={currentPage} onChange={onChangePagination} />
                        </Stack>
                    </Grid>
                </Grid>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHeadCustomer
                            currentPage={currentPage}
                            rowsPerPage={rowsPerPage}
                        />
                        <TableBody>
                            {
                                customers
                                    ? customers.map((customer, i) => {
                                        return <StyledTableRow
                                            key={i}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <StyledTableCell component="th" scope="row">
                                                {(i + 1) + (rowsPerPage * (currentPage - 1))}
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{customer.fullName}</StyledTableCell>
                                            <StyledTableCell align="left">{customer.phone}</StyledTableCell>
                                            <StyledTableCell align="left">{customer.email}</StyledTableCell>
                                            <StyledTableCell align="left">{`${customer.address}, ${customer.district}, ${customer.city}, ${customer.country}`}</StyledTableCell>
                                            <StyledTableCell align="left">{customer.group ? customer.group.name : ""}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <ThemeProvider theme={theme}>
                                                    <IconButton onClick={onClickBtnEdit} value={JSON.stringify(customer)}>
                                                        <Edit color="primary" sx={{ pointerEvents: 'none' }} />
                                                    </IconButton>
                                                    <IconButton onClick={onClickBtnDelete} value={customer._id}>
                                                        <Delete color="danger" sx={{ pointerEvents: 'none' }} />
                                                    </IconButton>
                                                </ThemeProvider>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    })
                                    : false
                            }

                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            {
                openModalAdd
                    ? <Grid item xs={12}>
                        <AddCustomerModal getModalStatus={(data) => { setOpenModalAdd(data) }} />
                    </Grid>
                    : false
            }
            {
                openModalUpdate
                    ? <Grid item xs={12}>
                        <UpdateCustomerModal
                            customer={customer}
                            getModalStatus={(data) => { setOpenModalUpdate(data) }}
                        />
                    </Grid>
                    : false
            }
            {
                openModalDelete
                    ? <Grid item xs={12}>
                        <DeleteConfirmCustomerModal
                            getModalStatus={(data) => { setOpenModalDelete(data) }}
                            idCustomer={idCustomer}
                        />
                    </Grid>
                    : false
            }
        </Grid>
    )
}

export default AdminCustomerList;