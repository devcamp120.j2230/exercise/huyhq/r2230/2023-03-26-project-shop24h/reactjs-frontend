import { TableHead, TableRow, TableCell, TableSortLabel, Box } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProduct } from "../../../../../action/product.action";
import { StyledTableCell } from "../../../../../style/table-mui.style";

const headCells = [
    {
        id: 'name',
        align: 'left',
        padding: 'normal',
        label: 'Tên sản phẩm',
    },
    {
        id: 'color',
        align: 'left',
        padding: 'normal',
        label: 'Màu sắc',
    },
    {
        id: 'type.name',
        align: 'left',
        padding: 'normal',
        label: 'Phân loại',
    },
    {
        id: 'buyPrice',
        align: 'center',
        padding: 'normal',
        label: 'Giá hiện tại',
    },
    {
        id: 'promotionPrice',
        align: 'center',
        padding: 'normal',
        label: 'Giá khuyến mại',
    },
    {
        id: 'amount',
        align: 'center',
        padding: 'normal',
        label: 'Số lượng',
    },
];

const TableHeadProduct = (props) => {
    const [order, setOrder] = useState("desc");
    const [orderBy, setOrderBy] = useState("dateUpdated");
    const { rowsPerPage, currentPage } = props
    
    const { search } = useSelector(data => data.adminSearchReducer);

    const dispatch = useDispatch();

    const createSortHandler = (orderById) => {
        return (event) => {
            setOrderBy(orderById);
            order == "desc" ? setOrder("asc") : setOrder("desc");
        }
    };

    useEffect(() => {
        var filter = {
            name: search
        };

        dispatch(getAllProduct(rowsPerPage, currentPage, order, orderBy, filter))
    }, [order, currentPage, rowsPerPage, search]);

    return (
        <TableHead>
            <TableRow sx={{ fontWeight: "700", background: "grey" }}>
                <StyledTableCell align="left">STT</StyledTableCell>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.align}
                        padding={headCell.padding}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                        </TableSortLabel>
                    </StyledTableCell>
                ))}
                <StyledTableCell align="center">Action</StyledTableCell>
            </TableRow>
        </TableHead>
    );
}

export default TableHeadProduct