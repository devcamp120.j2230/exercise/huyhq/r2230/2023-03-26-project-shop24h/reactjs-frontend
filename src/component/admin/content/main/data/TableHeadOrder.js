import { TableHead, TableRow, TableCell, TableSortLabel, Box } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { StyledTableCell } from "../../../../../style/table-mui.style";
import { getAllOrder } from "../../../../../action/order.action";

const headCells = [
    {
        id: 'customer.name',
        align: 'left',
        padding: 'normal',
        label: 'Khách hàng',
    },
    {
        id: 'product',
        align: 'left',
        padding: 'normal',
        label: 'Sản phẩm',
    },
    {
        id: 'amount',
        align: 'left',
        padding: 'normal',
        label: 'Số lượng',
    },
    {
        id: 'orderDate',
        align: 'center',
        padding: 'normal',
        label: 'Ngày đặt hàng',
    },
    {
        id: 'shippedDate',
        align: 'center',
        padding: 'normal',
        label: 'Ngày giao',
    },
    {
        id: 'note',
        align: 'center',
        padding: 'normal',
        label: 'Ghi chú',
    },
    {
        id: 'status',
        align: 'center',
        padding: 'normal',
        label: 'Trạng thái',
    },
];

const TableHeadOrder = (props) => {
    const [order, setOrder] = useState("desc");
    const [orderBy, setOrderBy] = useState("orderDate");
    const { rowsPerPage, currentPage } = props

    const dispatch = useDispatch();

    const createSortHandler = (orderById) => {
        return (event) => {
            setOrderBy(orderById);
            order == "desc" ? setOrder("asc") : setOrder("desc");
        }
    };

    useEffect(() => {
        dispatch(getAllOrder(rowsPerPage, currentPage, order, orderBy))
    }, [order, rowsPerPage, currentPage]);

    return (
        <TableHead>
            <TableRow sx={{ fontWeight: "700", background: "grey" }}>
                <StyledTableCell align="left">STT</StyledTableCell>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.align}
                        padding={headCell.padding}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                        </TableSortLabel>
                    </StyledTableCell>
                ))}
                <StyledTableCell align="center">Action</StyledTableCell>
            </TableRow>
        </TableHead>
    );
}

export default TableHeadOrder