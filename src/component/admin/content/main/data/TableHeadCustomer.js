import { TableHead, TableRow, TableCell, TableSortLabel, Box } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StyledTableCell } from "../../../../../style/table-mui.style";
import { getAllCustomer } from "../../../../../action/customer.action";

const headCells = [
    {
        id: 'fullName',
        align: 'left',
        padding: 'normal',
        label: 'Tên khách hàng',
    },
    {
        id: 'phone',
        align: 'left',
        padding: 'normal',
        label: 'SĐT',
    },
    {
        id: 'email',
        align: 'left',
        padding: 'normal',
        label: 'Email',
    },
    {
        id: 'city',
        align: 'left',
        padding: 'normal',
        label: 'Địa chỉ',
    },
    {
        id: 'group',
        align: 'left',
        padding: 'normal',
        label: 'Phân loại nhóm',
    },
];

const TableHeadCustomer = (props) => {
    const [order, setOrder] = useState("desc");
    const [orderBy, setOrderBy] = useState("dateUpdated");
    const { rowsPerPage, currentPage } = props

    const dispatch = useDispatch();
    const { search } = useSelector(data => data.adminSearchReducer);

    const createSortHandler = (orderById) => {
        return (event) => {
            setOrderBy(orderById);
            order == "desc" ? setOrder("asc") : setOrder("desc");
        }
    };

    useEffect(() => {
        var filter = ""
        if(search !== ""){
            filter = {
                all: search
            }
        }
        dispatch(getAllCustomer(rowsPerPage, currentPage, order, orderBy, filter))
    }, [order, rowsPerPage, currentPage, search]);

    return (
        <TableHead>
            <TableRow sx={{ fontWeight: "700", background: "grey" }}>
                <StyledTableCell align="left">STT</StyledTableCell>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.align}
                        padding={headCell.padding}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                        </TableSortLabel>
                    </StyledTableCell>
                ))}
                <StyledTableCell align="center">Action</StyledTableCell>
            </TableRow>
        </TableHead>
    );
}

export default TableHeadCustomer