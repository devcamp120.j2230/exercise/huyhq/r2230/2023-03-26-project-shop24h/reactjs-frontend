import { Grid, TableContainer, TableHead, TableRow, TableBody, Paper, Table, Pagination, Stack, IconButton, Typography, Button } from "@mui/material"
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Delete, Edit } from "@mui/icons-material";
import { changePaginationOrder, getAllOrder } from "../../../../action/order.action";
import { StyledTableCell, StyledTableRow } from "../../../../style/table-mui.style";
import DeleteConfirmOrderModal from "../../../modal/admin/order/DeleteConfirmOrderModal";
import { useState } from "react";
import AlertNotification from "../../../alert/AlertNotification";
import UpdateOrderModal from "../../../modal/admin/order/UpdateOrderModal";
import TableHeadOrder from "./data/TableHeadOrder";
import DetailOrderModal from "../../../modal/admin/order/DetailOrderModal";

const theme = createTheme({
    palette: {
        danger: {
            main: '#e53e3e',
            darker: '#053e85',
        },
    },
});

const AdminOrderList = () => {
    const [rowsPerPage, setRowsPerPage] = useState(3);
    const [alert, setAlert] = useState({ status: false, state: "info", message: "" });
    const [order, setOrder] = useState("");
    const [idOrder, setIdOrder] = useState("");
    const [idCustomer, setIdCustomer] = useState("");
    const [orderDetail, setOrderDetail] = useState("");

    const [openModalDeleteConfirm, setOpenModalDeleteConfirm] = useState(false)
    const [openModalUpdate, setOpenModalUpdate] = useState(false)
    const [openModalDetail, setOpenModalDetail] = useState(false)

    const dispatch = useDispatch();

    const { orders, currentPage, countPage, stateUpdateOrder, stateDeleteOrder } = useSelector((data) => data.orderReducer);

    useEffect(() => {
        //thay đổi trạng thái xóa order sẽ hiện thông báo
        if (stateDeleteOrder !== "") {
            setAlert({ status: true, state: stateDeleteOrder, message: "Xóa đơn hàng thành công!" })
        }

        //thay đổi trạng thái update order sẽ hiện thông báo
        if (stateUpdateOrder !== "") {
            setAlert({ status: true, state: stateUpdateOrder, message: "Cập nhật đơn hàng thành công!" })
        }

    }, [stateUpdateOrder, stateDeleteOrder])

    //click phân trang
    const onChangePagination = (event, value) => {
        dispatch(changePaginationOrder(value));
    };

    //xử lý số dòng trên trang
    const handleChangeRowsPerPage = (e) => {
        setRowsPerPage(e.target.value);
        // setPage(currentPage);
    };
    //click show Product detail
    const onClickProductDetail = (value)=>{
        return (e)=>{
            setOpenModalDetail(true)
            setOrder(value);
        }
    }
    //click Button Edit
    const onClickBtnEdit = (e) => {
        setOpenModalUpdate(true);
        setOrder(e.target.value);
    }

    //click Button Delete
    const onClickBtnDelete = (e) => {
        setOpenModalDeleteConfirm(true);
        var data = JSON.parse(e.target.value);

        var orderId = data.orderId;
        var idCustomer = data.idCustomer;
        var orderDetail = data.orderDetail;

        var idOrderDetail = [];
        orderDetail.map((order) => {
            idOrderDetail.push(order._id);
        })

        setIdOrder(orderId);
        setIdCustomer(idCustomer);
        setOrderDetail(JSON.stringify(idOrderDetail));
    }
    return (
        <Grid container>
            {
                alert.status
                    ? <Grid item xs={12}>
                        <AlertNotification
                            status={alert.status}
                            state={alert.state}
                            message={alert.message}
                            getStatus={(data) => { setAlert({ status: data, state: "info", message: "" }) }}
                        />
                    </Grid>
                    : false
            }
            <Grid item xs={12}>
                <Typography component="div" variant="h5">Quản lý đơn hàng</Typography>
            </Grid>
            <Grid item xs={12}>
                <Grid container alignItems='center'>
                    <Grid item xs={6}>
                        <Typography component="span">Num rows: </Typography>
                        <Typography
                            component="select"
                            defaultValue={rowsPerPage}
                            width={50}
                            border="none"
                            onChange={handleChangeRowsPerPage}
                        >
                            <option value={3}>3</option>
                            <option value={5}>5</option>
                            <option value={10}>10</option>
                        </Typography>
                    </Grid>
                    <Grid item xs={6} textAlign="right">
                        <Stack spacing={2} mb={3} sx={{ float: "right" }}>
                            <Pagination count={countPage} page={currentPage} onChange={onChangePagination} />
                        </Stack>
                    </Grid>
                </Grid>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHeadOrder
                            currentPage={currentPage}
                            rowsPerPage={rowsPerPage}
                        />
                        <TableBody>
                            {
                                orders
                                    ? orders.map((order, i) => {
                                        return <StyledTableRow
                                            key={i}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <StyledTableCell component="th" scope="row">
                                                {(i + 1) + (rowsPerPage * (currentPage - 1))}
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{order.customer.fullName}</StyledTableCell>
                                            <StyledTableCell align="left">
                                                <ThemeProvider theme={theme}>
                                                    <Typography component="a" sx={{cursor: "pointer", "&:hover":{color: "blue"}}} onClick={onClickProductDetail(JSON.stringify(order))}>
                                                        {
                                                            order.orderDetail.map((item, i) => {
                                                                return <Typography component="div" key={i}>{item.product.name}</Typography>
                                                            })
                                                        }
                                                    </Typography>
                                                </ThemeProvider>
                                            </StyledTableCell>
                                            <StyledTableCell align="left">
                                                {
                                                    order.orderDetail.map((item, i) => {
                                                        return <Typography component="div" key={i}>{item.quantity}</Typography>
                                                    })
                                                }
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{order.orderDate}</StyledTableCell>
                                            <StyledTableCell align="left">{order.shippedDate}</StyledTableCell>
                                            <StyledTableCell align="left">{order.note}</StyledTableCell>
                                            <StyledTableCell align="center">{order.status}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <ThemeProvider theme={theme}>
                                                    <IconButton onClick={onClickBtnEdit} value={JSON.stringify(order)}>
                                                        <Edit color="primary" sx={{ pointerEvents: 'none' }} />
                                                    </IconButton>
                                                    <IconButton
                                                        onClick={onClickBtnDelete}
                                                        value={
                                                            JSON.stringify({
                                                                orderId: order._id,
                                                                idCustomer: order.customer._id,
                                                                orderDetail: order.orderDetail,
                                                            })
                                                        }
                                                    >
                                                        <Delete color="danger" sx={{ pointerEvents: 'none' }} />
                                                    </IconButton>
                                                </ThemeProvider>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    })
                                    : false
                            }

                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            {
                openModalDeleteConfirm
                    ? <Grid item xs={12}>
                        <DeleteConfirmOrderModal
                            getModalStatus={(data) => { setOpenModalDeleteConfirm(data) }}
                            idOrder={idOrder}
                            idCustomer={idCustomer}
                            orderDetail={orderDetail}
                        />
                    </Grid>
                    : false
            }
            {
                openModalUpdate
                    ? <Grid item xs={12}>
                        <UpdateOrderModal
                            getModalStatus={(data) => { setOpenModalUpdate(data) }}
                            order={order}
                        />
                    </Grid>
                    : false
            }
            {
                openModalDetail
                    ? <Grid item xs={12}>
                        <DetailOrderModal
                            getModalStatus={(data) => { setOpenModalDetail(data) }}
                            order={order}
                        />
                    </Grid>
                    : false
            }

        </Grid>
    )
}

export default AdminOrderList;