import { Grid, Typography, Button, TextField } from "@mui/material";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useSelector } from "react-redux";

const AdminHeader = (props) => {
    const {getStateNav, getSearch} = props;
    const {user} = useSelector(data=>data.userReducer);
    return (
        <Grid container height={62}>
            <Grid item xs={8}>
                <Typography component="span" mr={2}>
                    <Button onClick={()=>{getStateNav()}}>
                        <FontAwesomeIcon icon={faBars} />
                    </Button>
                </Typography>
                <Typography component="a" href="/admin/home">Home</Typography>
            </Grid>
            <Grid item xs={2}>
                <TextField fullWidth label="search" variant="filled" onChange={(e)=>{getSearch(e.target.value)}}/>
            </Grid>
            <Grid item xs={2} justifyContent="flex-end">
                <Typography component="div" sx={{ float: "right", cursor: "pointer" }}>
                    <Typography
                        borderRadius="50%"
                        component="img"
                        src={user.imgUrl}
                        maxWidth="40px" />
                </Typography>
            </Grid>
        </Grid>
    )
}

export default AdminHeader;