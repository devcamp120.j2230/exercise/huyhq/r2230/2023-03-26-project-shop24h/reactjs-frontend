
import { Divider, Grid } from "@mui/material";
import AdminNavigation from "./nav/AdminNavigation";
import AdminHeader from "./head/AdminHeader";
import { Route, Routes } from "react-router-dom";
import { routeAdmin } from "../../../routes";
import AdminHome from "./main/AdminHome";
import { useDispatch, useSelector } from 'react-redux';
import { useState } from "react";

const AdminContent = () => {
    const { checkNavOpen } = useSelector((data) => data.checkNavAdminReducer)
    const dispatch = useDispatch();

    return (
        <Grid container>
            {
                checkNavOpen
                    ? <Grid item xs={6} sm={6} md={4} lg={2}>
                        <AdminNavigation />
                    </Grid>
                    : false
            }
            <Grid item
                xs={checkNavOpen ? 6 : 12}
                sm={checkNavOpen ? 6 : 12}
                md={checkNavOpen ? 8 : 12}
                lg={checkNavOpen ? 10 : 12}
            >
                <Grid container flexDirection="column" width="100%">
                    <Grid item p={3}>
                        <AdminHeader
                            getStateNav={() => { dispatch({ type: "ADMIN_NAV_TOGGLE" }) }}
                            getSearch={(data) => { dispatch({ type: "ADMIN_SEARCH", data: data }) }}
                        />
                    </Grid>
                    <Grid item>
                        <Divider sx={{ borderColor: "#515151" }} />
                    </Grid>
                    <Grid item p={3}>
                        <Routes>
                            {
                                routeAdmin.map((route, index) => {
                                    if (route.path) {
                                        return <Route path={route.path} element={route.element} key={index} />
                                    } else {
                                        return null
                                    }
                                })
                            }
                            <Route path="/*" element={<AdminHome />} />
                        </Routes>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default AdminContent;