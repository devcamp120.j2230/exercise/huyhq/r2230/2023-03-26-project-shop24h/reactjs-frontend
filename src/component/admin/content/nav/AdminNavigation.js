import { Box, List, Typography, Grid, ListItem, ListItemButton, ListItemText, createTheme, ThemeProvider } from '@mui/material';
import { themeList } from '../../../../style/theme-button.style';
import { useLocation } from 'react-router-dom';

const arrList = [
    { name: "Sản phẩm", path: "/admin/products" },
    { name: "Khách hàng", path: "/admin/customers" },
    { name: "Đơn hàng", path: "/admin/orders" },
]

const AdminNavigation = () => {
    const location = useLocation();

    return (
        <Box sx={{
            position: "sticky",
            width: "100%",
            minHeight: "100vh",
            height: "100%",
            background: "#3C4B64",
            color: "#ffff"
        }}>
            <Grid container flexDirection="column">
                <Grid item
                    justifyContent="center"
                    textAlign='center'
                    p={3}
                    height={110}
                    sx={{
                        background: "#303C54"
                    }}
                >
                    <Typography component="a" variant="h4" href="/admin/home" color="white">Bagbags</Typography>
                </Grid>
                <Grid item
                    justifyContent="center"
                    textAlign='center'
                >
                    <Box>
                        <List>
                            {
                                arrList.map((data, i) => {
                                    return <ListItem disablePadding key={i}>
                                        <ThemeProvider theme={themeList}>
                                            <ListItemButton component="a" href={data.path} selected={data.path === location.pathname ? true : false}>
                                                <ListItemText primary={data.name} />
                                            </ListItemButton>
                                        </ThemeProvider>
                                    </ListItem>
                                })
                            }
                        </List>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
}

export default AdminNavigation;