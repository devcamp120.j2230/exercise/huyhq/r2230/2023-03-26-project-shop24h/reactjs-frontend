import { Container } from "@mui/material";
import AdminContent from "./content/AdminContent";
import { useSelector } from "react-redux";
import NotFound from "../page/NotFound.404";

const AdminComponent = () => {
    const { authorization, pendingAuth } = useSelector((data) => data.userReducer);

    return (
        <>
            {
                !pendingAuth
                    ? authorization
                        ? <Container maxWidth={false} disableGutters>
                            <AdminContent />
                        </Container>
                        : <NotFound />
                    : false
            }
        </>

    )
}

export default AdminComponent;