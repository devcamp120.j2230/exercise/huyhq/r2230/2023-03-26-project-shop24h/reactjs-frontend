import { Grid, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import logo from "../../../assets/images/logo.svg"
const HeaderLogo = () => {
    const navigate = useNavigate();	
    return (
        <Grid item md={6} lg={4} textAlign="center">
            <Typography
                component="img"
                src={logo}
                onClick={()=>{navigate("/")}}
                maxWidth="250px"
                sx={{
                    cursor: "pointer"
                }}
            />
        </Grid>
    );
};

export default HeaderLogo;