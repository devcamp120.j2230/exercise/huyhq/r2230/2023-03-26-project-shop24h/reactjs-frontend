import { faLocationDot, faMessage } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid, Typography } from "@mui/material";

const HeaderContact = () => {
    return (
        <Grid item md={3} lg={4} textAlign="left">
            <Grid container sx={{justifyContent: {xs: "center", sm: "center", md: "left"}}} >
                <Grid item mx={2}>
                    <Typography component="a" href="#aboutus" color="text.secondary">
                        <FontAwesomeIcon icon={faLocationDot} fontSize="20px"/>
                        <Typography component="span" ml={1} fontSize="13px">Về chúng tôi</Typography>
                    </Typography>
                </Grid>
                <Grid item mx={2}>
                    <Typography component="a" href="#contactus" color="text.secondary">
                        <FontAwesomeIcon icon={faMessage} fontSize="20px"/>
                        <Typography component="span" ml={1} fontSize="13px">Chat với chúng tôi</Typography>
                    </Typography>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default HeaderContact;