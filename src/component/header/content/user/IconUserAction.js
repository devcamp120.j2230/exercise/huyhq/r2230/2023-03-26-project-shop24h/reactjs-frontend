import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Menu, MenuItem, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import auth from "../../../../firebase/config";
import { signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { logoutAction } from "../../../../action/authen.action";
import userIcon from "../../../../assets/images/user-avatar.png";

const IconUserAction = () => {
    const navigate = useNavigate();
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const dispatch = useDispatch();
    const { user } = useSelector((reduxData) => {
        return reduxData.userReducer
    })
    // console.log(user);

    //chức năng log out
    const clickBtnLogout = () => {
        // signOut(auth)
        //     .then(() => {
        //         dispatch({
        //             type: "LOGOUT",
        //         });
        //     })
        //     .catch((error) => {
        //         console.error(error);
        //     })
        dispatch(logoutAction());
    }
    return (
        <>
            <Typography
                component="a"
                aria-label="more"
                id="long-button"
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup="true"
                sx={{ cursor: "pointer" }}
                onClick={handleClick}
            >
                {
                    user
                        ? <Typography component="img" src={user.imgUrl !== '' ? user.imgUrl : userIcon} sx={{ width: "30px", borderRadius: "50%" }} />
                        : <FontAwesomeIcon icon={faUser} />
                }
            </Typography>
            <Menu
                id="long-menu"
                MenuListProps={{
                    'aria-labelledby': 'long-button',
                }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                PaperProps={{
                    style: {
                        maxHeight: '200px',
                        width: 'auto',
                    },
                }}
            >
                {
                    user
                        ? <>
                            <Typography component="div" color="text.secondary" mx={2}>
                                {user.name}
                            </Typography>
                            <Typography component="div" color="text.secondary" mx={2}>
                                {user.email}
                            </Typography>
                            <MenuItem selected={false} onClick={handleClose}>
                                <Typography component="a" href="/user">Người dùng</Typography>
                            </MenuItem>
                            <MenuItem selected={false} onClick={handleClose}>
                                <Typography component="a" onClick={clickBtnLogout} href="/">Đăng xuất</Typography>
                            </MenuItem>
                        </>

                        : <MenuItem selected={false} onClick={handleClose}>
                            <Typography component="a" href="/login">Đăng nhập</Typography>
                        </MenuItem>
                }
            </Menu>
        </>
    )
}

export default IconUserAction