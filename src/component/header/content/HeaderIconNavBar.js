import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell } from "@fortawesome/free-solid-svg-icons";
import { Grid, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import cartIcon from "../../../assets/images/icon-cart.svg"
import IconUserAction from "./user/IconUserAction";
import HeaderSearch from "./search/HeaderSearch";

const HeaderIconNavBar = () => {
    const { itemNum } = useSelector((reduxData) => {
        return reduxData.cartReducer;
    })   

    return (
        <Grid item md={3} lg={4}>
            <Grid container sx={{justifyContent:{xs: "center", sm: "center", md: "right"}}} lineHeight={3}>
                {/* <HeaderSearch/> */}
                <Grid item>
                    <Grid container>
                        {/* <Grid item mr={2}>
                            <a href="#" className="header-icon">
                                <FontAwesomeIcon icon={faBell} />
                            </a>
                        </Grid> */}
                        <Grid item mr={2}>
                            <IconUserAction/>
                        </Grid>
                        <Grid item mt="-2px">
                            <a href="/cart" className="header-icon me-3">
                                <Typography component="img" src={cartIcon} width="30px"/>
                                {/* <FontAwesomeIcon icon={faCartShopping} /> */}
                                <Typography
                                    component="span"
                                    variant="span"
                                    textAlign="center"
                                    sx={{
                                        position: "relative",
                                        top: "5px",
                                        left: "-24px",
                                        padding: "1px 5px",
                                        fontSize: "12px",
                                        // background: "gray",
                                        // color: "white",
                                        borderRadius: "50%"
                                    }}
                                >
                                    {itemNum}
                                </Typography>
                            </a>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
};

export default HeaderIconNavBar;