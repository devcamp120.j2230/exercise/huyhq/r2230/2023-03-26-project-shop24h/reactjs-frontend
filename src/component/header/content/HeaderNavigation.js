import { Grid, Button, Box } from "@mui/material";
import { useLocation } from "react-router-dom";

const links = [
    { link: "/", name: "Trang chủ" },
    { link: "/products", name: "Sản phẩm" },
    { link: "/news", name: "Tin tức" },
]
const HeaderNavigation = () => {
    const location = useLocation();
    return (
        <Grid item xs={12}>
            <Box
                sx={{
                    flexGrow: 1,
                    display: { xs: 'block', sm: 'block', md: 'flex' },
                    justifyContent: { md: 'center' },
                    textAlign: { xs: 'left', sm: 'left', md: 'center' }
                }}
            >
                {
                    links.map((elm, i) => {
                        return <Button
                            sx={{
                                p: 1,
                                borderTop: {xs: "1px solid #cacaca", md: "none"},
                                borderBottom: {xs: "1px solid #cacaca", md: location.pathname === elm.link ? "1px solid red" : "none"},
                                borderRadius: 0,
                                m: {xs: "0 0", md: "8px 24px"},
                                color: "#515151",
                                display: 'block',
                                textTransform: "uppercase",
                                fontSize: "20px",
                                "&: hover": {borderBottom: "1px solid red", background: "#ffff"}
                            }}
                            href={elm.link}
                        >
                            {elm.name}
                        </Button>
                    })
                }
            </Box>
        </Grid>
    )
}

export default HeaderNavigation;