import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { Grid, Typography, Button } from "@mui/material";

const HeaderSearch = () => {
    return (
        <Grid item sx={{ position: 'relative', }}>
            <Typography
                component="input"
                type="text"
                placeholder="Search"
                borderRadius="20px"
                border="1px solid #959595"
                padding="6px"
                mr={2}
            />
            <Button
                sx={{
                    position: 'absolute',
                    top: '10px',
                    right: '10px',
                }}
            >
                <FontAwesomeIcon icon={faSearch} />
            </Button>

        </Grid>
    )
}

export default HeaderSearch;