import { Container } from "@mui/material";
import HeaderDrawer from "./HeaderDrawer";

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"></link>
const HeaderComponent = () => {
    return (
        <Container maxWidth={false} disableGutters>
            <HeaderDrawer/>
        </Container>
    );
}

export default HeaderComponent;