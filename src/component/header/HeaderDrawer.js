import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Container, AppBar, Box, CssBaseline, Divider, Drawer, IconButton, List, Typography, Toolbar, Grid } from '@mui/material';
import { useState } from 'react';
import HeaderContact from './content/HeaderContact';
import HeaderIconNavBar from './content/HeaderIconNavBar';
import HeaderLogo from './content/HeaderLogo';
import logo from "../../assets/images/logo.svg"
import HeaderNavigation from './content/HeaderNavigation';
import { useNavigate } from 'react-router-dom';

const drawerWidth = 300;

const HeaderDrawer = () => {
    const navigate = useNavigate();
    const [mobileOpen, setMobileOpen] = useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Grid container mt={3} pl={0}>
                <Grid item xs={12}>
                    <HeaderContact />
                </Grid>
                <Grid item xs={12} my={1}>
                    <Divider sx={{ color: "#515151" }} />
                </Grid>
                <Grid item xs={12} my={1}>
                    <Divider sx={{ color: "#515151" }} />
                </Grid>
                <Grid item xs={12}>
                    <HeaderNavigation />
                </Grid>
            </Grid>

        </Box>
    );

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <Container maxWidth={false} disableGutters sx={{ boxShadow: "-2px 10px 10px -10px #ccc" }}>
                <Toolbar>
                    <Box sx={{ width: "100%", display: { md: 'none' } }}>
                        <Grid container alignItems="center">
                            <Grid item xs={8}>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    edge="start"
                                    onClick={handleDrawerToggle}
                                >
                                    <FontAwesomeIcon icon={faBars} />
                                </IconButton>
                                <Typography component="img" src={logo} width="150px" m={2} onClick={() => { navigate("/") }} />
                            </Grid>
                            <Grid item xs={4} textAlign="right" ml="auto">
                                <HeaderIconNavBar />
                            </Grid>
                        </Grid>
                    </Box>
                    <Grid container
                        sx={{ display: { xs: 'none', sm: 'none', md: 'flex' } }}
                        mt={3}
                    // boxShadow="-2px 10px 10px -10px #ccc"
                    >
                        <Grid item xs={12}>
                            <Grid container>
                                <HeaderContact />
                                <HeaderLogo />
                                <HeaderIconNavBar />
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <HeaderNavigation />
                        </Grid>
                    </Grid>
                </Toolbar>
            </Container>
            <Box component="nav">
                <Drawer
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { sm: 'block', md: 'none' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                    {drawer}
                </Drawer>
            </Box>
        </Box>

    )
}

export default HeaderDrawer