import { faFacebook, faInstagram, faTwitter, faYoutube } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const FooterSocial = () => {
    return (
        <>
            <a href="#facebook" className="me-3">
                <FontAwesomeIcon icon={faFacebook} />
            </a>
            <a href="#youtube" className="me-3">
                <FontAwesomeIcon icon={faYoutube} />
            </a>
            <a href="#instagram" className="me-3">
                <FontAwesomeIcon icon={faInstagram} />
            </a>
            <a href="#twitter" className="me-3">
                <FontAwesomeIcon icon={faTwitter} />
            </a>
        </>
    )
}

export default FooterSocial;