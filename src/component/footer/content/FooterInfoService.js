import { Grid, List, Typography, ListItem, ListItemButton } from "@mui/material";

const showrooms = [
    { name: "Bagbags Long Biên", map: [21.028535909029625, 105.89924655397762] },
    { name: "Bagbags Mỹ Đình", map: [21.015886871878873, 105.77772000563161] },
    { name: "Bagbags Lê Thanh Nghị", map: [21.001866256313832, 105.84497382482283] },
    { name: "Bagbags Cầu Giấy", map: [21.03434649108983, 105.79500392861087] },
    { name: "Bagbags Hà Đông", map: [20.989904018964, 105.75179199649806] },
]
const FooterInfoService = (props) => {
    const {getLocationMap} = props;
    const setLocatonMap = (data)=>{
        return (e)=>{
            // console.log(data);
            getLocationMap(data)
        }
    }
    return (
        <Grid container flexDirection="column" justifyContent="center" textAlign="left" gap={2} pl={3} mb={3} color="#ffff">
            <Typography component="div" fontWeight={700}>HỆ THỐNG SHOWROOM</Typography>
            <List>
                {
                    showrooms.map((showroom, i) => {
                        return <ListItem disablePadding key={i}>
                            <ListItemButton onClick={setLocatonMap(showroom.map)}>
                                <Typography component="div">{showroom.name}</Typography>
                            </ListItemButton>
                        </ListItem>
                    })
                }
            </List>
        </Grid>
    )
}
export default FooterInfoService