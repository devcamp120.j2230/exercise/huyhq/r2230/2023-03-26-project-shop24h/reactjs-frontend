import { Divider, Grid, Typography } from "@mui/material";
import logo from "../../../assets/images/logo.svg";

const FooterInfoProduct = () => {
    return (
        <Grid container flexDirection="column" justifyContent="center" textAlign="left" gap={2} pl={3} mb={3} color="#ffff">
            <Typography component="img" src={logo} maxWidth="150px" sx={{ filter: "invert(100%)" }} />
            <Typography component="div" fontWeight={700}>BAGBAGS CO.,LTD</Typography>
            <Typography component="div">Tax code: 0123456789</Typography>
            <Typography component="div">Head Office: Lê Thanh Nghị Street, Hai Bà Trưng District, Hà Nội City</Typography>
        </Grid>

    )
}
export default FooterInfoProduct