import { Grid, Typography } from "@mui/material";
import FooterSocial from "./content/FooterSocial";

const FooterContact = () => {
    return (
        <Grid item xs={12} textAlign="center" mt={5}>
            <Typography component="div" variant="h6" fontWeight={700} mb={2} textTransform="uppercase">LIÊN HỆ VỚI CHÚNG TÔI</Typography>
            <Grid container justifyContent="center" gap={2}>
                <Grid item
                    width="400px"
                    textAlign="center"
                    p="10px"
                    sx={{
                        border: "1px solid #acacac",
                        borderRadius: "50px"
                    }}
                >
                    <Typography component="span" variant="h6" mr={2}>SỐ ĐIỆN THOẠI:</Typography>
                    <Typography component="span" variant="h6" fontWeight={700}>0123.456.789</Typography>
                </Grid>
                <Grid item
                    width="400px"
                    textAlign="center"
                    p="10px"
                    sx={{
                        border: "1px solid #acacac",
                        borderRadius: "50px"
                    }}
                >
                    <Typography component="span" variant="h6" mr={2}>MẠNG XÃ HỘI:</Typography>
                    <FooterSocial/>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default FooterContact;