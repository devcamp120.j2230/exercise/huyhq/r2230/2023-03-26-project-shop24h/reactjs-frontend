import { Container, Divider, Grid } from "@mui/material";
import { Row } from "reactstrap";
import FooterInfoProduct from "./content/FooterInfoProduct";
import FooterInfoService from "./content/FooterInfoService";
import FooterInfoMap from "./content/FooterInfoMap";
import FooterSocial from "./content/FooterSocial";
import FooterContact from "./FooterContact";
import GetMapGoogle from "../handle/google/GetMapGoogle";
import { useState } from "react";

const FooterComponent = () => {
    const [locationMap, setLocationMap] = useState([21.028535909029625, 105.89924655397762]);

    return (
        <Container maxWidth={false} disableGutters>
            <Grid container mt={5} sx={{ boxShadow: "-2px -10px 10px -10px #ccc" }}>
                <Grid container>
                    <FooterContact />
                </Grid>
                <Grid container mt={3} sx={{ background: "#000" }} p={3}>
                    <Grid item xs={12} sm={6} md={3} lg={2}>
                        <Grid container flexDirection="row" flexWrap="nowrap">
                            <Grid item>
                                <FooterInfoProduct />
                            </Grid>
                            <Grid item>
                                <Divider
                                    orientation="vertical"
                                    sx={{ borderColor: "#fff" }} />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={6} md={3} lg={2}>
                        <FooterInfoService getLocationMap = {(data)=>{setLocationMap(data)}}/>
                    </Grid>
                    <Grid item xs={11} sm={11} md={5} lg={7} p={3} height={{xs: "300px", md:"auto"}} mx="auto">
                        <GetMapGoogle locationMap={JSON.stringify(locationMap)}/>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
};

export default FooterComponent;