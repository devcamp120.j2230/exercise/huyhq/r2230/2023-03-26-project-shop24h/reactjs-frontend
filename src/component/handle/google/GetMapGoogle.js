import { WhereToVote } from '@mui/icons-material';
import GoogleMapReact from 'google-map-react';
import { useState } from 'react';
import { useEffect } from 'react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const GetMapGoogle = (props) => {
    const location = JSON.parse(props.locationMap)
    const [lat, setLat] = useState(location? location[0]: 21.028535909029625)
    const [lng, setLng] = useState(location? location[1]: 105.89924655397762)
    const [zoom, setZoom] = useState(11)
    // console.log(location);
    
    useEffect(()=>{
        setLat(location[0]);
        setLng(location[1]);
        setZoom(13);
    },[location])

    return (
            <div style={{ height: '100%', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyDNI_ZWPqvdS6r6gPVO50I4TlYkfkZdXh8" }}
                    center={{
                        lat: lat,
                        lng: lng
                    }}
                    defaultCenter={{
                        lat: lat,
                        lng: lng
                    }}
                    defaultZoom={zoom}
                    zoom={zoom}
                >
                    <AnyReactComponent
                        lat={lat}
                        lng={lng}
                        text={<WhereToVote color='error'/>}
                    />
                </GoogleMapReact>
            </div>
    )
}

export default GetMapGoogle