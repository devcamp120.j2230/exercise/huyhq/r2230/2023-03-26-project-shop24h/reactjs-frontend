import { Button } from "@mui/material";
import { useDispatch } from "react-redux";
import { getNumberItem } from "../../action/cart.action";
import { useState } from "react";
import ModalAlertNotification from "../alert/ModalAlertNotification";

const AddToCartHandle = (props) => {
    const [openAlert, setOpenAlert] = useState(false);

    const [message, setMessage] = useState("");
    const [status, setStatus] = useState("success");

    const { product, quantity } = props;
    const dispatch = useDispatch();

    const clickBtnAddToCart = () => {
        var cartArray = []
        //lấy danh sách sản phẩm từ localstorage
        cartArray = localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : [];

        //kiểm tra sản phẩm đã được thêm vào cart chưa?
        //nếu chưa thì thêm vào cart, nếu có rồi thì thêm quantity
        var check = -1;

        if (cartArray.length !== 0) {
            cartArray.find((item, key) => {
                if (item._id === product._id) {
                    check = key
                    return true;
                } else {
                    return false;
                }
            });
        }

        if (check < 0) {
            cartArray.push({ ...product, quantity });
        } else {
            cartArray[check].quantity += quantity;
        }
        //thêm thay đổi vào localstorage
        localStorage.setItem("cart", JSON.stringify(cartArray));
        //thêm số lượng item trong cart vào reducer
        dispatch(getNumberItem(cartArray.length))
        handleAlert("success", "Cập nhật giỏ hàng thành công.")
    };

    //handel thông báo
    const handleAlert = (status, message) => {
        setOpenAlert(true);
        setStatus(status);
        setMessage(message);
    };

    return (
        <>
            <Button
                variant="contained"
                sx={{
                    margin: "20px 0px",
                    minWidth: "100%",
                    background: "black",
                    color: "white",
                }}
                onClick={clickBtnAddToCart}
            >
                Thêm vào giỏ hàng
            </Button>
            {
                openAlert
                    ? <ModalAlertNotification
                        state={status}
                        status={openAlert}
                        message={message}
                        getStatus={(val) => { setOpenAlert(val) }}
                    />
                    : false
            }
        </>
    )
}

export default AddToCartHandle;