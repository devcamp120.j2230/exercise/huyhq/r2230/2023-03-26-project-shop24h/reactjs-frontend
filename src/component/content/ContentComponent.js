import { Routes, Route } from "react-router-dom";
import { Col } from "reactstrap";
import { routeList } from "../../routes";
import HomeComponent from "./home/HomeComponent";
import HeaderComponent from "../header/HeaderComponent";
import FooterComponent from "../footer/FooterComponent";
import ModalAlertNotification from "../alert/ModalAlertNotification";
import { useDispatch, useSelector } from "react-redux";
import { getAlertHandle, getAlertState } from "../../action/alert.action";
import { useEffect } from "react";

const ContentComponent = () => {
    return (
        <>
            <HeaderComponent />
            <Col className="m-0">
                <Routes>
                    {
                        routeList.map((route, index) => {
                            if (route.path) {
                                return <Route path={route.path} element={route.element} key={index} />
                            } else {
                                return null
                            }
                        })
                    }
                    <Route path="/*" element={<HomeComponent />}></Route>
                </Routes>
            </Col>
            <FooterComponent />
        </>
    );
}

export default ContentComponent;