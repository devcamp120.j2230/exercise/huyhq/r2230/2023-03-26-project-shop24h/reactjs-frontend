import { Button, Grid } from "@mui/material";
import { useNavigate } from "react-router-dom";

const ViewAllComponent = ()=>{
    const navigate = useNavigate();
    return(
        <Grid container justifyContent="center">
            <Button variant="contained" color="inherit" onClick={()=>{navigate("/products")}}>Xem thêm</Button>
        </Grid>
    )
}

export default ViewAllComponent;