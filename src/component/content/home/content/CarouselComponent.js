import Slider from "react-slick";
import { Button, Col, Row } from "reactstrap";

const images = [
    { src: "https://www.vascara.com/uploads/banner/2023/March/31/16631680197539.jpg" },
    { src: "https://www.vascara.com/uploads/banner/2023/April/5/16691680658256.png" },
    { src: "https://www.vascara.com/uploads/banner/2023/March/31/16651680197285.jpg" },
    { src: "https://www.vascara.com/uploads/banner/2023/April/12/16901681263447.jpg" },
]
const CarouselComponent = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    };
    return (
        <Col>
            <Slider {...settings}>
                {
                    images.length > 0
                        ? images.map((image, i) => {
                            return <Row className="d-flex mx-0" key={i} style={{maxWidth: "100%"}}>
                                <Col xs="12" sm="12" md="12" className="d-flex justify-content-center p-0">
                                    <img src={image.src} width="100%" />
                                </Col>
                            </Row>
                        })
                        : false
                }
            </Slider>
        </Col>
    )
};

export default CarouselComponent;