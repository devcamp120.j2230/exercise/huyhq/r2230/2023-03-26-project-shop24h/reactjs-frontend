import CarouselComponent from "./content/CarouselComponent"
import ViewAllComponent from "./content/ViewAllComponent"
import NewProductsComponent from "../collection-product/NewProductsComponent"

const HomeComponent = () => {
    return (
        <>
            <CarouselComponent />
            <div className="p-5">
                <NewProductsComponent />
                <ViewAllComponent />
            </div>
        </>
    )
}

export default HomeComponent;