import { Container } from "@mui/material";
import UserSettting from "./content/UserSettting";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getCustomerById } from "../../../action/customer.action";
import { useNavigate } from "react-router-dom";

const UserComponent = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { customer } = useSelector(data => data.customerReducer);
    const { user, pending } = useSelector(data => data.userReducer);

    useEffect(() => {
        if (user !== null) {
            dispatch(getCustomerById(user._id))
        }
    }, [user])

    return (
        <Container>
            {
                customer !== ""
                    ? <UserSettting customer={JSON.stringify(customer)} />
                    : false
            }
        </Container>
    )
}

export default UserComponent;