import { Grid, TextField, MenuItem } from "@mui/material";
import { useState } from "react";
import cityList from "../../../../../data/data-country-vn.json"
import countryList from "../../../../../data/data-country-list.json"
import { useEffect } from "react";

const SelectAddress = (props) => {
    const address = props.address ? JSON.parse(props.address) : "";

    const [countriesData, setCountriesData] = useState(countryList)
    const [citiesData, setCitiesData] = useState([])
    const [districtsData, setDistrictsData] = useState([])

    const [country, setCountry] = useState(address.country ? address.country : "");
    const [city, setCity] = useState(address.city ? address.city : "");
    const [district, setDistrict] = useState(address.district ? address.district : "");

    //lấy country khách hàng
    const getCountryCustomer = (e) => {
        var country = e.target.value;
        setCountry(country);   
    }

    //lấy city khách hàng
    const getCityCustomer = (e) => {
        var city = e.target.value
        setCity(city);

        
    }

    //lấy district khách hàng
    const getDistrictCustomer = (e) => {
        var district = e.target.value;
        setDistrict(district);
    }

    useEffect(() => {
        if (country === "Việt Nam") {
            setCitiesData([...cityList])
        } else {
            setCity("");
            setCitiesData([]);
            setDistrict("");
            setDistrictsData([]);
        }

        if (city === "") {
            setDistrict("");
            setDistrictsData([]);
        }

        //lấy dữ liệu các quận/ huyện thành phố tương ứng
        var cityData = "";
        cityData = citiesData.find(cityData => cityData.name === city);

        if (cityData !== undefined) {
            setDistrictsData([...cityData.districts]);
        }
        
        props.getData({
            country, city, district
        });
    }, [country, city, district])

    return (
        <Grid container>
            <Grid item xs={4} pr={1}>
                <TextField select label="Quốc gia" defaultValue={country} sx={{ width: "100%" }} fullWidth onChange={getCountryCustomer}>
                    <MenuItem value="">--Lựa chọn--</MenuItem>
                    {
                        countriesData.length > 0
                            ? countriesData.map((country, i) => {
                                return <MenuItem value={country.name} key={i}>{country.name}</MenuItem>
                            })
                            : false

                    }
                </TextField>
            </Grid>
            <Grid item xs={4} pr={1}>
                <TextField select label="Tỉnh/ TP" defaultValue={city} sx={{ width: "100%" }} fullWidth onChange={getCityCustomer}>
                    <MenuItem value="">--Lựa chọn--</MenuItem>
                    {
                        citiesData.length > 0
                            ? citiesData.map((city, i) => {
                                return <MenuItem value={city.name} key={i}>{city.name}</MenuItem>
                            })
                            : false
                    }
                </TextField>
            </Grid>
            <Grid item xs={4}>
                <TextField select label="Quận/ huyện" defaultValue={district} sx={{ width: "100%" }} fullWidth onChange={getDistrictCustomer}>
                    <MenuItem value="">--Lựa chọn--</MenuItem>
                    {
                        districtsData.length > 0
                            ? districtsData.map((district, i) => {
                                return <MenuItem value={district.name} key={i}>{district.name}</MenuItem>
                            })
                            : false
                    }
                </TextField>
            </Grid>
        </Grid>
    )
}

export default SelectAddress;