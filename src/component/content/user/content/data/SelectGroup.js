import { TextField, MenuItem } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListGroupAction } from "../../../../../action/group.action";

//props = {getGroup(), group}

const SelectGroup = (props) => {
    // const [group, setGroup] = useState(props.group ? props.group : "");
    const dispatch = useDispatch();

    const { groups } = useSelector((data) => data.groupReducer);

    const onChangeGetGroup = (e) => {
        console.log(e.target.value);
        props.getGroup(e.target.value)
    };

    useEffect(() => {
        dispatch(getListGroupAction());
    }, []);

    return (
        <TextField select defaultValue={props.group ? props.group : ""} label="Phân quyền" sx={{ width: "100%" }} fullWidth onChange={onChangeGetGroup}>
            <MenuItem value="">--Lựa chọn--</MenuItem>
            {
                groups !== ""
                    ? groups.map((group, i) => {
                        return <MenuItem value={group._id} key={i}>{group.name}</MenuItem>
                    })
                    : false
            }
        </TextField>
    )
}

export default SelectGroup;