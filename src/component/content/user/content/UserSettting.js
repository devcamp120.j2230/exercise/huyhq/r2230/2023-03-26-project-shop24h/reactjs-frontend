import { AddCircleOutlineRounded } from "@mui/icons-material";
import { Box, Grid, Typography, Button, TextField, InputAdornment, IconButton } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import SelectAddress from "./data/SelectAddress";
import SelectGroup from "./data/SelectGroup";
import ModalAlertNotification from "../../../alert/ModalAlertNotification";
import { updateCustomerByIdAction } from "../../../../action/customer.action";
import { useNavigate } from "react-router-dom";
import { updateAction } from "../../../../action/authen.action";
import BreadCrumb from "../../../BreadCrumb";

const breadcrumb = [
    {
        name: "Trang chủ",
        url: "/"
    },
    {
        name: "Người dùng",
        url: "#"
    },
]

const UserSettting = (props) => {
    const customer = JSON.parse(props.customer);
    const customerId = customer._id;

    const [alert, setAlert] = useState({ status: false, state: "info", message: "" });

    const [imgSrc, setImgSrc] = useState("");
    const [imageAvatar, setImageAvatar] = useState(customer.imageAvatar ? customer.imageAvatar : "");
    const [fullName, setFullName] = useState(customer.fullName ? customer.fullName : "");
    const [email, setEmail] = useState(customer.email ? customer.email : "");
    const [password, setPassword] = useState("");
    const [repassword, setRepassword] = useState("");
    const [phone, setPhone] = useState(customer.phone ? customer.phone : "");
    const [country, setCountry] = useState("");
    const [city, setCity] = useState(customer.country ? customer.country : "");
    const [district, setDistrict] = useState(customer.district ? customer.district : "");
    const [address, setAddress] = useState(customer.address ? customer.address : "");
    const [group, setGroup] = useState(customer.group ? customer.group._id : "");

    const dispatch = useDispatch();

    const navigate = useNavigate();
    //khi click thêm ảnh
    const onInputImg = () => {
        setAlert({ status: true, state: "success", message: "Thêm ảnh thành công!" })
        setImageAvatar(imgSrc)
    };

    //lấy tên khách hàng
    const getFullNameCustomer = (e) => {
        setFullName(e.target.value);
    }

    //lấy email khách hàng
    const getEmailCustomer = (e) => {
        setEmail(e.target.value);
    }

    //lấy password khách hàng
    const getPasswordCustomer = (e) => {
        setPassword(e.target.value);
    }

    //lấy re-password khách hàng
    const getRePasswordCustomer = (e) => {
        setRepassword(e.target.value);
    }

    //lấy SDT khách hàng
    const getPhoneCustomer = (e) => {
        setPhone(e.target.value);
    }

    //lấy district khách hàng
    const getAddressCustomer = (e) => {
        setAddress(e.target.value);
    }

    //khi click button thêm sản phẩm
    const clickBtnAddProduct = () => {
        var obj = {
            fullName,
            email,
            password,
            repassword,
            phone,
            country,
            city,
            district,
            address,
            imageAvatar,
            group
        };

        if (validateData(obj)) {
            setAlert({ status: true, state: "success", message: "Cập nhật thành công." })
            var data = {
                fullName,
                email,
                phone,
                country,
                city,
                district,
                address,
                imageAvatar,
                group
            }

            if (password !== "") {
                data = { ...data, password }
            }

            //console.log(data);

            //send data
            dispatch(updateCustomerByIdAction(customerId, data));
            //update user
            dispatch(updateAction({ email }));
            //load page
            setTimeout(() => { navigate(0) }, 1000);
        }
    }

    //validate form 
    const validateData = (objData) => {
        if (objData.fullName === "") {
            setAlert({ status: true, state: "error", message: "Họ tên không được để trống" })
            return false;
        }

        if (objData.email === "") {
            setAlert({ status: true, state: "error", message: "Email không được để trống" })
            return false;
        }

        if (objData.password !== objData.repassword) {
            setAlert({ status: true, state: "error", message: "Mật khẩu xác nhận không khớp" })
            return false;
        }

        if (objData.phone === "") {
            setAlert({ status: true, state: "error", message: "Số điện thoại không được để trống" })
            return false;
        }

        return true;
    }

    return (
        <Grid container flexDirection="row" p={3}>
            <Grid item xs={12}>
                <ModalAlertNotification
                    status={alert.status}
                    state={alert.state}
                    message={alert.message}
                    getStatus={(data) => { setAlert({ status: data, state: "info", message: "" }) }}
                />
            </Grid>
            <Grid item xs={12} my={3}>
                <BreadCrumb link={JSON.stringify(breadcrumb)} />
            </Grid>
            <Grid item xs={12} mb={3}>
                <Typography component="div" variant="h5" textTransform="uppercase" fontWeight={700} mt={1}>Thông tin người dùng</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
                <Grid container flexDirection="column" height="100%">
                    <Grid maxWidth="100%" p={1} m={1} height="336px" sx={{ border: 'none', overflow: "auto" }}>
                        <Grid container justifyContent="center">
                            <Grid item xs={8} p={1}>
                                <Typography component="img" alt="Avatar" src={imageAvatar} width="100%" height="auto" sx={{ objectFit: "contain" }} />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid p={1} mt="auto">
                        <Box component="form"
                            noValidate
                            autoComplete="off"
                        >
                            <TextField
                                label="Link Ảnh đại diện"
                                variant="outlined"
                                fullWidth
                                onChange={(e) => { setImgSrc(e.target.value) }}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="toggle password visibility"
                                                onClick={onInputImg}
                                                edge="end"
                                            >
                                                <AddCircleOutlineRounded />
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }} />
                        </Box>
                    </Grid>
                    {/* <Grid p={1}>
                    <SelectGroup
                        getGroup={(value) => { setGroup(value) }}
                        group={group}
                    />
                </Grid> */}
                </Grid>
            </Grid>
            <Grid item xs={12} sm={8}>
                <Box
                    component="form"
                    noValidate
                    autoComplete="off"
                >
                    <Grid container flexDirection="row" width="100%">
                        <Grid item xs={12}>
                            <Grid container flexDirection="column">
                                <Grid item p={1}>
                                    <TextField label="Họ tên" defaultValue={fullName} required variant="outlined" fullWidth onChange={getFullNameCustomer} />
                                </Grid>
                                <Grid item p={1}>
                                    <TextField label="Email" disabled defaultValue={email} required variant="outlined" fullWidth onChange={getEmailCustomer} />
                                </Grid>
                                <Grid item p={1}>
                                    <TextField label="Password" required type="password" autoComplete="current-password" variant="outlined" fullWidth onChange={getPasswordCustomer} />
                                </Grid>
                                <Grid item p={1}>
                                    <TextField label="Re-Password" required type="password" autoComplete="current-password" variant="outlined" fullWidth onChange={getRePasswordCustomer} />
                                </Grid>
                                <Grid item p={1}>
                                    <TextField label="Số điện thoại" disabled defaultValue={phone} required variant="outlined" fullWidth onChange={getPhoneCustomer} />
                                </Grid>
                                <Grid item p={1}>
                                    <SelectAddress
                                        address={JSON.stringify({
                                            country: customer.country,
                                            city: customer.city,
                                            district: customer.district,
                                        })}
                                        getData={(data) => {
                                            setCountry(data.country);
                                            setCity(data.city);
                                            setDistrict(data.district);
                                        }}
                                    />
                                </Grid>
                                <Grid item p={1}>
                                    <TextField label="Địa chỉ" defaultValue={address} variant="outlined" fullWidth onChange={getAddressCustomer} />
                                </Grid>
                            </Grid>
                        </Grid>

                    </Grid>
                </Box>
            </Grid>
            <Grid item xs={12} textAlign="right">
                <Button variant="contained" color="inherit" sx={{ marginRight: "10px" }} onClick={() => { navigate(-1) }}>Quay lại</Button>
                <Button variant="contained" color="info" onClick={clickBtnAddProduct}>Cập nhật</Button>
            </Grid>
        </Grid>
    )
}

export default UserSettting