import { Modal, Box, Grid, Typography, Button } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "auto",
    height: "auto",
    p: 4,
};

const ShowImageModal = (props) => {
    const [modalOpen, setModalOpen] = useState(props.setModal);

    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)

    };

    useEffect(() => {
        setModalOpen(props.setModal)
    }, [props])

    return (
        <Modal
            open={modalOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container flexDirection="column" justifyContent="center">
                    <Grid item textAlign="center">
                        <Typography
                            component="img"
                            alt="Sample"
                            src={props.imgUrl}
                            width="90%"
                            height="90%"
                            sx={{
                                cursor: "pointer",
                                objectFit: "contain"
                            }}
                            onClick={handleModalClose}
                        />
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default ShowImageModal;