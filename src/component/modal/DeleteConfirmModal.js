import { Modal, Box, Grid, Typography, Button } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

//{setModal, getModalStatus(),}
const DeleteConfirmModal = (props) => {
    const [modalOpen, setModalOpen] = useState(props.setModal);

    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)

    };

    const onClickBtnConfirm = ()=>{
        setModalOpen(false)
        props.getModalStatus(true)
    };

    useEffect(() => {
        setModalOpen(props.setModal)
    }, [props])

    // const ColorButton = styled(Button)<ButtonProps>(({ theme }) => ({
    //     color: theme.palette.getContrastText(purple[500]),
    //     backgroundColor: purple[500],
    //     '&:hover': {
    //       backgroundColor: purple[700],
    //     },
    //   }));

    return (
        <Modal
            open={modalOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container flexDirection="column">
                    <Typography id="server-modal-title" variant="h6" component="div">
                        Xác nhận xóa!!!
                    </Typography>
                    <Typography id="server-modal-description" sx={{ pt: 2 }} component="div">
                        Bạn muốn xóa sản phẩm này?
                    </Typography>
                    <Grid container justifyContent="flex-end" mt={1}>
                        <Grid item mr={2}>
                            <Button variant="contained" onClick={handleModalClose}>Hủy</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="error" onClick={onClickBtnConfirm}>Xóa</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default DeleteConfirmModal;