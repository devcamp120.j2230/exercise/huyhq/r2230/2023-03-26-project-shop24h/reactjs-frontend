import { Modal, Box, Grid, Typography, Button } from "@mui/material";
import { useState } from "react";
import OrderItem from "../../../order/content/OrderItem";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: {xs:"90%", sm: "80%", md: 800},
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

//{getModalStatus(), order}
const DetailOrderModal = (props) => {
    const order = JSON.parse(props.order);

    const [modalOpen, setModalOpen] = useState(true);

    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)
    };

    return (
        <Modal
            open={modalOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Typography id="server-modal-title" variant="h6" component="div" fontWeight={700}>
                    Chi tiết đơn hàng
                </Typography>
                <Grid container flexDirection="row">
                    <Grid item xs={12} sm={12} md={6} border="1px solid #ccc" p={1}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    Họ tên: {order.customer.fullName}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    Email: {order.customer.email}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    SĐT: {order.customer.phone}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    Địa chỉ: {order.customer.address}, {order.customer.district}, {order.customer.city}, {order.customer.country}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} border="1px solid #ccc" p={1}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Typography sx={{ pt: 2 }} component="div">
                                        Sản phẩm:
                                    </Typography>
                                    <Grid item xs={12}>
                                        {
                                            order.orderDetail.map((item, i) => {
                                                const productItem = { ...item.product, quantity: item.quantity }
                                                return <OrderItem product={productItem} />
                                            })
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>

                        </Grid>
                    </Grid>
                    <Grid item xs={12} border="1px solid #ccc" p={1}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    Thành tiền: {order.subtotal} VNĐ
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    Giảm giá: {order.discount} VNĐ
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    Phí ship: {order.costShip} VNĐ
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography sx={{ pt: 2 }} component="div">
                                    Tổng: {order.total} VNĐ
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justifyContent="flex-end" mt={1}>
                        <Grid item mr={2}>
                            <Button variant="contained" color="inherit" onClick={handleModalClose}>Quay lại</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default DetailOrderModal;