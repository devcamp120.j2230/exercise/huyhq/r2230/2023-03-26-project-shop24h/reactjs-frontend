import { Modal, Box, Grid, Typography, Button } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteOrderById, deleteOrderDetailById } from "../../../../action/order.action";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

//{getModalStatus(), idOrder, idCustomer, orderDetail}
const DeleteConfirmOrderModal = (props) => {
    const [modalOpen, setModalOpen] = useState(true);
    const dispatch = useDispatch();

    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)

    };

    const onClickBtnConfirm = ()=>{
        setModalOpen(false);
        props.getModalStatus(false);
        var idOrder = props.idOrder;
        var idCustomer = props.idCustomer;

        var idOrderDetails = JSON.parse(props.orderDetail);
        //xóa order detail với id order detail
        idOrderDetails.map((idOrderDetail)=>{
            dispatch(deleteOrderDetailById(idOrder, idOrderDetail))
        })
        //xóa order với id order
        dispatch(deleteOrderById(idOrder, idCustomer))
        handleModalClose();
    };

    useEffect(() => {

    }, [])

    return (
        <Modal
            open={modalOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container flexDirection="column">
                    <Typography id="server-modal-title" variant="h6" component="div">
                        Xác nhận xóa!!!
                    </Typography>
                    <Typography id="server-modal-description" sx={{ pt: 2 }} component="div">
                        Bạn muốn xóa đơn hàng này?
                    </Typography>
                    <Grid container justifyContent="flex-end" mt={1}>
                        <Grid item mr={2}>
                            <Button variant="contained" onClick={handleModalClose}>Hủy</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="error" onClick={onClickBtnConfirm}>Xóa</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default DeleteConfirmOrderModal;