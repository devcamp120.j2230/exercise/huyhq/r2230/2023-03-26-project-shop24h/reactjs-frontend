import { Modal, Box, Grid, Typography, Button, TextField, MenuItem } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateOrderById } from "../../../../action/order.action";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: {xs:"90%", sm: "80%", md: 400},
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

//{getModalStatus(), order}
const UpdateOrderModal = (props) => {
    const order = JSON.parse(props.order);

    const [modalOpen, setModalOpen] = useState(true);
    const [idOrder, setIdOrder] = useState(order._id);

    const dispatch = useDispatch();

    const [statusOrder, setStatusOrder] = useState(order.status);
    const [note, setNote] = useState(order.note);

    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)
        setStatusOrder("");
        setNote("");

    };

    //lấy dữ liệu status
    const getStatusOrder = (e) => {
        setStatusOrder(e.target.value);
    }

    //lấy dữ liệu note
    const getNoteOrder = (e) => {
        setNote(e.target.value);
    }

    //xác nhận update
    const onClickBtnConfirm = () => {
        setModalOpen(false);
        props.getModalStatus(false);
        var obj = {
            shippedDate: Date.now(),
            status: statusOrder,
            note: note,
        }

        //update order với id order
        dispatch(updateOrderById(idOrder, obj))
        handleModalClose();
    };

    useEffect(() => {

    }, [])

    return (
        <Modal
            open={modalOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container flexDirection="column">
                    <Typography id="server-modal-title" variant="h6" component="div">
                        Cập nhật đơn hàng
                    </Typography>
                    <Grid item xs={12}>
                        <Typography sx={{ pt: 2 }} component="div">
                            Trạng thái đơn hàng:
                        </Typography>
                        <Grid item p={1} m={1}>
                            <TextField select required label="Status" value={statusOrder ? statusOrder : ""} sx={{ width: "100%" }} fullWidth onChange={getStatusOrder}>
                                <MenuItem value="">--Lựa chọn--</MenuItem>
                                <MenuItem value="open">Mới tạo</MenuItem>
                                <MenuItem value="confirm">Xác nhận</MenuItem>
                                <MenuItem value="cancel">Hủy đơn</MenuItem>
                            </TextField>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography sx={{ pt: 2 }} component="div">
                            Ghi chú:
                        </Typography>
                        <Grid item p={1} m={1}>
                            <TextField label="Note" variant="outlined" defaultValue={note ?? ""} multiline rows={4} fullWidth onChange={getNoteOrder} />
                        </Grid>
                    </Grid>
                    <Grid container justifyContent="flex-end" mt={1}>
                        <Grid item mr={2}>
                            <Button variant="contained" color="inherit" onClick={handleModalClose}>Hủy</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="info" onClick={onClickBtnConfirm}>Cập nhật</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default UpdateOrderModal;