import { Modal, Box, Grid, Typography, Button } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteProductById } from "../../../../action/product.action";
import { deleteCustomerById, softDeleteCustomerByIdAction } from "../../../../action/customer.action";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

//{getModalStatus(), idCustomer}
const DeleteConfirmCustomerModal = (props) => {
    const [modalOpen, setModalOpen] = useState(true);
    const dispatch = useDispatch();

    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)

    };

    const onClickBtnConfirm = ()=>{
        setModalOpen(false);
        props.getModalStatus(false);
        dispatch(softDeleteCustomerByIdAction(props.idCustomer));
    };

    useEffect(() => {

    }, [])

    return (
        <Modal
            open={modalOpen}
            onClose={handleModalClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container flexDirection="column">
                    <Typography id="server-modal-title" variant="h6" component="div">
                        Xác nhận xóa!!!
                    </Typography>
                    <Typography id="server-modal-description" sx={{ pt: 2 }} component="div">
                        Bạn muốn xóa khách hàng này?
                    </Typography>
                    <Grid container justifyContent="flex-end" mt={1}>
                        <Grid item mr={2}>
                            <Button variant="contained" color="inherit" onClick={handleModalClose}>Hủy</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="error" onClick={onClickBtnConfirm}>Xóa</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default DeleteConfirmCustomerModal;