import { AddCircleOutlineRounded } from "@mui/icons-material";
import { Modal, Box, Grid, Typography, Button, TextField, InputAdornment, IconButton } from "@mui/material";
import { useState } from "react";
import AlertNotification from "../../../alert/AlertNotification";
import { useDispatch } from "react-redux";
import SelectAddress from "./data/SelectAddress";
import { createCustomerAction } from "../../../../action/customer.action";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: {xs:"90%", sm: "80%", md: "auto"},
    height: "auto",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const AddCustomerModal = (props) => {
    const [modalOpen, setModalOpen] = useState(true);
    const [alert, setAlert] = useState({ status: false, state: "info", message: "" });

    const [imgSrc, setImgSrc] = useState("");
    const [imageAvatar, setImageAvatar] = useState("");
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [repassword, setRepassword] = useState("");
    const [phone, setPhone] = useState("");
    const [country, setCountry] = useState("");
    const [city, setCity] = useState("");
    const [district, setDistrict] = useState("");
    const [address, setAddress] = useState("");

    const dispatch = useDispatch();

    //xử lý mở modal
    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    //xử lý đóng modal
    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)

    };

    //khi click thêm ảnh
    const onInputImg = () => {
        setAlert({ status: true, state: "success", message: "Thêm ảnh thành công!" })
        setImageAvatar(imgSrc)
    };

    //lấy tên khách hàng
    const getFullNameCustomer = (e) => {
        setFullName(e.target.value);
    }

    //lấy email khách hàng
    const getEmailCustomer = (e) => {
        setEmail(e.target.value);
    }

    //lấy password khách hàng
    const getPasswordCustomer = (e) => {
        setPassword(e.target.value);
    }

    //lấy re-password khách hàng
    const getRePasswordCustomer = (e) => {
        setRepassword(e.target.value);
    }

    //lấy SDT khách hàng
    const getPhoneCustomer = (e) => {
        setPhone(e.target.value);
    }

    //lấy district khách hàng
    const getAddressCustomer = (e) => {
        setAddress(e.target.value);
    }

    //khi click button thêm sản phẩm
    const clickBtnAddProduct = () => {
        var obj = {
            fullName,
            email,
            password,
            repassword,
            phone,
            country,
            city,
            district,
            address,
            imageAvatar
        };

        if (validateData(obj)) {
            setAlert({ status: true, state: "success", message: "Tạo dữ liệu thành công." })
            var data = {
                fullName,
                email,
                password,
                phone,
                country,
                city,
                district,
                address,
                imageAvatar
            }
            dispatch(createCustomerAction(data));
            handleModalClose();
        }
    }

    //validate form 
    const validateData = (objData) => {
        if (objData.fullName === "") {
            setAlert({ status: true, state: "error", message: "Họ tên không được để trống" })
            return false;
        }

        if (objData.email === "") {
            setAlert({ status: true, state: "error", message: "Email không được để trống" })
            return false;
        }

        if (objData.password === "") {
            setAlert({ status: true, state: "error", message: "Mật khẩu không được để trống" })
            return false;
        }

        if (objData.password !== objData.repassword) {
            setAlert({ status: true, state: "error", message: "Mật khẩu xác nhận không khớp" })
            return false;
        }

        if (objData.phone === "") {
            setAlert({ status: true, state: "error", message: "Số điện thoại không được để trống" })
            return false;
        }

        return true;
    }

    return (
        <Modal
            open={modalOpen}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container flexDirection="row">
                    <Grid item xs={12}>
                        <AlertNotification
                            status={alert.status}
                            state={alert.state}
                            message={alert.message}
                            getStatus={(data) => { setAlert({ status: data, state: "info", message: "" }) }}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Grid maxWidth="100%" p={1} m={1} height="150px" sx={{ border: 'none', overflow: "auto" }}>
                            <Grid container justifyContent="center">
                                <Grid item xs={8} p={1}>
                                    <Typography component="img" alt="Avatar" src={imageAvatar} width="100%" height="auto" sx={{ objectFit: "contain" }} />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid p={1} m={1}>
                            <Box component="form"
                                noValidate
                                autoComplete="off"
                            >
                                <TextField
                                    label="Link Ảnh đại diện"
                                    variant="outlined"
                                    fullWidth
                                    onChange={(e) => { setImgSrc(e.target.value) }}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={onInputImg}
                                                    edge="end"
                                                >
                                                    <AddCircleOutlineRounded />
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }} />
                            </Box>
                        </Grid>
                    </Grid>
                    <Grid item xs={8}>
                        <Box
                            component="form"
                            noValidate
                            autoComplete="off"
                        >
                            <Grid container flexDirection="row" width="100%">
                                <Grid item xs={12}>
                                    <Grid container flexDirection="column">
                                        <Grid item p={1}>
                                            <TextField label="Họ tên" required variant="outlined" fullWidth onChange={getFullNameCustomer} />
                                        </Grid>
                                        <Grid item p={1}>
                                            <TextField label="Email" required variant="outlined" fullWidth onChange={getEmailCustomer} />
                                        </Grid>
                                        <Grid item p={1}>
                                            <TextField label="Password" required type="password" autoComplete="current-password" variant="outlined" fullWidth onChange={getPasswordCustomer} />
                                        </Grid>
                                        <Grid item p={1}>
                                            <TextField label="Re-Password" required type="password" autoComplete="current-password" variant="outlined" fullWidth onChange={getRePasswordCustomer} />
                                        </Grid>
                                        <Grid item p={1}>
                                            <TextField label="Số điện thoại" required variant="outlined" fullWidth onChange={getPhoneCustomer} />
                                        </Grid>
                                        <Grid item p={1}>
                                            <SelectAddress getData={(data) => {
                                                setCountry(data.country);
                                                setCity(data.city);
                                                setDistrict(data.district);
                                            }} />
                                        </Grid>
                                        <Grid item p={1}>
                                            <TextField label="Địa chỉ" variant="outlined" fullWidth onChange={getAddressCustomer} />
                                        </Grid>
                                    </Grid>
                                </Grid>

                            </Grid>
                        </Box>
                    </Grid>
                    <Grid item xs={12} textAlign="right">
                        <Button variant="contained" color="inherit" sx={{ marginRight: "10px" }} onClick={handleModalClose}>Hủy</Button>
                        <Button variant="contained" color="info" onClick={clickBtnAddProduct}>Thêm</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default AddCustomerModal;