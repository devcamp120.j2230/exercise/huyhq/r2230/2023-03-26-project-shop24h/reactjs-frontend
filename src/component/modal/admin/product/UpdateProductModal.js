import { AddCircleOutlineRounded, HighlightOff } from "@mui/icons-material";
import { Modal, Box, Grid, Typography, Button, TextField, InputAdornment, IconButton } from "@mui/material";
import { useState } from "react";
import AlertNotification from "../../../alert/AlertNotification";
import { useDispatch, useSelector } from "react-redux";
import { postProductAction, updateProductById } from "../../../../action/product.action";
import SelectProductType from "./data/SelectProductType";
import SelectProductColor from "./data/SelectProductColor";

const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: {xs:"90%", sm: "80%", md: "auto"},
    height: "auto",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const UpdateProductModal = (props) => {
    const product = JSON.parse(props.product);
    const idProduct = product._id;

    const [modalOpen, setModalOpen] = useState(true);
    const [alert, setAlert] = useState({ status: false, state: "info", message: "" });

    const [imgSrc, setImgSrc] = useState("");
    const [imageUrl, setImageUrl] = useState(product.imageUrl ? JSON.parse(product.imageUrl) : []);
    const [name, setName] = useState(product.name ? product.name : "");
    const [description, setDescription] = useState(product.description ? product.description : "");
    const [color, setColor] = useState(product.color ? JSON.parse(product.color) : []);
    const [type, setType] = useState(product.type ? product.type._id : "");
    const [buyPrice, setBuyPrice] = useState(product.buyPrice ? product.buyPrice : "");
    const [discount, setDiscount] = useState(product.promotionPrice ? (1 - product.promotionPrice / product.buyPrice) * 100 : 0);
    const [amount, setAmount] = useState(product.amount ? product.amount : "");

    const dispatch = useDispatch();

    //xử lý mở modal
    const handleModalOpen = () => {
        setModalOpen(true)
        props.getModalStatus(true)
    };

    //xử lý đóng modal
    const handleModalClose = () => {
        setModalOpen(false)
        props.getModalStatus(false)

    };

    //khi click thêm ảnh
    const onInputImg = () => {
        var arr = imageUrl;
        if (arr.length < 6) {
            setAlert({ status: true, state: "success", message: "Thêm ảnh thành công!" })
            setImageUrl([...arr, imgSrc])
        } else {
            setAlert({ status: true, state: "warning", message: "Số lượng ảnh đạt tối đa!" })
        }
    };

    //click xóa ảnh
    const clickRemoveImg = (position)=>{
        return (e)=>{
            var arr = imageUrl;
            arr.splice(position,1);
            setImageUrl([...arr])
            setAlert({ status: true, state: "success", message: "Xóa ảnh thành công!" })
        }
    }
    //lấy dữ liệu tên sản phẩm
    const getNameProduct = (e) => {
        setName(e.target.value)
    }

    //lấy dữ liệu mô tả sp
    const getDescProduct = (e) => {
        setDescription(e.target.value);
    }

    //lấy dữ liệu màu sắc
    const getColorProduct = (value) => {
        var _color = color;
        setColor([..._color, value]);
    }

    //lấy dữ liệu giá sp
    const getBuyPriceProduct = (e) => {
        setBuyPrice(e.target.value);
    }

    //lấy dữ liệu giảm giá
    const getDiscountPriceProduct = (e) => {
        setDiscount(e.target.value);
    }

    //lấy dữ liệu số lượng
    const getAmountProduct = (e) => {
        setAmount(e.target.value);
    }

    //khi click button thêm sản phẩm
    const clickBtnAddProduct = () => {
        var obj = {
            name,
            description,
            color,
            type,
            imageUrl,
            buyPrice,
            discount,
            amount
        };
        if (validateData(obj)) {
            setAlert({ status: true, state: "success", message: "Thêm sản phẩm thành công" })
            var promotionPrice = buyPrice - buyPrice * discount / 100;

            var data = {
                name,
                description,
                color: JSON.stringify(color),
                type,
                imageUrl: JSON.stringify(imageUrl),
                buyPrice: parseInt(buyPrice),
                promotionPrice,
                amount: parseInt(amount)
            };
            dispatch(updateProductById(idProduct, data));
            handleModalClose();

        }
    }

    //validate data
    const validateData = (objData) => {
        if (objData.imageUrl.length === 0) {
            setAlert({ status: true, state: "error", message: "Hãy thêm ít nhất 1 ảnh sản phẩm." })
            return false;
        }

        if (objData.name === "") {
            setAlert({ status: true, state: "error", message: "Tên sản phẩm không được để trống" })
            return false;
        }

        if (objData.type === "") {
            setAlert({ status: true, state: "error", message: "Hãy chọn loại sản phẩm" })
            return false;
        }

        if (objData.buyPrice === "") {
            setAlert({ status: true, state: "error", message: "Giá sản phẩm không được để trống" })
            return false;
        }

        if (objData.discount === "") {
            setAlert({ status: true, state: "error", message: "Giảm giá không được để trống" })
            return false;
        }

        if (objData.amount === "") {
            setAlert({ status: true, state: "error", message: "Số lượng không được để trống" })
            return false;
        }

        if (objData.color.length === 0) {
            setAlert({ status: true, state: "error", message: "Hãy chọn ít nhất một màu sắc cho sản phẩm." })
            return false;
        }

        return true;
    }

    return (
        <Modal
            open={modalOpen}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleModal}>
                <Grid container flexDirection="row">
                    <Grid item xs={12}>
                        <AlertNotification
                            status={alert.status}
                            state={alert.state}
                            message={alert.message}
                            getStatus={(data) => { setAlert({ status: data, state: "info", message: "" }) }}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <Grid p={1} m={1}>
                            <Box component="form"
                                noValidate
                                autoComplete="off"
                            >
                                <TextField
                                    label="Link Ảnh"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    onChange={(e) => { setImgSrc(e.target.value) }}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={onInputImg}
                                                    edge="end"
                                                >
                                                    <AddCircleOutlineRounded />
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }} />
                            </Box>
                        </Grid>
                        <Grid maxWidth="100%" p={1} m={1} height="233px" sx={{ border: '1px solid #000', overflow: "auto" }}>
                            <Grid container>
                                {
                                    imageUrl.length > 0
                                        ? imageUrl.map((img, i) => {
                                            return <Grid item xs={6} p={1} key={i} position="relative">
                                                <Typography component="img" alt="Sample" src={img} width="100%" height="100px" sx={{ objectFit: "contain" }} />
                                                <IconButton color="error" size="small" sx={{position: "absolute", top: "5px", right: 0}} onClick={clickRemoveImg(i)}>
                                                    <HighlightOff />
                                                </IconButton>
                                            </Grid>
                                        })
                                        : false
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={8}>
                        <Box
                            component="form"
                            noValidate
                            autoComplete="off"
                        >
                            <Grid container flexDirection="row" width="100%">
                                <Grid item xs={6}>
                                    <Grid container flexDirection="column">
                                        <Grid item p={1} m={1}>
                                            <TextField required defaultValue={name} label="Tên sản phẩm" variant="outlined" fullWidth onChange={getNameProduct} />
                                        </Grid>
                                        <Grid item p={1} pb={0} m={1} >
                                            <TextField label="Mô tả sản phẩm" defaultValue={description} variant="outlined" multiline rows={5} fullWidth onChange={getDescProduct} />
                                        </Grid>
                                        <Grid item p={1} m={1} pt={0} mt="5px">
                                            <SelectProductColor
                                                color={color[0]}
                                                getColorProduct={getColorProduct}
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={6}>
                                    <Grid container flexDirection="column">
                                        <Grid item p={1} m={1}>
                                            <SelectProductType
                                                type={type}
                                                getTypeProduct={(value) => { setType(value) }}
                                            />
                                        </Grid>
                                        <Grid item p={1} mt={1}>
                                            <TextField type="number" defaultValue={buyPrice} required label="Giá bán (VND)" variant="outlined" fullWidth onChange={getBuyPriceProduct} />
                                        </Grid>
                                        <Grid item p={1} my={1}>
                                            <TextField type="number" defaultValue={discount} required label="Giảm giá (%)" variant="outlined" fullWidth onChange={getDiscountPriceProduct} />
                                        </Grid>
                                        <Grid item p={1}>
                                            <TextField type="number" defaultValue={amount} required label="Số lượng (number)" variant="outlined" fullWidth onChange={getAmountProduct} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid item xs={12} textAlign="right">
                        <Button variant="contained" color="inherit" sx={{ marginRight: "10px" }} onClick={handleModalClose}>Hủy</Button>
                        <Button variant="contained" color="info" onClick={clickBtnAddProduct}>Sửa</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal>
    )
}

export default UpdateProductModal;