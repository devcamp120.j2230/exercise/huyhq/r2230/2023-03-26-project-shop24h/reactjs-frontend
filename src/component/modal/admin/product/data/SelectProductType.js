import { Grid, TextField, MenuItem } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProductType } from "../../../../../action/product-type.action";

const SelectProductType = (props) => {
    const dispatch = useDispatch();
    const { productType } = useSelector((data) => data.productTypeReducer)

    const getTypeProduct = (e) => {
        props.getTypeProduct(e.target.value)
    }
    //gửi trạng thái modal
    useEffect(() => {
        // setModalOpen(props.setModal)
        dispatch(getAllProductType())
    }, [])

    return (
        <TextField select required label="Loại sản phẩm" defaultValue={props.type ? props.type : ""} sx={{ width: "100%" }} fullWidth onChange={getTypeProduct}>
            <MenuItem value="">--Lựa chọn--</MenuItem>
            {
                productType !== ""
                    ? productType.map((type, i) => {
                        return <MenuItem value={type._id} key={i}>{type.name}</MenuItem>
                    })
                    : false
            }
        </TextField>
    )
}

export default SelectProductType;