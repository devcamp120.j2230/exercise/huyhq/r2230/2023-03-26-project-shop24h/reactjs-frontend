import { TextField, MenuItem, Typography } from "@mui/material";

import colors from "../../../../../data/color-bags.data";

const SelectProductColor = (props) => {
    const getColorProduct = (e) => {
        props.getColorProduct(e.target.value)
    }

    return (
        <TextField select required label="Màu sắc" defaultValue={props.color ? props.color : ""} sx={{ width: "100%" }} fullWidth onChange={getColorProduct}>
            <MenuItem value="">--Lựa chọn--</MenuItem>
            {
                colors !== ""
                    ? colors.map((color, i) => {
                        return <MenuItem value={color.value} key={i}>
                            {color.name}
                            <Typography
                                component="div"
                                width="20px"
                                height="20px"
                                bgcolor={color.value}
                                borderRadius="50%"
                                ml="auto"
                            />
                        </MenuItem>
                    })
                    : false
            }
        </TextField>
    )
}

export default SelectProductColor;