const colorBag = [
    {name: "black", value: "#252528"},
    {name: "white", value: "#F9F6F9"},
    {name: "yellow", value: "#E4B76D"},
    {name: "gray", value: "#BDC4CF"},
    {name: "pink", value: "#EECACA"},
    {name: "cream", value: "#E9C7B0"},
    {name: "violet", value: "#D1C7DF"},
    {name: "orange", value: "#F3B39E"},
    {name: "blue", value: "#C2CCD4"},
    {name: "brown", value: "#aa7146"},
    {name: "red", value: "#8b2741"},
]

export default colorBag;