import { combineReducers } from "redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import cartReducer from "../reducers/cart.reducer";
import orderReducer from "../reducers/order.reducer";
import productTypeReducer from "../reducers/product-type.reducer";
import productReducer from "../reducers/product.reducer";
import userReducer from "../reducers/user.reducer";
import alertReducer from "../reducers/alert.reducer";
import customerReducer from "../reducers/customer.reducer";
import checkNavAdminReducer from "../reducers/admin-nav.reducer";
import groupReducer from "../reducers/group.reducer";
import adminSearchReducer from "../reducers/admin-search.reducer";

//root chứa các task reducer 
const rootReducer = combineReducers({
    //gọi các task
    userReducer,
    customerReducer,
    productTypeReducer,
    productReducer,
    cartReducer,
    orderReducer,
    alertReducer,
    checkNavAdminReducer,
    adminSearchReducer,
    groupReducer
});

//tạo store
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;