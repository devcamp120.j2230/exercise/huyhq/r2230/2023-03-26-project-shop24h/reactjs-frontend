import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import ContentComponent from "./component/content/ContentComponent";
import { useEffect } from 'react';
import { onAuthStateChanged } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import auth from './firebase/config';
import { Route, Routes } from 'react-router-dom';
import AdminComponent from './component/admin/AdminComponent';
import CheckAuth from './component/auth/CheckAuth';

function App() {
  const dispatch = useDispatch();
  //lưu trữ thông tin đăng nhập
  useEffect(() => {
    // onAuthStateChanged(auth, (result) => {
    //   dispatch({
    //     type: "LOGIN",
    //     data: result
    //   });
    // })
  }, [])

  return (
    <div>
      <CheckAuth/>
      <Routes>
        <Route path="/admin/*" element={<AdminComponent />}>
        </Route>
        <Route path="/*" element={<ContentComponent />} />
      </Routes>
    </div>
  );
}

export default App;
